var gulp = require('gulp'),
    minifyCSS = require('gulp-minify-css'),
    changed = require('gulp-changed'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify')


// очистка папки public
gulp.task('clean', function(){
    return gulp.src(['public/*'], {read:false})
        .pipe(clean());
});


// файлы для перемещения без изменений
var filesToMove = [
        'public-dev/fonts/*',
        'public-dev/js/**',
        'public-dev/packages/**',
        'public-dev/.htaccess',
        'public-dev/favicon.ico',
        'public-dev/index.php',
        'public-dev/robots.txt'
    ];


// перемещение файлов
gulp.task('move',['clean'], function() {
    gulp.src(filesToMove, { base: 'public-dev/' })
        .pipe(gulp.dest('public/'));
});


// минификация и сборка css
gulp.task('css', function() {
    gulp.src('public-dev/css/*.css')
        .pipe(concat('style.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('public/css'))

    gulp.src('public-dev/cp-css/*.css')
        .pipe(concat('style.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('public/cp-css'))
});


// линтинг, минификация и сборка js
gulp.task('js', function(){
    gulp.src([
        'public-dev/js/main.js',
        'public-dev/js/lazyYT.js'
    ])
        .pipe(concat('main.js'))
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js'));

    gulp.src('public-dev/js/cp-main.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js'));
});


// минификация изображений
gulp.task('images', function() {
    gulp.src('public-dev/img/*')
        .pipe(changed('public/img'))
        .pipe(imagemin())
        .pipe(gulp.dest('public/img'))

    gulp.src('public-dev/cp-img/*')
        .pipe(changed('public/cp-img'))
        .pipe(imagemin())
        .pipe(gulp.dest('public/cp-img'))
});


// сборка проекта с нуля
gulp.task('build', ['move'], function() {
    gulp.run('css', 'js', 'images');
})


// сборка файлов проекта и отслеживание изменений
gulp.task('default', function() {
    gulp.run('css', 'js', 'images')
    gulp.watch(
        [
            'public-dev/css/*.css',
            'public-dev/cp-css/*.css',
            'public-dev/js/main.js',
            'public-dev/js/lazyYT.js',
            'public-dev/js/cp-main.js',
            'public-dev/img/*'
        ],
        function(changes) {
            console.log(changes);
            gulp.run('css', 'js', 'images')
        })
});