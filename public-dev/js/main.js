﻿$(document).ready(function() {

    // Триггер всплывающего окна, по нему определяется предыдущий/следующий 
    // объект для отображения во всплывающем окне.
    var currentPopupTrigger,

        // Проигрывамеый трек.
        currentSound = null,

        // Громкость
        currentVolume = 75,

        // Ключ приложения для API Soundcloud.
        client_id = '7e370803e1ff451fe67265a2bccbdb3a',

        // Конвертация миллисекунд в человекочитаемый формат [Часы:]Минуты:Секунды
        timecode = function (ms) {
            var hms = function (ms) {
                    return {
                        h: Math.floor(ms / (60 * 60 * 1000)),
                        m: Math.floor((ms / 60000) % 60),
                        s: Math.floor((ms / 1000) % 60)
                    };
                }(ms),
                tc = []; // Timecode array to be joined with '.'

            if (hms.h > 0) {
                tc.push(hms.h);
            }

            tc.push((hms.m < 10 && hms.h > 0 ? "0" + hms.m : hms.m));
            tc.push((hms.s < 10 ? "0" + hms.s : hms.s));

            return tc.join(':');
        },

        // debug flag
        debug = true,

        // Остановка плеера, освобождение ресурсов, сброс прогрессбара.
        playerStop = function() {
            if (currentSound) {
                currentSound.destruct();
                currentSound = false;

                $('.loadBar').css('width', '0');
                $('.progBar').css('width', '0');

                if (debug) { console.log('PlayerStop called.', currentSound); }
            }
        },

        // Инициализация и запуск плеера.
        playerPlay = function() {

            if (!$('.popup-music-play').length) {
                if (debug) { console.log('playerPlay interrupted: ".popup-music-play" not found.'); }
                return false;
            }

            if (currentSound) { 
                currentSound.destruct();
                if (debug) { console.log('playerPlay: currentSound is true, and will be destructed.', currentSound); }
            }

            if (debug) { console.log('createSound url', $('.popup-music-play').data('streamUrl') + '?client_id=' + client_id); }

            currentSound = soundManager.createSound({
                url: $('.popup-music-play').data('streamUrl') + '?client_id=' + client_id,
                autoPlay: true,
                volume: currentVolume,
                whileplaying: function() {
                    $('.progBar').css('width', ((this.position/this.duration) * 100) + '%');
                    $('.popup-music-position').html(timecode(Math.floor(this.position)));
                },
                whileloading: function() {
                    $('.loadBar').css('width', ((this.bytesLoaded/this.bytesTotal)*100)+'%');
                },
                onfinish: function() {
                    $('.progBar').css('width', '0');

                    // По окончании трека плеер попытается перейти к следующему в списке.
                    $('.popup-music-next').trigger('click');
                }
            });

            $('.popup-music-volume').noUiSlider({
                start: currentVolume,
                range: {
                    'min': 0,
                    'max': 100
                }
            });
        };

    document.playerPlay = playerPlay;
    document.playerStop = playerStop;

    //Показ попапа с ajax-содержимым.
    $(document).on('click', '.js-ajax-popup', function() {

        currentPopupTrigger = $(this);

        var url = currentPopupTrigger.attr('href');

        if (debug) { console.log('Popup url', url); }

        $.arcticmodal({
            ajax: {type: 'POST'},
            type: 'ajax',
            'url': url,
            beforeOpen: function(data, el) {
                console.log(data, el);
            },
            afterLoadingOnShow: playerPlay,
            beforeClose: playerStop
        });

        return false;
    });


    // Изменение громкости ползунком.
    $(document).on('change', '.popup-music-volume', function() {
        currentVolume = Math.floor($(this).val());
        currentSound.setVolume(currentVolume);

        if (debug) { console.log('Volume changed', currentVolume); }
    });


    // Смена трека кнопками next/prev в попапе.
    $(document).on('click', '.js-music-change', function() {

        var currentClass;

        if (!currentPopupTrigger) {
            if (debug) { console.log('.js-music-change onClick: currentPopupTrigger is false.', currentPopupTrigger); }
            return false;
        }

        if (currentPopupTrigger.hasClass('music-card-play')) {
            currentClass = '.music-card-play';
        } else if (currentPopupTrigger.hasClass('top-ten-play')) {
            currentClass = '.top-ten-play';
        } else if (currentPopupTrigger.hasClass('footer-popular-link')) {
            currentClass = '.footer-popular-link';
        }

        currentClass = currentClass + '.js-ajax-popup';

        var index = currentPopupTrigger.index(currentClass);

        $(this).hasClass('popup-music-next') ?
            index++:
            index--;

        var nextPopup = $($(currentClass)[index]);

        if (nextPopup.length === 0) { return false; }

        playerStop();

        $('.arcticmodal-container_i2')
            .fadeOut(100)
            .html('<div class="arcticmodal-loading" />')
            .fadeIn(100);

        $.post(
            nextPopup.attr('href'),
            function(data) { 
                $('.arcticmodal-container_i2').html(data);
                playerPlay(); 
            }
        );

        currentPopupTrigger = nextPopup;

        return false;
    });


    // Play/pause по нажатию кнопки плеера.
    $(document).on('click', '.popup-music-play', function() {
        if (!currentSound) { return false; }

        currentSound.togglePause();
        $(this).toggleClass('pause');

        if (debug) { console.log('Play/pause button click.'); }

        return false;
    });


    // Перемотка трека.
    $(document).on('click', '.progBarWrapper', function (e) {
        if (!currentSound) { return false; }

        var $this = $(this);
        var posX = $this.offset().left;
        var relPosX = e.pageX - posX;

        var position = Math.floor(relPosX / $this.innerWidth() * 100) / 100;
        var loading = Math.floor(currentSound.bytesLoaded/currentSound.bytesTotal * 100) / 100;

        if (loading > position) {
            currentSound.setPosition(Math.floor(currentSound.duration * position));
        }

    });

    // Отключаем drag-n-drop гистограммы трека.
    $(document).on('dragstart', '.waveBar', function(event) { event.preventDefault(); });


    // Смена мероприятия кнопками next/prev в попапе.
    $(document).on('click', '.js-event-change', function() {

        var index = currentPopupTrigger.index('.event-card-button.js-ajax-popup');

        $(this).hasClass('popup-event-next') ?
            index++:
            index--;

        var nextPopup = $($('.event-card-button.js-ajax-popup')[index]);
        if (nextPopup.length === 0) { return false; }

        $('.arcticmodal-container_i2')
            .fadeOut(100)
            .html('<div class="arcticmodal-loading" />')
            .fadeIn(100);

        $.post(
            nextPopup.attr('href'),
            function(data) { $('.arcticmodal-container_i2').html(data); }
        );

        currentPopupTrigger = nextPopup;

        return false;
    });


    // Смена клипа кнопками next/prev в попапе.
    $(document).on('click', '.js-video-change', function() {
        var index = currentPopupTrigger.index('.video-card-play.js-ajax-popup');

        $(this).hasClass('popup-video-next') ?
            index++:
            index--;

        var nextPopup = $($('.video-card-play.js-ajax-popup')[index]);
        if (nextPopup.length === 0) { return false; }

        $('.arcticmodal-container_i2')
            .fadeOut(100)
            .html('<div class="arcticmodal-loading" />')
            .fadeIn(100);

        $.post(
            nextPopup.attr('href'),
            function(data) { $('.arcticmodal-container_i2').html(data); }
        );

        currentPopupTrigger = nextPopup;

        return false;
    });


    // Подгрузка дополнительного контента по нажатию кнопки «view more».
    $(document).on('click', '.js-ajax-more', function(e) {
        var url = $(this).addClass('active').attr('href');
        
        $.ajax(url, {
            type: 'POST'
        }).done(function( msg ) {
            $('.js-ajax-more.active').replaceWith(msg);
        }).fail(function() {
            $('.js-ajax-more.active').remove();
        });

        return false;
    });


    // Закрытие попапа.
    $(document).on('click', '.popup-close', function() { 
        $(this).arcticmodal('close'); 
        return false; 
    });


    // Сворачивание/разворачивание описания.
    $(document).on('click', '.popup-about-title', function() {
        var $this = $(this);
        if ($this.hasClass('active')) {
            $('.popup-about-description').css('height', '22px');
            $this.removeClass('active');
        } else {
            $('.popup-about-description').css('height', 'auto');
            $this.addClass('active');
        }
    });

});