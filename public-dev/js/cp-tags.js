$(function() {

    // Стилизация скроллбаров.
    $('.js-scroll').jScrollPane({
       autoReinitialise: true,
       hideFocus: true
    });


    // Инициализация уведомлений.
    humane.error = humane.spawn({ addnCls: 'humane-flatty-error', timeout: 3000 });
    humane.success = humane.spawn({ addnCls: 'humane-flatty-success', timeout: 3000 });


    // Запрос контента
    var xhr;
    function retrieve() {

        // Если предыдущий запрос ещё не завершился, прерываем его.
        if (xhr) {
            xhr.abort();
        } else {
            $('.content-list').html('<li><img src="/cp-img/loading.gif"> <li>');
        }

        // снимаем чекбокс «check all»
        $('.topbar-check-all').removeClass('active');

        xhr = $.post(
            '/adminka/metatags',
            function(data, textStatus) {

                $('.js-scroll').jScrollPane().data('jsp').destroy();
                $('.content-list').html(data.view);

                // Подстройка высоты textarea под содержимое
                $('.js-elastic').elastic();

                // повторная инициализация скроллбара
                $('.js-scroll').jScrollPane({
                   autoReinitialise: true,
                   hideFocus: true
                });

                xhr = null;
            }
        );
    }

    // Первичная загрузка контента
    retrieve();


    /* Обработчики кнопок панели управления */

    // Кнопка «удалить». При нажатии удаляет отмеченные элементы.
    $('.topbar-control-delete').on('click', function() {
        var control = $(this), data = {items: []};

        $('.content-header-check.active')
            .closest('.content-item')
            .each(function(i, e) {
                var $e = $(e);
                data.items.push({
                    id: $e.find('.content-header-id').html(),
                    type: $e.find('.content-form-type').val()
                });
            });

        // Отправляем описание объектов для удаления на сервер.
        $.post( "/adminka/delete/metatags", data, function(data) {
            if (!data.result) { humane.error('When you delete objects, an error occurred.'); return false; }
            retrieve();
            humane.success('Objects deleted successfully.');
        }).fail(function() {
            humane.error('When you delete objects, an error occurred.');
        });

        return false;
    });


    // Кнопка «редактировать». По нажатию раскрывает отмеченные элементы.
    $('.topbar-control-edit').on('click', function() {
        var control = $(this);

        $('.content-header-check.active')
            .closest('.content-item')
            .find('.js-content-edit')
            .not('.active')
            .trigger('click');

        return false;
    });


    // Кнопка «добавить».
    $('.topbar-control-add').on('click', function() {
        $('.add-content').arcticmodal({clone: true});
        return false;
    });


    // чекбокс «ALL»
    $('.topbar-check-all').on('click', function() {
        var control = $(this);
        if (!control.hasClass('active')) {
            control.addClass('active');
            $('.content-header-check').addClass('active');
        } else {
            control.removeClass('active');
            $('.content-header-check').removeClass('active');
        }
    });


    // чекбоксы контента
    $('.content').on('click', '.content-header-check', function() {
        $(this).toggleClass('active');
        $('.topbar-check-all').removeClass('active');
    });


    // сворачивание/разворачивание объектов
    $('.content').on('click', '.js-content-edit:not(.active)', function(e) {
        var control = $(this);
        control.addClass('active');

        var contentItem = control.closest('.content-item');
        contentItem.addClass('active');
        contentItem.find('.content-header-category').hide();
        contentItem.find('.dropdown').css('display','inline-block');
        return false;
    });

    $('.content').on('click', '.js-content-edit.active', function() {
        var control = $(this);
        control.removeClass('active');
        
        var contentItem = control.closest('.content-item');
        contentItem.removeClass('active');
        contentItem.find('.dropdown').css('display','none');
        contentItem.find('.content-header-category').show();
        return false;
    });

    // сворачивание формы по кнопке «cancel»
    $('.content').on('click', '.js-form-cancel', function() {
        $(this).closest('.content-item').find('.js-content-edit').trigger('click');
        return false;
    });

    // Сериализация формы в объект.
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    // Сохранение объекта по кнопке «save».
    $('.content').on('click', '.js-form-save', function() {
        var control = $(this), data = {};

        $contentItem = $(control.closest('.content-item'));

        data.form = $contentItem.find('.content-form').serializeObject();
        data.id = $contentItem.find('.content-header-id').html();

        // Отправляем информацию о новом объекте на сервер.
        $.post( "/adminka/update/metatags", data, function(data) {
            humane.success('Objects saved successfully.');
            retrieve();
        }).fail(function() {
            humane.error('When you save objects, an error occurred.');
        });

        return false;
    });

    // Закрытие формы добавления контента.
    $(function() {
        $(document).on('click', '.js-content-form-cancel', function() {
            $(this).arcticmodal('close');

            // сброс данных о добавляемом объекте
            tempObjectData = false;

            return false;
        });
    });

    // Данные о добавляемых объектах
    var tempObjectData = false;

    // Добавление события.
    $(document).on('click', '.js-content-event-save', function() {
        $(this).closest('.add-content-form').submit();
    });

});