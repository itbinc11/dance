$(function() {

    // Стилизация скроллбаров.
    $('.js-scroll').jScrollPane({
       autoReinitialise: true,
       hideFocus: true
    });


    // Инициализация уведомлений.
    humane.error = humane.spawn({ addnCls: 'humane-flatty-error', timeout: 3000 });
    humane.success = humane.spawn({ addnCls: 'humane-flatty-success', timeout: 3000 });


    // Стилизации select'ов категорий.
    var categoryChange = function(selected) {
        $(this).closest('.content-item').find('.content-header-category').text(selected.title);
    };
    $('.js-category-select').easyDropDown({ 'onChange':categoryChange });


    // Теги.
    // Ограничение на количество тегов.
    var tagsLimitation = function() {

        var tagMax = 5,
            input = $(this);

        if (input.val().split(',').length > tagMax) {

            var tags = input.val().split(',');
            tags.length = tagMax;
            input.importTags(tags.join(','));
            
            humane.error('You cannot assign more tags.');
        }
    };

    // Инициализация tagsInput'ов.
    $('.content-form-field-tags').tagsInput({
        'defaultText': '',
        'maxChars': 128,
        'onAddTag': tagsLimitation
    });


    // Переключение фильтра опубликовано/не опубликовано.
    var publishedFilter = $('.js-published-filter').first();
    publishedFilter.addClass('active');

    $('.js-published-filter').on('click', function() {
        if ($(this).hasClass('active')) { return false; }
        publishedFilter.removeClass('active');
        publishedFilter = $(this);
        publishedFilter.addClass('active');

        $('.js-current-page').val(1);

        retrieve();
        return false;
    });


    // Переключение фильтра по типу контента.
    var typeFilter = $('.js-type-filter').first();
    typeFilter.addClass('active');

    $('.js-type-filter').on('click', function() {

        // Скрыть/показать подкатегории для events.
        if ($(this).data('type') == 'events') {
            $('.tag-filter-list').addClass('hidden');
        } else if($(this).data('type') == 'comments') {
            $('.tag-filter-list').removeClass('hidden');
            $('.tag-filter-item').addClass('hidden');
            $('.tag-filter-item a').removeClass('active');
            $('.tag-filter-item').first().removeClass('hidden');
            $('.tag-filter-item a').first().addClass('active');
            $('.comments-item').removeClass('hidden');
            categoryFilter = $('.tag-filter-item a').first();
        } else {
            var currentFilter = $('.tag-filter-link.js-tag-filter.active');
            if (currentFilter.data('tag') == 'videos' || currentFilter.data('tag') == 'event' || currentFilter.data('tag') == 'music') {
                $('.tag-filter-item a').removeClass('active');
                //$('.tag-filter-item a').first().addClass('active');
                categoryFilter = $('.tag-filter-item a').first().addClass('active');
            }
            $('.tag-filter-item').removeClass('hidden');
            $('.comments-item').addClass('hidden');
            $('.tag-filter-list').removeClass('hidden');
        }

        if ($(this).hasClass('active')) { return false; }
        typeFilter.removeClass('active');
        typeFilter = $(this);
        typeFilter.addClass('active');

        $('.js-current-page').val(1);

        retrieve();
        return false;
    });


    // Переключение фильтра по подкатегориям.
    var categoryFilter = $('.js-tag-filter').first();
    categoryFilter.addClass('active');

    $('.js-tag-filter').on('click', function() {
        if ($(this).hasClass('active')) { return false; }
        $('.js-tag-filter').removeClass('active');
        categoryFilter = $(this);
        categoryFilter.addClass('active');

        $('.js-current-page').val(1);

        retrieve();
        return false;
    });


    // Переключение страниц кнопками prev/next.
    $('.js-prev-page').on('click', function() {
        var val = parseInt($('.js-current-page').val(), 10);
        $('.js-current-page').val(val-1).change();
    });
    $('.js-next-page').on('click', function() {
        var val = parseInt($('.js-current-page').val(), 10);
        $('.js-current-page').val(val+1).change();
    });


    // Изменение номера текущей страницы.
    $('.js-current-page').on('change', function() { 
        var val = parseInt($(this).val(), 10);
        var totalPages = parseInt($('.js-total-pages').html(), 10);

        if (val < 1 || isNaN(val)) {
            val = 1;
        } else if (val > totalPages) {
            val = totalPages;
        }
        $(this).val(val);

        retrieve();
    });


    // Запрос контента, соответствующего настройкам фильтров.
    var xhr;
    function retrieve() {

        // Если предыдущий запрос ещё не завершился, прерываем его.
        if (xhr) {
            xhr.abort(); 
        } else { 
            $('.content-list').html('<li><img src="/cp-img/loading.gif"> <li>'); 
        }

        // снимаем чекбокс «check all»
        $('.topbar-check-all').removeClass('active');

        console.log(
            'filter attributes:',
            publishedFilter.data('published'),
            typeFilter.data('type'),
            categoryFilter.attr('data-tag'),
            $('.js-current-page').val()
        );

        xhr = $.post(
            '/adminka/api',
            {
                'publishedFilter': publishedFilter.attr('data-published'),
                'typeFilter': typeFilter.attr('data-type'),
                'categoryFilter': categoryFilter.attr('data-tag'),
                'currentPage': $('.js-current-page').val()
            },
            function(data, textStatus) {

                $('.js-scroll').jScrollPane().data('jsp').destroy();
                $('.content-list').html(data.view);
                $('.js-total-pages').html(data.totalPages);

                console.log('response pages:', data.totalPages);

                // Подстройка высоты textarea под содержимое
                $('.js-elastic').elastic();

                $('.js-date-picker').appendDtpicker();

                // повторная инициализация скроллбара
                $('.js-scroll').jScrollPane({
                   autoReinitialise: true,
                   hideFocus: true
                });

                // повторная инициализация tagsinput
                $('.content-form-field-tags').tagsInput({ 'defaultText':'', 'maxChars':128, 'onAddTag':tagsLimitation });

                // повторная инициализация select'ов категорий
                $('.js-category-select').easyDropDown({ 'onChange':categoryChange });

                // инициализация js-redactor
                $('.js-redactor').redactor({ minHeight: 115 });

                xhr = null;
            }
        );
    }

    // Первичная загрузка контента
    retrieve();


    /* Обработчики кнопок панели управления */

    // Кнопка «опубликовать». При нажатии изменяет статус отмеченных объектов на «опубликовано».
    $('.topbar-control-publish').on('click', function() {
        var control = $(this), data = {items: []};

        $('.content-header-check.active')
            .closest('.content-item')
            .each(function(i, e) {
                var $e = $(e);
                data.items.push({
                    id: $e.find('.content-header-id').html(),
                    type: $e.find('.content-form-type').val()
                });
            });

        // Отправляем описание объектов для публикации на сервер.
        $.post( "/adminka/publish", data, function(data) {
            humane.success('Objects successfully published.');
            retrieve();
        }).fail(function() {
            humane.error('When publishing, an error occurred.');
        });

        return false;
    });


    // Кнопка «снять с публикации». При нажатии изменяет статус отмеченных объектов на «не опубликовано».
    $('.topbar-control-unpublish').on('click', function() {
        var control = $(this), data = {items: []};

        $('.content-header-check.active')
            .closest('.content-item')
            .each(function(i, e) {
                var $e = $(e);
                data.items.push({
                    id: $e.find('.content-header-id').html(),
                    type: $e.find('.content-form-type').val()
                });
            });

        // Отправляем описание объектов для снятия с публикации на сервер.
        $.post( "/adminka/unpublish", data, function(data) {
            humane.success('Objects successfully unpublished.');
            retrieve();
        }).fail(function() {
            humane.error('When unpublishing, an error occurred.');
        });

        return false;
    });


    // Кнопка «удалить». При нажатии удаляет отмеченные элементы.
    $('.topbar-control-delete').on('click', function() {
        var control = $(this), data = {items: []};

        $('.content-header-check.active')
            .closest('.content-item')
            .each(function(i, e) {
                var $e = $(e);
                data.items.push({
                    id: $e.find('.content-header-id').html(),
                    type: $e.find('.content-form-type').val()
                });
            });

        // Отправляем описание объектов для удаления на сервер.
        $.post( "/adminka/delete", data, function(data) {
            if (!data.status) { humane.error('When you delete objects, an error occurred.'); return false; }
            retrieve();
            humane.success('Objects deleted successfully.');
        }).fail(function() {
            humane.error('When you delete objects, an error occurred.');
        });

        return false;
    });


    // Кнопка «редактировать». По нажатию раскрывает отмеченные элементы.
    $('.topbar-control-edit').on('click', function() {
        var control = $(this);

        $('.content-header-check.active')
            .closest('.content-item')
            .find('.js-content-edit')
            .not('.active')
            .trigger('click');

        return false;
    });


    // Генерация datetime
    function formatDateTime(d) {

        function pad (num) {
            return num < 10 ? '0'+num : num;
        }
        
        var YYYY = d.getFullYear(),
            MM = pad(d.getMonth()+1),
            DD = pad(d.getDate()),
            hh = pad(d.getHours()),
            mm = pad(d.getMinutes()),
            ss = pad(d.getSeconds());
        
        return [YYYY,MM,DD].join('-') + ' ' + [hh,mm,ss].join(':');
        
    }

    // Кнопка «добавить».
    $('.topbar-control-add').on('click', function() {
        $('.add-content').arcticmodal({
            clone: true,
            beforeClose: function(data, el) {
                $('.js-redactor-popup').redactor('destroy');
            }
        });
        $('.arcticmodal-container .js-popup-date-picker').appendDtpicker();

        // инициализация js-redactor
        $('.js-redactor-popup').redactor({ minHeight: 115 });

        // Подстановка текущего datetime
        var date = formatDateTime(new Date());

        $('.arcticmodal-container .js-popup-date-picker').val(date);
        $('input[name=pdate]').add('input[name=hidden-pdate]').val(date); // используется для определения очереди

        return false;
    });


    // чекбокс «ALL»
    $('.topbar-check-all').on('click', function() {
        var control = $(this);
        if (!control.hasClass('active')) {
            control.addClass('active');
            $('.content-header-check').addClass('active');
        } else {
            control.removeClass('active');
            $('.content-header-check').removeClass('active');
        }
    });


    // чекбоксы контента
    $('.content').on('click', '.content-header-check', function() {
        $(this).toggleClass('active');
        $('.topbar-check-all').removeClass('active');
    });


    // сворачивание/разворачивание объектов
    $('.content').on('click', '.js-content-edit:not(.active)', function(e) {
        var control = $(this);
        control.addClass('active');

        var contentItem = control.closest('.content-item');
        contentItem.addClass('active');
        contentItem.find('.content-header-category').hide();
        contentItem.find('.dropdown').css('display','inline-block');
        return false;
    });

    $('.content').on('click', '.js-content-edit.active', function() {
        var control = $(this);
        control.removeClass('active');
        
        var contentItem = control.closest('.content-item');
        contentItem.removeClass('active');
        contentItem.find('.dropdown').css('display','none');
        contentItem.find('.content-header-category').show();
        return false;
    });

    // сворачивание формы по кнопке «cancel»
    $('.content').on('click', '.js-form-cancel', function() {
        $(this).closest('.content-item').find('.js-content-edit').trigger('click');
        return false;
    });

    // Кнопка-триггер file input'a.
    $(document).on('click', '.js-img-input-trigger', function() {
        $(this).closest('.content-item').find('.js-img-input').click();
        return false;
    });

    // Превью изображения.
    $(document).on('change', '.js-img-input', function() {
        var input = $(this);

        if (typeof(FileReader)!='undefined') {
            var reader = new FileReader();
            reader.onload = function(e) {
                input.closest('.content-item').find('.content-form-image').attr('src', e.target.result);
            };
            reader.readAsDataURL(this.files.item(0));
        }

    });

    // Сериализация формы в объект.
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    // Сохранение объекта по кнопке «save».
    $('.content').on('click', '.js-form-save', function() {
        var control = $(this), data = {};

        $contentItem = $(control.closest('.content-item'));

        data.form = $contentItem.find('.content-form').serializeObject();
        data.form.category = $contentItem.find('.js-category-select').val();
        data.id = $contentItem.find('.content-header-id').html();

        // Отправляем информацию о новом объекте на сервер.
        $.post( "/adminka/update", data, function(data) {
            if (data.status) {
                humane.success('Objects saved successfully.');
            } else {
                var arr = $.map(data.errors, function(el) { return el; });
                humane.error(arr);
            }
            retrieve();
        }).fail(function(data) {
            humane.error('Error communicating with the server.');
        });

        return false;
    });

    // Табы.
    $(function () {
        $("dl.tabs dt").click(function () {
            $(this)
                .siblings().removeClass("active").end()
                .next("dd").andSelf().addClass("active");
        });
    });

    // Закрытие формы добавления контента.
    $(function() {
        $(document).on('click', '.js-content-form-cancel', function() {
            $(this).arcticmodal('close');

            // сброс данных о добавляемом объекте
            tempObjectData = false;

            return false;
        });
    });

    // Данные о добавляемых объектах
    var tempObjectData = false;

    // Проверка доступности трека.
    $(document).on('click', '.js-content-form-add-track', function() {

        var inputUrl = $(this).closest('.add-content-form').find('.content-form-field').val();
        if (!inputUrl) { return false; }

        // сборка url с GET-параметрами
        var url = {
            url: inputUrl,
            client_id: '7e370803e1ff451fe67265a2bccbdb3a'
        };
        url = 'http://api.soundcloud.com/resolve.json?' + decodeURIComponent($.param(url));

        console.log('Generated url', url);

        // отображение индикатора загрузки
        $('.arcticmodal-container_i2').html('<div class="arcticmodal-loading" />');

        // получение данных о треке, заполнение и вывод формы редактирования
        $.getJSON(url, function(data) {

            console.log('Resolve successfull!', data);

            // проверка разрешения на стриминг трека
            if (!data.streamable) {
                console.log('Track is not available for streaming.');
                return false;
            }

            console.log('Track is streamable.');

            // сохранение информации о треке
            tempObjectData = data;

            // рендеринг формы
            var template = $.templates("#add-track");
            $('.arcticmodal-container_i2').html(template.render(data));

            // инициализация поля тегов
            $('.content-form-field-tags').tagsInput({ 'defaultText':'', 'maxChars':128, 'onAddTag':tagsLimitation });

            // инициализация выбора категории
            $('.js-category-select').easyDropDown();
            $('.arcticmodal-container_i2').find('.dropdown').addClass('add-dropdown');

            $('.arcticmodal-container .js-popup-date-picker').appendDtpicker();

            // Подстановка текущего datetime
            var date = formatDateTime(new Date());

            $('.arcticmodal-container .js-popup-date-picker').val(date);
            $('input[name=pdate]').add('input[name=hidden-pdate]').val(date); // используется для определения очереди
        })
        .fail(function() {
            $.arcticmodal('close');
            humane.error('Track is not available for streaming.');

            console.log('Resolve failed!');

            return false;
        });
    });

    // Добавление трека в базу
    $(document).on('click', '.js-content-track-save', function() {
        var control = $(this), data = {};

        $contentItem = $(control.closest('.add-content-form'));

        data.form = $contentItem.serializeObject();
        data.tempObjectData = tempObjectData;

        console.log($contentItem, $contentItem.serializeObject(), data);

        $('.arcticmodal-container_i2').html('<div class="arcticmodal-loading" />');

        // Отправляем информацию о новом объекте на сервер.
        $.post( "/adminka/create/track", data, function(data) {
            if (data.status) {
                humane.success('Objects saved successfully.');
            } else {
                var arr = $.map(data.errors, function(el) { return el; });
                humane.error(arr);
            }
            retrieve();
            $.arcticmodal('close');
        }).fail(function() {
            $.arcticmodal('close');
            humane.error('Error communicating with the server.');
        });

        return false;
    });

    // Извлечение video id из youtube ссылки.
    function matchYoutubeUrl(url) {
        var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        return (url.match(p)) ? RegExp.$1 : false ;
    }

    
    // Получение информации о клипе на youtube.
    $(document).on('click', '.js-content-form-add-video', function() {
        var inputUrl = $(this).closest('.add-content-form').find('.content-form-field').val();
        if (!inputUrl) { return false; }

        var video_id = matchYoutubeUrl(inputUrl);

        console.log('Matched video id', video_id);

        // отображение индикатора загрузки
        $('.arcticmodal-container_i2').html('<div class="arcticmodal-loading" />');

        $.getJSON('http://gdata.youtube.com/feeds/api/videos/'+video_id+'?v=2&alt=jsonc', function(data){
            console.log('Information about the youtube video received!', data);

            // сохранение информации о клипе
            tempObjectData = data.data;

            // рендеринг формы
            var template = $.templates("#add-video");
            $('.arcticmodal-container_i2').html(template.render(tempObjectData));

            // инициализация поля тегов
            $('.content-form-field-tags').tagsInput({ 'defaultText':'', 'maxChars':128, 'onAddTag':tagsLimitation });

            // инициализация выбора категории
            $('.js-category-select').easyDropDown();
            $('.arcticmodal-container_i2').find('.dropdown').addClass('add-dropdown');

            $('.arcticmodal-container .js-popup-date-picker').appendDtpicker();

            // Подстановка текущего datetime
            var date = formatDateTime(new Date());

            $('.arcticmodal-container .js-popup-date-picker').val(date);
            $('input[name=pdate]').add('input[name=hidden-pdate]').val(date); // используется для определения очереди
        })
        .fail(function() {
            $.arcticmodal('close');
            humane.error('Failed to retrieve information about a video.');

            console.log('Failed to retrieve information about a video.');

            return false;
        });
    });

    // Добавление клипа в базу
    $(document).on('click', '.js-content-video-save', function() {
        console.log('add');
        var control = $(this), data = {};

        $contentItem = $(control.closest('.add-content-form'));

        data.form = $contentItem.serializeObject();
        data.tempObjectData = tempObjectData;

        $('.arcticmodal-container_i2').html('<div class="arcticmodal-loading" />');

        // Отправляем информацию о новом объекте на сервер.
        $.post( "/adminka/create/video", data, function(data) {
            if (data.status) {
                humane.success('Objects saved successfully.');
            } else {
                var arr = $.map(data.errors, function(el) { return el; });
                humane.error(arr);
            }
            $.arcticmodal('close');
            retrieve();
        }).fail(function() {
            $.arcticmodal('close');
            humane.error('Error communicating with the server.');
        });

        return false;
    });


    // Добавление события.
    $(document).on('click', '.js-content-event-save', function() {
        $(this).closest('.add-content-form').submit();
    });

    // Сохранение данных пользователя.
    $('.content').on('click', '.js-user-save', function() {
        var form = $(this).closest("form");
        data = form.serializeObject();
        $.post(
            '/adminka/editUser',
            data,
            function (data) {
                // OK
                // Закрытие формы при успешном сохранении данных пользователя
                form.closest('.content-item').find('.js-content-edit').trigger('click');
            }
        ).fail(function() {
                //Not OK
                console.log('Saving error.');
            });

        return false;
    });

});