del app\database\production.sqlite
copy app\database\production.sqlite.empty app\database\production.sqlite
php artisan migrate --package=cartalyst/sentry
php artisan migrate
php artisan optimize
php artisan db:seed
pause