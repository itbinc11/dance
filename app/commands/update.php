<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class update extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:update';


	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update database';


	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}


	/**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $events = MusicEvent::all();
        $events->each(function($event) {
            $date = \Carbon\Carbon::parse($event->date);
            $event->date = $date->toDateTimeString();
            $event->save();
            $this->info($date);
        });

        $sub = new Subcategory;

        $sub->title = ('B-boying');
        $sub->slug = ('b-boying');

        $sub->save();

        // перенос видосов из категории в категорию
        DB::table('videos')
            ->where('subcategory_id', 6)
            ->update(array('subcategory_id' => 7));

        $music = Music::all();
        $i = 0;

        foreach ($music as $track) {

            if ( !($i%30) ) {
                sleep(3);
            }

            //получить id пользователя
            $ch = curl_init($track->api_url . '.json?client_id=' . Config::get('app.apiKey'));
            curl_setopt_array($ch, array(
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_CONNECTTIMEOUT => 2,
                CURLOPT_RETURNTRANSFER => true
            ));
            $response = curl_exec($ch);
            $response = json_decode($response);

            if (curl_errno($ch) or $response === null or !isset($response->user_id)) {
                continue;
            }

            //получить ссылки на пользователя
            curl_setopt($ch, CURLOPT_URL, sprintf("http://api.soundcloud.com/users/%d/web-profiles.json?client_id=%s", $response->user_id, Config::get('app.apiKey')));
            $response = curl_exec($ch);
            $response = json_decode($response);
            $links = array();

            foreach ($response as $link) {
                switch ($link->service) {
                    case 'twitter':
                        $links["user_twitter"] = $link->url;
                        break;

                    case 'facebook':
                        $links["user_facebook"] = $link->url;
                        break;

                    case 'personal':
                        $links["user_personal"] = $link->url;
                        break;
                        
                    default:
                        // do nothing
                        break;
                }
            }
            
            if (count($links)) {
                //добавить их к объекту
                $track->fill($links);

                $this->info($track->title);
                foreach ($links as $link) {
                    $this->info($link);
                }
                
                //сохранить объект
                $track->save();
            }

            $i++;

        }
    }


	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::OPTIONAL, 'An example argument.'),
		);
	}


	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
