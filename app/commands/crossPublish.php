<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class crossPublish extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:crossPublish';


	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Crossposting in social networks';


	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}


	/**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        // получаем очередь заданий на кросспостинг
        $queue = CrosspostingQueue::all();

        // авторизуемся в соц.сети
        $facebook = $this->facebookAuth();
        $twitter = $this->twitterAuth();

        // кросспостим
        $this->publicate($queue, $facebook, $twitter);
    }


    /**
     * Аутентификация в Facebook Graph API.
     * @return FacebookSession
     */
    protected function facebookAuth()
    {
        try {
            // Устанавливаем учётные данные Facebook-приложения.
            Facebook\FacebookSession::setDefaultApplication(
                Config::get('app.facebookAppId'),
                Config::get('app.facebookAppSecret')
            );

            // Открываем и проверяем сессию с заранее полученным токеном страницы.
            $session = new Facebook\FacebookSession(Config::get('app.facebookPageToken'));
            $session->validate();

        } catch (\Exception $e) {
            Log::error('Кросспостинг: не удалось авторизоваться в facebook.');            
            $this->info('Facebook auth fail.');

            $session = false;
        }

        return $session;
    }


    /**
     * Аутентификация в twitter API.
     * @return Twitter
     */
    protected function twitterAuth()
    {
        $twitter = new \Twitter(
            Config::get('app.consumerKey'),
            Config::get('app.consumerSecret'),
            Config::get('app.accessToken'),
            Config::get('app.accessTokenSecret')
        );

        // Проверяем статус аутентификации.
        if (!$twitter->authenticate()) {
            Log::error('Кросспостинг: не удалось авторизоваться в twitter.');
            $this->info('Twitter auth fail');
            $twitter = false;
        }

        return $twitter;
    }


    /**
     * Кросспостинг контента.
     * @param  Collection $queue
     * @param  FacebookSession|false $facebook
     * @param  Twitter|false $twitter
     * @return void
     */
    protected function publicate($queue, $facebook, $twitter)
    {
        if (!$queue or !$queue->count()) {
            Log::info("Кросспостинг: очередь задач пуста.");
            return false; // Если задач нет, завершаемся.
        }

        foreach ($queue as $task) {

            $content = $task->content;

            // Отфильтровываем удалённый контент.
            if (!is_a($content, 'Publication')) {
                Log::info("Кросспостинг: связанный контент не найден. {$task->content_id}");
                $this->info("Content not found. {$task->content_id}");

                $task->delete();
                continue;
            }

            // Собираем информацию для публикации.
            $contentUrl = $content->getUrl();
            $contentShortUrl = $content->getShortUrl();

            // У событий нет своих тегов, поэтому для них подставляем готовые.
            if ($content->tags) {
                $contentTags = $content->tags->lists('title');
                $contentTags[] = 'danceproject';

                $tags = array();
                foreach ($contentTags as $tag) {
                    if (strlen($tag)) { // отсеиваем пустые строки.
                        $tags[] = '#' . preg_replace('/\s+/', '', $tag);
                    }
                }
                $contentTags = implode(' ', $tags);
            } else {
                $contentTags = '#event #danceproject';
            }

            // Постинг в Facebook.
            // Если авторизация прошла успешно и контент ещё не был опубликован.
            if ($facebook and !$task->facebook) {
                try {
                    $response = (new Facebook\FacebookRequest(
                        $facebook,
                        'POST',
                        "/me/feed",
                        array(
                            'message' => $contentTags,
                            'link' => $contentUrl
                        )
                    ))->execute()->getGraphObject();

                    if ($response->getProperty('id')) {
                        $task->facebook = true;
                        Log::info('Пост опубликован в facebook', array('id' => $response->getProperty('id')));
                        $this->info('FB post published', array('id' => $response->getProperty('id')));
                    } else {
                        Log::info('Пост не опубликован в facebook', array('error' => $response->getProperty('error')));
                        $this->info('FB post not published', array('error' => $response->getProperty('error')));
                    }

                } catch (\Exception $ex) {
                    Log::error("Не удалось опубликовать пост в facebook ({$ex->getMessage()})");
                    $this->info("FB post not published ({$ex->getMessage()})");
                }
            }

            // Постинг в Twitter.
            // Если авторизация прошла успешно и контент ещё не был опубликован.
            if ($twitter and !$task->twitter) {

                // Составляем твит
                $contentTitle = $content->title;

                // Длина заголовка контента = доступное количество символов 
                // после добавления ссылки, тегов, кавычек и их склейки.
                $titleLength = 140 - 2 - 2 - mb_strlen($contentShortUrl) - mb_strlen($contentTags);

                // Умещаем слишком длинные названия в отведенную длину.
                if (mb_strlen($contentTitle) > $titleLength) {
                    $contentTitle = mb_substr($contentTitle, 0, ($titleLength - 1)) . '…';
                }

                // Защищаемся от текстовых twitter-команд кавычками.
                $contentTitle = '“' . $contentTitle . '”';

                // Собираем твит и…
                $message = implode(' ', array(
                    $contentTitle,
                    $contentShortUrl,
                    $contentTags
                ));

                // отправляем.
                try {
                    $twitter->send($message);
                    $task->twitter = true;
                    Log::info("Твит опубликован: «{$message}»");
                    $this->info("Tweet published: {$message}");
                } catch (TwitterException $e) {
                    Log::error("Не удалось опубликовать твит ({$message}): " . $e->getMessage());
                    $this->info("Tweet not published: " . $e->getMessage());
                }
            }

            // Если контент везде размещен успешно — удаляем задачу,
            if ($task->facebook and $task->twitter) {
                $task->delete();
            } else {
                // иначе — сохраняем её состояние до следующей попытки.
                $task->save();
            }

        }
	}


	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::OPTIONAL, 'An example argument.'),
		);
	}


	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
