<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Route::get('/add/admin', function() {
    // Создаём пользователя
    $user = Sentry::createUser(array(
        'username' => 'okYnb',
        'email' => 'okYnb@example.com',
        'password' => 'qwerty321',
        'activated' => 1
    ));

    // Находим группу администраторы
    $group = Sentry::findGroupByName('Administrators');

    // Назначаем его в группу администраторы
    $user->addGroup($group);
});*/

Route::get('/addItem', array('uses' => 'ItemToPlaylistController@addItemTooPlaylist'));


// Главная страница
Route::get('/', array('as' => 'home', 'uses' => 'HomeController@showHome'));

/*
Route::get('/migrate', function() {
    return DB::table('videos')
            ->where('subcategory_id', 6)
            ->update(array('subcategory_id' => 7));
});
*/

// Короткие ссылки на контент
Route::get('/{type}/{id}', function($type, $id) {
    switch ($type) {
        case 'm':
            $contentClass = 'Music';
            break;

        case 'v':
            $contentClass = 'Video';
            break;

        case 'e':
            $contentClass = 'MusicEvent';
            break;
            
        default:
            $contentClass = NULL;
            break;
    }

    // Корректно отвечаем на неправильные URL
    if (!$contentClass) {
        return App::abort(404);
    }

    $content = $contentClass::findOrFail($id);

    return Redirect::to($content->getUrl());

})->where(array('type' => '\w', 'id' => '\d+'));

// Музыка
// Список треков
Route::get('/music', array('as' => 'music', 'uses' => 'MusicController@showMusic'));

// Трек
Route::get('/music/track/{slug}', array('as' => 'music/track', 'uses' => 'MusicController@showTrack'));

// Попап-проигрыватель
Route::post('/music/track/{slug}', 'MusicController@getTrack');

// Подгрузка списка треков по view more
Route::post('/music/page/{pageNumber}', 'MusicController@getPage');

// Переадресация на список треков при попытке открыть view more в новом окне
Route::get('/music/page/{pageNumber}', function () { return Redirect::route('music'); });

// Список треков соответствующих категории
Route::get('/music/{subSlug}', 'MusicController@showSubMusic');

// Подгрузка списка треков соответствующих категории по view more
Route::post('/music/{subSlug}/{pageNumber}', 'MusicController@getSubPage');

// Переадресация на список треков при попытке открыть view more в новом окне
Route::get('/music/{subSlug}/{pageNumber}', function () { return Redirect::route('music'); });


// Видео
// Список клипов
Route::get('/videos', array('as' => 'videos', 'uses' => 'VideoController@showVideo'));

// Клип
Route::get('/videos/clip/{slug}', array('as' => 'videos/clip', 'uses' => 'VideoController@showClip'));

// Попап-проигрыватель
Route::post('/videos/clip/{slug}', 'VideoController@getClip');

// Подгрузка списка треков по view more
Route::post('/videos/page/{pageNumber}', 'VideoController@getPage');

// Переадресация на список клипов при попытке открыть view more в новом окне
Route::get('/videos/page/{pageNumber}', function () { return Redirect::route('videos'); });

// Список клипов соответствующих категории
Route::get('/videos/{subSlug}', 'VideoController@showSubVideo');

// Подгрузка списка клипов соответствующих категории по view more
Route::post('/videos/{subSlug}/{pageNumber}', 'VideoController@getSubPage');

// Переадресация на список клипов при попытке открыть view more в новом окне
Route::get('/videos/{subSlug}/{pageNumber}', function () { return Redirect::route('videos'); });

// Мероприятия
// Список мероприятий
Route::get('/events', array('as' => 'events', 'uses' => 'MusicEventController@showEvents'));

// Мероприятие
Route::get('/events/{slug}', array('as' => 'events/event', 'uses' => 'MusicEventController@showEvent'));

// Мероприятия в попапе
Route::post('/events/{slug}', 'MusicEventController@getEvent');

// Подгрузка списка мероприятий по view more
Route::post('/events/page/{pageNumber}', 'MusicEventController@getPage');

// Переадресация на список мероприятий при попытке открыть view more в новом окне
Route::get('/events/page/{pageNumber}', function () { return Redirect::route('events'); });


// Поиск
// Страница результатов поиска
Route::get('/search', array('as' => 'search', 'uses' => 'SearchController@showResult'));

// Подгрузка результатов поиска
Route::post('/search/page/{pageNumber}', 'SearchController@getPage');

// Переадресация на начальную страницу результатов поиска при попытке открыть view more в новом окне
Route::get('/search/page/{pageNumber}', function () { return Redirect::route('search'); });

// Контрольная панель
Route::group(array('before' => 'controlPanel.auth'), function ()
{
    Route::get('/adminka', array('as' => 'controlPanel', 'uses' => 'ControlPanel\ControlPanelController@showDashboard'));
    Route::post('/adminka/api', array('as' => 'controlPanelRetrieveData', 'uses' => 'ControlPanel\ControlPanelController@retrieveData'));

    // Выход
    Route::get('/adminka/logout', array('as' => 'controlPanelLogout', 'uses' => 'ControlPanel\AuthController@logout'));
    Route::get('/logout', array('uses' => 'ControlPanel\AuthController@Logout'));

    // Создание контента
    Route::post('/adminka/create/track', array('as' => 'controlPanelCreateTrack', 'uses' => 'ControlPanel\MusicController@create'));
    Route::post('/adminka/create/video', array('as' => 'controlPanelCreateVideo', 'uses' => 'ControlPanel\VideoController@create'));
    Route::post('/adminka/create/event', array('as' => 'controlPanelCreateEvent', 'uses' => 'ControlPanel\EventController@create'));

    // Обновление, удаление контента
    Route::post('/adminka/update', array('as' => 'controlPanelUpdateData', 'uses' => 'ControlPanel\ControlPanelController@updateData'));
    Route::post('/adminka/delete', array('as' => 'controlPanelDeleteData', 'uses' => 'ControlPanel\ContentGroupController@deleteData'));

    // Статус публикации
    Route::post('/adminka/publish', array('as' => 'controlPanelPublishData', 'uses' => 'ControlPanel\ContentGroupController@publishData'));
    Route::post('/adminka/unpublish', array('as' => 'controlPanelUnpublishData', 'uses' => 'ControlPanel\ContentGroupController@unpublishData'));

    // Управление метатегами
    Route::get('/adminka/metatags', array('as' => 'controlPanelMetatags', 'uses' => 'ControlPanel\MetatagsController@showMetatags'));
    Route::post('/adminka/metatags', array('as' => 'controlPanelRetrieveMetatags', 'uses' => 'ControlPanel\MetatagsController@retrieveTags'));
    Route::post('/adminka/create/metatags', array('as' => 'controlPanelCreateMetatags', 'uses' => 'ControlPanel\MetatagsController@createMetatags'));
    Route::post('/adminka/update/metatags', array('as' => 'controlPanelUpdateMetatags', 'uses' => 'ControlPanel\MetatagsController@updateMetatags'));
    Route::post('/adminka/delete/metatags', array('as' => 'controlPanelDeleteMetatags', 'uses' => 'ControlPanel\MetatagsController@deleteMetatags'));

    // Управление личной информацией
    Route::get('/adminka/myinfo', array('uses' => 'InfoController@showMyInformation'));
    Route::post('/adminka/myinfo', array('as' => 'changeInfo','uses' => 'InfoController@changeInfo'));

    // Управление информацией пользователей
    Route::get('/adminka/users', array('uses' => 'ControlPanel\ControlPanelController@usersInfo'));
    Route::post('/adminka/editUser', array('uses' => 'ControlPanel\ControlPanelController@editUser'));

});

// Вход в контрольную панель
Route::group(array('before' => 'controlPanel.guest'), function ()
{
    Route::get('/login', array('as' => 'controlPanel.login', 'uses' => 'ControlPanel\AuthController@showLoginForm'));
    Route::get('/adminka/login', array('as' => 'controlPanel.adminkaLogin', 'uses' => 'ControlPanel\AuthController@showLoginForm'));
    Route::post('/login', array('as' => 'controlPanel.login', 'uses' => 'ControlPanel\AuthController@login'));
    Route::post('/loginCheck', array('uses' => 'ControlPanel\AuthController@loginCheck'));
});


Route::get('radio', function()
{
    return View::make('pages.radio');
});

// Вход зарегистрированного пользователя
Route::group(array('before' => 'controlPanel.auth'), function () {

});

// Регистрация нового пользователя
Route::group(array('before' => 'controlPanel.guest'), function () {
    Route::post('/register', array('as' => 'controlPanel.register', 'uses' => 'ControlPanel\AuthController@register'));
});

// Функционал зарегистрированного пользователя
Route::group(array('before' => 'controlPanel.user'), function () {

    // Выход
    Route::get('/logout', array('uses' => 'ControlPanel\AuthController@Logout'));
    Route::post('/', array('before' => 'csrf', 'uses' => 'ControlPanel\AuthController@login'));

    // Управление личной информацией
    Route::get('/myinfo', array('uses' => 'InfoController@showMyInformation'));
    Route::post('/myinfo', array('as' => 'changeInfo','uses' => 'InfoController@changeInfo'));

    // Избранные
    Route::get('/favorites', array('uses' => 'FavoritesController@showFavorites'));

    // Управление плэйлистами
    Route::get('/playlists', array('uses' => 'PlaylistsController@showPlaylists'));
    Route::post('/addPlaylist', array('as' => 'addPlaylist', 'uses' => 'PlaylistsController@addPlaylist'));
    Route::post('/getPlaylist', array('as' => 'getPlaylist', 'uses' => 'PlaylistsController@getPlaylist'));
    Route::post('/musicPlaylistAjax/{slug}', array('uses' => 'PlaylistsController@getMusicPlaylistAjax'));

    // Отметить лайк
    // Для музыкального трэка
    Route::post('/musiclike', array('uses' => 'MusicController@setLike'));
    // Для видео клипа
    Route::post('/videolike', array('uses' => 'VideoController@setLike'));
    // Для видео клипа
    Route::post('/eventlike', array('uses' => 'MusicEventController@setLike'));

    // Изменение порядка песни в плэйлисте
    Route::get('/setSort', array('uses' => 'ItemToPlaylistController@setSort'));

    // Оставить коментарий для трэка
    Route::post('/addComment', array('as' => 'addComment', 'uses' => 'CommentController@addComment'));

    // Показать комментарии комментарии
    Route::post('/showComments', array('as' => 'showComments', 'uses' => 'CommentController@showComments'));

    // Страница контактов
    //Route::get('/mycontacts', array('uses' => 'VideoController@setLike'));
    
    Route::get('/test123', function(){


    });
});