<?php
/**
 * Отображение страницы Playlists с возможностью управления.
 */

class PlaylistsController extends BaseController {

    /**
     * Количество объектов, выводимых на странице.
     * @var integer
     */
    public $step = 8;


    /**
     * Срок годности информации от SC. По истечении этого срока
     * SC-описание трека будет обновлено.
     * @var integer
     */
    public $expiration = 3;

    /**
     * Вывод формы страницы About.
     * @return View
     */
    public function showPlaylists() {

        $metaTags = MetaTags::generateTags();

        $playlists = Playlist::where('user_id', '=', Sentry::getUser()->id)
            ->get();

        $categories = Subcategory::all();

        return View::make('pages/playlists')->with(compact('metaTags', 'playlists', 'categories'));

    }

    /**
     * Формирование элементов плэйлиста
     * @return mixed
     */
    public function getPlayList() {
        $data = Input::all();
        if ($data['name'] == "ALL") {
            $musics = Music::join('itemtoplaylists', 'itemtoplaylists.item_id', '=', 'music.id')
                ->join('playlists', 'playlists.id', '=', 'itemtoplaylists.playlist_id')
                ->where('playlists.user_id', '=', Sentry::getUser()->id)
                ->get()
                ->sortBy('sort');;
        } else {
            $musics = Music::join('itemtoplaylists', 'itemtoplaylists.item_id', '=', 'music.id')
                ->join('playlists', 'playlists.id', '=', 'itemtoplaylists.playlist_id')
                ->where('playlists.user_id', '=', Sentry::getUser()->id)
                ->where('playlists.name', '=', $data['name'])
                ->get()
                ->sortBy('sort');
        }

        $view = View::make('pages/playlistsAjax')
            ->with(compact('musics'))
            ->render();

        return Response::json(compact('view'));
    }

    /**
     * Добавление плэйлиста
     * @return Exception
     */
    public function addPlaylist() {
        $data = Input::all();

        // Проверка на пустое имя прэйлиста
        if($data['name'] == '') {
            return Response::json('Empty name.');
        }

        // Проверка на наличие плэйлиста с таким именем у данного пользователя
        $playlist = Playlist::where('name', '=', $data['name'])
            ->where('user_id', '=', Sentry::getUser()->id)
            ->first();
        if($playlist) {
            return Response::json('Playlist with name "' . $data['name'] . '" already exists.');
        }

        // Создаем и сохраняем новый плэйлист
        $playlist = new Playlist;
        $playlist->name = $data['name'];
        $playlist->user_id = Sentry::getUser()->id;
        try {
            $playlist->save();
        } catch (Exception $e) {
            return $e;
        }
        return Response::json(true);
    }

    /**
     * Редактирование плэйлиста
     * @return Exception
     */
    public function editPlaylist() {
        $data = Input::all();
        $playlist = Playlist::find($data['id']);
        // Изменение названия плэйлиста
        if($data['name']) { $playlist->name = $data['name']; }
        // Изменение статуса приватности плэйлиста
        if($data['status']) { $playlist->status = $data['status']; }
        try {
            // Сохранение изменений
            $playlist->save();
        } catch (Exception $e) {
            return $e;
        }
    }

    public function getMusicPlaylistAjax($slug) {

        $track = Music::where('slug', $slug)->firstOrFail();

        // Прячем неопубликованные треки
        if (!$track->published) { App::abort(404); };

        $track->increment('internal_playback_count');
        $track->increment('internal_weekly_playback_count');

        // Если SC-информация о треке давно не обновлялась — обновляем её.
        if ($track->updated_at->diffInDays() > $this->expiration) {
            $track->updateSCInfo();
        }

        $relatedTracks = $track->getRelated();

        // Статус лайка для трека.
        $like = 'popup-music-likes';
        if(Sentry::check()) {
            $like_status = Like::where('user_id', '=', Sentry::getUser()->id)
                ->where('type', '=', 'music')
                ->where('item_id', '=', $track->id)
                ->first();
            if(!empty($like_status)) {
                $like = 'popup-music-liked';
            }
        }

        return View::make('music/musicPlaylistAjax')->with(compact('track', 'like', 'relatedTracks'));
    }

}