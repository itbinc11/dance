<?php
/**
 * Отображение результатов поиска, с возможностью фильтрации по типу.
 */

class SearchController extends BaseController {

    /**
     * Количество объектов, выводимых на странице.
     * @var integer
     */
    public $step = 8;


    /**
     * Отобразить результаты поиска.
     * @return View
     */
    public function showResult()
    {
        $q = Input::get('q', false);

        $metaTags = MetaTags::generateTags();

        // игнорируем пустой поисковый запрос
        if(!$q) {
            return View::make('search/search')->with(compact(
                'metaTags'
            ));
        };

        $result = $this->isSearchByTag($q) ? 
            $this->searchByTag($q):
            $this->search($q);

        return View::make('search/search')->with(compact(
            'result',
            'metaTags'
        ));
    }


    /**
     * Отобразить контент страницы результатов по нажатию «view more».
     * @param  int $page номер страницы результатов
     * @return View
     */
    public function getPage($page)
    {
        $q = Input::get('q', false);

        $result = $this->isSearchByTag($q) ?
            $this->searchByTag($q, $page):
            $this->search($q, $page);

        // если выводить нечего, пропускаем генерацию view
        if (!$result->count()) { return ''; };

        // Если «найдено» < «выводится на странице», выставляем флаг 
        // для шаблона: не выводить кнопку «view more».
        $listEnds = $result->count() < $this->step;

        $type = Input::get('type', false);

        return $type == 'video' ?
            View::make('search/videoSearchPageAjax')->with(compact('page', 'result', 'listEnds')):
            View::make('search/musicSearchPageAjax')->with(compact('page', 'result', 'listEnds'));
    }


    /**
     * Проверить, осуществляется ли поиск по тегу. Для поиска по тегу, 
     * его название, в поисковом запросе, должно быть заключено в кавычки.
     * @param  string  $q поисковый запрос пользователя
     * @return boolean
     */
    private function isSearchByTag($q)
    {
        return (bool) preg_match('/^".+"$/', $q);
    }


    /**
     * Найти объекты, отмеченные соответствующим тегом. Поддерживается пагинация.
     * @param  string $q поисковый запрос пользователя
     * @return Collection
     */
    private function searchByTag($q, $page = false)
    {
        // Поиск регистронезависим. Осуществляется по отдельному столбцу БД, 
        // текстовое содержимое которого переведено в нижний регистр.
        $searchTag = strtolower(trim($q, '"'));

        $tag = Tag::where('search', $searchTag)->first();

        // игнорируем несуществующие теги
        if (!$tag) { return false; };

        $type = Input::get('type', false);

        $query = $type == 'video' ?
            $tag->video():
            $tag->music();

        $query->where('published', 1)
            ->orderBy('created_at', 'desc')
            ->orderBy('id')
            ->take($this->step)
            ->with('tags');

        // добавляем смещение согласно пагинации, смещение <= 1 — невалидно
        if ($page > 1) { $query->skip($this->step*($page-1)); };

        return $query->get();
    }


    /**
     * Найти объекты содержащие в названии хотя бы одно из слов запроса.
     * Поддерживается пагинация и частичное вхождение.
     * @param  string $q поисковый запрос пользователя
     * @return Collection
     */
    private function search($q, $page = false)
    {
        $type = Input::get('type', false);

        $query = $type == 'video' ?
            Video::query():
            Music::query();

        $query->where('published', 1)
            ->orderBy('created_at', 'desc')
            ->orderBy('id')
            ->take($this->step)
            ->with('tags');

        $searchTerms = explode(' ', $q);

        $query->where(function($query) use($searchTerms)
        {
            foreach ($searchTerms as $term) {
                // Поиск регистронезависим. Осуществляется по отдельному столбцу БД, 
                // текстовое содержимое которого переведено в нижний регистр.
                $query->orWhere('search', 'LIKE', '%'. (strtolower($term)) .'%');
            }
        });

        // добавляем смещение согласно пагинации, смещение <= 1 — невалидно
        if ($page > 1) { $query->skip($this->step*($page-1)); };

        return $query->get();
    }

}