<?php
namespace ControlPanel;

use Config;
use Exception;
use Input;
use Log;
use Music;
use Response;
use Sentry;
use URLify;
use Validator;

/**
 * Контроллер треков в админ.панели.
 */
class MusicController extends ContentController {

    /**
     * Добавить в БД новый трек.
     * @return Response
     */
    public function create()
    {
        // Данные из формы и Soundcloud-описание трека.
        $input = Input::get('form');
        $trackInfo = Input::get('tempObjectData');

        $track = new Music(array(
            // Данные из формы
            'title' => $input['title'],
            'description' => $input['description'],
            'subcategory_id' => $input['subcategory_id'],
            'crosspublish' => isset($input['crosspublish']),

            // Данные от Soundcloud API
            'api_url' => $trackInfo['uri'],
            'artwork_url' => $trackInfo['artwork_url'],
            'download_url' => array_key_exists('download_url', $trackInfo) ? $trackInfo['download_url'] : NULL,
            'duration' => $trackInfo['duration'],
            'favoritings_count' => empty($trackInfo['favoritings_count']) ? 0 : $trackInfo['favoritings_count'],
            'playback_count' => empty($trackInfo['playback_count']) ? 0 : $trackInfo['playback_count'],
            'purchase_url' => $trackInfo['purchase_url'],
            'stream_url' => $trackInfo['stream_url'],
            'url' => $trackInfo['permalink_url'],
            'waveform_url' => $trackInfo['waveform_url'],

            // Предустановленные значения
            'user_id' => Sentry::getUser()->id, // пользователь добавивший трек
            'published' => 0,

            // Сгенерированные данные
            'search' => strtolower($input['title']),
            'slug' => URLify::filter($input['title']),
        ));

        // Добавляем ссылки из профиля пользователя.
        if ($webProfiles = $this->getUserWebsiteLink($trackInfo['user_id'])) {
            $track->fill($webProfiles);
        }

        // Валидируем трек.
        if (!$track->validate()) {
            return Response::json(array(
                'status' => false,
                'errors' => $track->getValidationErrorMessages()
            ));
        }

        // Сохраняем.
        try {
            $track->save();
            $result = array('status' => true);
        } catch (Exception $e) {
            $result = array(
                'status' => false,
                'errors' => array('save' => 'An error occured while saving track.')
            );
            Log::error("An error occured while saving track: {$e->getMessage()}");
        }

        // Добавляем теги.
        $track->tags()->sync($this->parseTags($input['tags']));

        // Ставим в очередь запланированной публикации.
        $isScheduled = $input['pdate'] == $input['hidden-pdate'];
        if ($isScheduled) {
            $this->queue($track);
        } else {
            $this->queue($track, $input['pdate']);
        }

        Log::info("Track added by “" . Sentry::getUser()->username . "”: {$track->title}");
        return Response::json($result);
    }


    /**
     * Обновить информацию о треке.
     * @param  integer $id
     * @param  array $formData
     * @return bool
     */
    public function update($id, $formData)
    {
        $track = Music::find($id);
        if (!$track) {
            return Response::json(array(
                'status' => false,
                'errors' => array('update' => 'Edited track was not found.')
            ));
        }
        
        // Обновляем параметры.
        $track->fill(array(
            'title' => $formData['title'],
            'slug' => URLify::filter($formData['title']),
            'description' => $formData['description'],
            'subcategory_id' => $formData['category'],
            'search' => mb_strtolower($formData['title'])
        ));

        // Валидируем трек.
        if (!$track->validate()) {
            return Response::json(array(
                'status' => false,
                'errors' => $track->getValidationErrorMessages()
            ));
        }

        // Актуализируем теги.
        $track->tags()->sync($this->parseTags($formData['tags']));

        // Сохраняем.
        try {
            $track->save();
            $result = array('status' => true);
        } catch (Exception $e) {
            $result = array(
                'status' => false,
                'errors' => array('save' => 'An error occured while saving track.')
            );
            Log::error("An error occured while saving track: {$e->getMessage()}");
        }

        return Response::json($result);
    }


    /**
     * Получить ссылки на соц.профили пользователя.
     * @param  integer $userId
     * @return array
     */
    protected function getUserWebsiteLink($userId)
    {
        try {
            $url = sprintf("http://api.soundcloud.com/users/%d/web-profiles.json?client_id=%s", $userId, Config::get('app.apiKey'));

            $ch = curl_init($url);

            curl_setopt_array($ch, array(
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_CONNECTTIMEOUT => 2,
                CURLOPT_RETURNTRANSFER => true
            ));

            $response = curl_exec($ch);

            if (curl_errno($ch)) {
                throw new Exception('Could not retrieve the data.');
            }

            $response = json_decode($response);

            if ($response === null) {
                throw new Exception("Error parsing data.");
            }

            if (isset($response->errors)) {
                throw new Exception('API response: ' . $response->errors[0]->error_message);
            }

            $links = array();

            foreach ($response as $link) {
                switch ($link->service) {
                    case 'twitter':
                        $links["user_twitter"] = $link->url;
                        break;

                    case 'facebook':
                        $links["user_facebook"] = $link->url;
                        break;

                    case 'personal':
                        $links["user_personal"] = $link->url;
                        break;
                    
                    default:
                        // do nothing
                        break;
                }
            }

            if (empty($links)) {
                throw new Exception('Empty web-profiles.');
            }

            return $links;

        } catch (Exception $e) {
            Log::info('MusicController::getUserWebsiteLink error: ' . $e->getMessage());
            return false;
        }
    }

}