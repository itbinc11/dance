<?php
namespace ControlPanel;

use DateTime;
use DB;
use Input;
use Music;
use MusicEvent;
use Response;
use Subcategory;
use Video;
use View;
use User;
use Sentry;
use Eloquent;
use Comment;

/**
 * Контроллер админ панели.
 */

class ControlPanelController extends \BaseController {

    /**
     * Количество объектов, выводимых на странице контрольной панели.
     * @var integer
     */
    public $step = 10;

    
    /**
     * Отобразить панель управления.
     * @return View
     */
    public function showDashboard()
    {
        $categories = Subcategory::all();
        return View::make('controlPanel.index')->with(compact('categories'));
    }


    /**
     * Обновить описание объекта.
     * @return bool
     */
    public function updateData()
    {
        $id = Input::get('id', false);
        $form = Input::get('form', false);

        // Тип контента нужен для определения используемого контроллера.
        if (!isset($form['type'])) {
            return Response::json(array('status' => false));
        }

        switch ($form['type']) {
            case 'music':
                $controller = new MusicController();
                return $controller->update($id, $form);
                break;

            case 'video':
                $controller = new VideoController();
                return $controller->update($id, $form);
                break;

            case 'event':
                $controller = new EventController();
                return $controller->update($id, $form);
                break;
            
            default:
                return Response::json(array('status' => false));
                break;
        }
    }


    /**
     * Диспетчеризация запроса и возврат сгенерированного html-списка 
     * редактируемых объектов, соответствующих входным параметрам.
     * @return View controlPanel/cpanelAjax
     */
    public function retrieveData()
    {
        // статус публикации: опубликовано/неопубликовано
        $published = (bool) Input::get('publishedFilter', true);

        // фильтр отображаемого контента
        $type = Input::get('typeFilter', 'all');

        // фильтр по подкатегории. может быть строкой или экземпляром-подкатегории
        $sub = Input::get('categoryFilter', 'all');
        if ($type !== 'users' && $type !== 'comments') {
            $sub = $sub != 'all' ?
                Subcategory::whereSlug($sub)->first() :
                'all';
        }

        // порядковый номер запрашиваемой страницы
        $page = (integer) Input::get('currentPage', 1);

        // из-за проблем с ORM в версиях Laravel старше 4.0, для вывода смешанного 
        // контента использован отдельный метод с «ручной» генерацией sql-запросов
        if ($type == 'all') {
            $result = $this->queryAll($published, $sub, $page);
        } else {
            $result = $this->queryType($type, $published, $sub, $page);
        };

        // список подкатегорий используется для генерации фильтра в UI
        $categories = Subcategory::all();

        switch ($type) {
            case 'users':
                $view = View::make('controlPanel/usersCpanelAjax')
                    ->with(compact('result', 'categories'))
                    ->render();
                break;

            case 'comments':
                $view = View::make('controlPanel/commentsCpanelAjax')
                    ->with(compact('result', 'categories'))
                    ->render();
                break;

            default:
                $view = View::make('controlPanel/cpanelAjax')
                    ->with(compact('result', 'categories'))
                    ->render();
                break;
        }

        // расчёт кол-ва страниц для отображения
        $totalCount = $this->totalCount($type, $published, $sub);
        $totalPages = ceil($totalCount/$this->step);

        return Response::json(compact('view', 'totalPages'));
    }


    /**
     * Извлекает из БД объекты всех типов, соответствующие входным параметрам.
     * @param  bool                 $published  статус публикации
     * @param  string|Subcategory   $sub        подкатегория запрашиваемого контента
     * @param  integer              $page       порядковый номер отображаемой страницы
     * @return array
     */
    private function queryAll($published, $sub, $page)
    {
        $query = $sub == 'all' ? 
            sprintf(
                'select id, created_at, "music" as type from music where published = %1$d
                union all
                select id, created_at, "video" as type from videos where published = %1$d
                union all
                select id, created_at, "event" as type from music_events where published = %1$d
                order by created_at desc, id desc
                limit %2$d offset %3$d',
                $published,
                $this->step,
                $this->step*($page-1)
            ):
            sprintf(
                'select id, created_at, "music" as type from music where published = %1$d and subcategory_id = %3$d
                union all
                select id, created_at, "video" as type from videos where published = %1$d and subcategory_id = %3$d
                order by created_at desc, id desc
                limit %2$d offset %4$d',
                $published,
                $this->step,
                $sub->id,
                $this->step*($page-1)
            );

        $result = DB::select($query);
        $collection = array();

        foreach ($result as $item) {
            switch ($item->type) {
                case 'music':
                    $collection[] = Music::whereId($item->id)->with('tags')->first();
                    break;
                
                case 'video':
                    $collection[] = Video::whereId($item->id)->with('tags')->first();
                    break;

                default:
                    $collection[] = MusicEvent::whereId($item->id)->first();
                    break;
            }
        }

        return $collection;
    }


    /**
     * Извлекает из БД объекты одного типа, соответствующие входным параметрам.
     * @param  string               $type      тип запрашиваемого контента
     * @param  bool                 $published статус публикации
     * @param  string|Subcategory   $sub       подкатегория запрашиваемого контента
     * @param  integer              $page      порядковый номер отображаемой страницы
     * @return Collection
     */
    private function queryType($type, $published, $sub, $page)
    {
        switch ($type) {
            case 'music':
                $query = Music::query()->with('tags');
                break;

            case 'video':
                $query = Video::query()->with('tags');
                break;

            case 'comments':
                if ($sub == 'all') {
                    $query_music = DB::table('comments')
                        ->select(
                            'comments.id as comment_id',
                            'comments.user_id as comment_user_id',
                            'comments.item_id as comment_item_id',
                            'comments.item_type as comment_item_type',
                            'comments.comment as comment_comment',
                            'comments.created_at as comment_created_at',
                            'comments.updated_at as comment_updated_at',
                            'users.id as user_id',
                            'users.email as user_email',
                            'users.first_name as user_first_name',
                            'users.last_name as user_last_name',
                            'users.username as user_username',
                            'users.avatar as user_avatar',
                            'users.about as user_about',
                            'users.created_at as user_created_at',
                            'users.updated_at as user_updated_at',
                            'music.title as item_title',
                            'music.slug as item_slug',
                            'music.url as item_url',
                            'music.description as item_description',
                            'music.user_id as item_user_id',
                            'music.published as item_published',
                            'music.created_at as item_created_at',
                            'music.updated_at as item_updated_at'

                        )
                        ->join('users', 'users.id', '=', 'comments.user_id')
                        ->join('music', 'music.id', '=', 'comments.item_id')
                        ->where('item_type', 'music');

                    $query_video = DB::table('comments')
                        ->select(
                            'comments.id as comment_id',
                            'comments.user_id as comment_user_id',
                            'comments.item_id as comment_item_id',
                            'comments.item_type as comment_item_type',
                            'comments.comment as comment_comment',
                            'comments.created_at as comment_created_at',
                            'comments.updated_at as comment_updated_at',
                            'users.id as user_id',
                            'users.email as user_email',
                            'users.first_name as user_first_name',
                            'users.last_name as user_last_name',
                            'users.username as user_username',
                            'users.avatar as user_avatar',
                            'users.about as user_about',
                            'users.created_at as user_created_at',
                            'users.updated_at as user_updated_at',
                            'videos.title as item_title',
                            'videos.slug as item_slug',
                            'videos.url as item_url',
                            'videos.description as item_description',
                            'videos.user_id as item_user_id',
                            'videos.published as item_published',
                            'videos.created_at as item_created_at',
                            'videos.updated_at as item_updated_at'

                        )
                        ->join('users', 'users.id', '=', 'comments.user_id')
                        ->join('videos', 'videos.id', '=', 'comments.item_id')
                        ->where('item_type', 'videos');

                    $query_events = DB::table('comments')
                        ->select(
                            'comments.id as comment_id',
                            'comments.user_id as comment_user_id',
                            'comments.item_id as comment_item_id',
                            'comments.item_type as comment_item_type',
                            'comments.comment as comment_comment',
                            'comments.created_at as comment_created_at',
                            'comments.updated_at as comment_updated_at',
                            'users.id as user_id',
                            'users.email as user_email',
                            'users.first_name as user_first_name',
                            'users.last_name as user_last_name',
                            'users.username as user_username',
                            'users.avatar as user_avatar',
                            'users.about as user_about',
                            'users.created_at as user_created_at',
                            'users.updated_at as user_updated_at',
                            'music_events.title as item_title',
                            'music_events.slug as item_slug',
                            'music_events.url as item_url',
                            'music_events.description as item_description',
                            'music_events.user_id as item_user_id',
                            'music_events.published as item_published',
                            'music_events.created_at as item_created_at',
                            'music_events.updated_at as item_updated_at'

                        )
                        ->join('users', 'users.id', '=', 'comments.user_id')
                        ->join('music_events', 'music_events.id', '=', 'comments.item_id')
                        ->where('item_type', 'music');

                    $query = $query_music->union($query_video)->union($query_events);

                    $query->orderBy('comment_created_at', 'desc')
                        ->skip($this->step*($page-1))
                        ->take($this->step);

                    return $query->get();
                } else {
                    if($sub == 'event') { $sub2 = 'music_events'; } else { $sub2 = $sub; }
                    $query = Comment::query()
                        ->select(
                            'comments.id as comment_id',
                            'comments.user_id as comment_user_id',
                            'comments.item_id as comment_item_id',
                            'comments.item_type as comment_item_type',
                            'comments.comment as comment_comment',
                            'comments.created_at as comment_created_at',
                            'comments.updated_at as comment_updated_at',
                            'users.id as user_id',
                            'users.email as user_email',
                            'users.first_name as user_first_name',
                            'users.last_name as user_last_name',
                            'users.username as user_username',
                            'users.avatar as user_avatar',
                            'users.about as user_about',
                            'users.created_at as user_created_at',
                            'users.updated_at as user_updated_at',
                            $sub2.'.id as item_id',
                            $sub2.'.title as item_title',
                            $sub2.'.slug as item_slug',
                            $sub2.'.url as item_url',
                            $sub2.'.description as item_description',
                            $sub2.'.user_id as item_user_id',
                            $sub2.'.published as item_published',
                            $sub2.'.created_at as item_created_at',
                            $sub2.'.updated_at as item_updated_at'
                        )
                        ->join('users', 'users.id', '=', 'comments.user_id')
                        ->join($sub2, $sub2.'.id', '=', 'comments.item_id')
                        ->where('comments.item_type', $sub);
                }

                $query->orderBy('comment_id', 'desc')
                    ->skip($this->step*($page-1))
                    ->take($this->step);

                $result = $query->get();
                return $result;
                break;

            case 'users':
                if ($sub == 'all') {
                    $query = User::query()
                        ->join('users_groups', 'users_groups.user_id', '=', 'users.id')
                        ->join('groups', 'groups.id', '=', 'users_groups.group_id')
                        ->select(
                        'users.id as user_id',
                        'users.email as user_email',
                        'users.permissions as user_permissions',
                        'users.activated as user_activated',
                        'users.activated_at as user_activated_at',
                        'users.last_login as user_last_login',
                        'users.first_name as user_first_name',
                        'users.last_name as user_last_name',
                        'users.username as user_username',
                        'users.created_at as user_created_at',
                        'users.updated_at as user_updated_at',
                        'users.avatar as user_avatar',
                        'users.about as user_about',
                        'groups.name as group_name'
                    );

                } else {
                    $query = User::select(
                        'users.id as user_id',
                        'users.email as user_email',
                        'users.permissions as user_permissions',
                        'users.activated as user_activated',
                        'users.activated_at as user_activated_at',
                        'users.last_login as user_last_login',
                        'users.first_name as user_first_name',
                        'users.last_name as user_last_name',
                        'users.username as user_username',
                        'users.created_at as user_created_at',
                        'users.updated_at as user_updated_at',
                        'users.avatar as user_avatar',
                        'users.about as user_about',
                        'groups.name as group_name'
                    )
                        ->join('users_groups', 'users_groups.user_id', '=', 'users.id')
                        ->join('groups', 'groups.id', '=', 'users_groups.group_id')
                        ->where('groups.name', '=', $sub);
                }

                $query->orderBy('user_created_at', 'desc')
                    ->orderBy('user_id', 'desc')
                    ->skip($this->step*($page-1))
                    ->take($this->step);

                $result = $query->get();

                return $result;
                break;
            
            default:
                $query = MusicEvent::query();
                break;
        }

        $query->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->where('published', $published)
            ->skip($this->step*($page-1))
            ->take($this->step);

        // фильтрация по подкатегориям работает только для музыки и клипов
        if ($sub != 'all' and $type != 'events' and $type != 'comments') {
            $query->where('subcategory_id', $sub->id);
        }

        $result = $query->get();

        return $result;
    }


    /**
     * Подсчитать количество объектов, соответствующих входным данным.
     * @param  string               $type      тип запрашиваемого контента
     * @param  bool                 $published статус публикации
     * @param  string|Subcategory   $sub       подкатегория запрашиваемого контента
     * @return integer
     */
    private function totalCount($type, $published, $sub)
    {
        // если тип all
        // посчитать по очереди все таблицы
        if ($type == 'all') {
            $musicQuery = Music::where('published', $published);
            if ($sub != 'all') { $musicQuery->where('subcategory_id', $sub->id); };
            $count = $musicQuery->count();

            $videoQuery = Video::where('published', $published);
            if ($sub != 'all') { $videoQuery->where('subcategory_id', $sub->id); };
            $count += $videoQuery->count();

            if ($sub == 'all') { $count += MusicEvent::where('published', $published)->count(); };

        } else {

            switch ($type) {
                case 'music':
                    $musicQuery = Music::where('published', $published);
                    if ($sub != 'all') { $musicQuery->where('subcategory_id', $sub->id); };
                    $count = $musicQuery->count();
                    break;

                case 'video':
                    $videoQuery = Video::where('published', $published);
                    if ($sub != 'all') { $videoQuery->where('subcategory_id', $sub->id); };
                    $count = $videoQuery->count();
                    break;

                case 'users':
                    $usersQuery = User::join('users_groups', 'users_groups.user_id', '=', 'users.id')
                            ->join('groups', 'groups.id', '=', 'users_groups.group_id');
                    if ($sub != 'all') { $usersQuery->where('name', $sub); };
                    $count = $usersQuery->count();
                    break;

                case 'comments':
                    $commentQuery = Comment::query();
                    if ($sub !== 'all') { $commentQuery->where('item_type', $sub); };
                    $count = $commentQuery->count();
                    break;

                default:
                    $count = MusicEvent::where('published', $published)->count();
                    break;
            }
        }

        $count = $count ? $count : 1;

        return $count;
    }

    public function usersInfo() {
        $categories = Subcategory::all();
        return View::make('controlPanel.users')->with(compact('categories'));
    }

    public function editUser() {
        $data = Input::all();
        $user = Sentry::findUserById($data['id']);
        if(!empty($data['username'])) { $user->username = $data['username']; }
        if(!empty($data['first_name'])) { $user->first_name = $data['first_name']; }
        if(!empty($data['last_name'])) { $user->last_name = $data['last_name']; }
        if(!empty($data['email'])) { $user->email = $data['email']; }
        if(!empty($data['about'])) { $user->about = $data['about']; }
        try {
            $user->save();
        } catch (Exception $e) {
            return $e;
        }

        return Response::json(true);
    }


}