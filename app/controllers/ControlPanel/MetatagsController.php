<?php
namespace ControlPanel;

use Input;
use MetaTags;
use Redirect;
use Response;
use Validator;
use View;

/**
 * Контроллер Метатегов в админ.панели.
 */
class MetatagsController extends \BaseController {

    /**
     * Отобразить страницу управления метатегами.
     * @return View
     */
    public function showMetatags()
    {
        $metatags = MetaTags::all();
        return View::make('controlPanel.metatags')->with(compact('metatags'));
    }


    /**
     * Добавить метатеги.
     * @return Redirect
     */
    public function createMetatags()
    {
        $input = Input::all();

        // Валидация
        $validator = Validator::make(
            $input,
            array(
                'url_pattern' => 'required|regex:/^(?!-)[a-z0-9-]+(?<!-)(\/(?!-)[a-z0-9-]+(?<!-))*(\/(?!-\.)[a-z0-9-\.]+(?<!-\.))?$/i|unique:meta_tags',
                'title' => "required"
            )
        );
        if ($validator->fails()) {
            return Redirect::route('controlPanelMetatags');
        }

        $metatags = MetaTags::create($input);
        $metatags->save();

        return Redirect::route('controlPanelMetatags');
    }


    /**
     * Получить json-список мета-тегов для отображения в панели управления.
     * @return string
     */
    public function retrieveTags()
    {
        $metatags = MetaTags::all();
        
        $view = View::make('controlPanel.metatagsAjax')
            ->with(compact('metatags'))
            ->render();

        return Response::json(compact('view'));
    }


    /**
     * Обновить метатеги.
     * @return string
     */
    public function updateMetatags()
    {
        $id = Input::get('id', false);
        $input = Input::get('form', false);

        $metatags = MetaTags::findOrFail($id);
        $metatags->fill($input);
        $result = $metatags->save();

        Response::json(array('result' => $result));
    }


    /**
     * Удалить метатеги из БД.
     * @return string
     */
    public function deleteMetatags()
    {
        $items = Input::get('items', false);

        if (empty($items)) {
            return Response::json(array(
                'result' => false,
                'message' => 'Not specified elements.'
            ));
        }

        $result = true;
        foreach ($items as $item) {
            if (!MetaTags::destroy($item['id'])) {
                $result = false;
            }
        }

        return Response::json(array('result' => $result));
    }

}