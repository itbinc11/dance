<?php
namespace ControlPanel;

use Input, View, Redirect, Sentry, Validator, Captcha, URL, Response;

/**
 * Отображение главной страницы с возможностью фильтрации по категории/жанру.
 */

class AuthController extends \BaseController {

    /**
     * Вывод формы логина.
     * @return View
     */
    public function showLoginForm() {
        if(Sentry::check()) {
            return Redirect::route('controlPanel');
        }
        return View::make('controlPanel/login');
    }

    /**
     * Аутентификация пользователя. В отличии от стандартной конфигурации 
     * Sentry, идентификатором пользователя служит не username, а email.
     * @return View|Redirect
     */
    public function login() {
        Input::flash();

        $emptyUser = Sentry::getUserProvider()->getEmptyUser();
        $user = $emptyUser->whereUsername(Input::get('username'))->first();
        if (!$user) {
            return Redirect::route('controlPanel.login');
        }
        // Проверка совпадения пароля
        $passwordMatch = $user->checkPassword(Input::get('password'));
        // Проверка на права администратора и пользователя
        $adminHasAccess = $user->hasAccess('admin');
        $userHasAccess = $user->hasAccess('user');

        if ($passwordMatch and $adminHasAccess) {
            Sentry::login($user, Input::get('remember-me', false));
            return Redirect::to(URL::previous());
        }elseif($passwordMatch and $userHasAccess) {
            Sentry::login($user, Input::get('remember-me', false));
            return Redirect::to(URL::previous());
        } else {
            return Redirect::route('controlPanel.login');
        }
    }

    /**
     * Проверка на правильность введенных данных для логина
     * @return mixed
     */
    public function loginCheck() {
        Input::flash();
        $emptyUser = Sentry::getUserProvider()->getEmptyUser();
        $user = $emptyUser->whereUsername(Input::get('username'))
            ->first();
        if (!$user) {
            return Response::json(false);
        }
        $passwordMatch = $user->checkPassword(Input::get('password'));
        if(!$passwordMatch) {
            return Response::json(false);
        }
        return Response::json(true);

    }


    /**
     * Выход пользователя.
     * @return Redirect
     */
    public function logout() {
        Sentry::logout();
        return Redirect::to(URL::previous());
    }

    /**
     * Регистрация нового пользователя.
     * @return Redirect
     */
    public function register() {
        // Получаем массив данных, с заполняемой формы
        $data = Input::all();

        // Создаем правила для проверки данных регистрации
        $rules = array(
            'username' => 'required|min:1|unique:users',
            'email' => 'required|min:1|unique:users',
            'password' => 'required|min:1|same:confirm_password',
            'captcha' => 'required|captcha'
        );
        // Проверяем данные пользователя с учётом ограничений
        $validator = Validator::make($data, $rules);

        // Вывод ошибок
        if ($validator->fails()) {
            $errors = $validator
                ->messages()
                ->toArray();
            $new_array = array();
            foreach($errors as $key => $value) {

                // Формирование текста ошибок
                switch ($value[0]) {
                    case 'validation.unique':
                        $value[0] = 'Already exists.';
                        break;
                    case 'validation.required':
                        $value[0] = 'Empty field.';
                        break;
                    case 'validation.same':
                        $value[0] = 'Password and password confirm not the same.';
                        break;
                    case 'It seems that you have entered an invalid captcha code. Enter the code that you see in the image below.':
                        $value[0] = 'Captcha error.';
                        break;
                    default:
                        break;
                }
                $new_array[$key] = $value[0];
            }
            // Формирование новой каптчи
            $img = Captcha::img();
            $new_array['captcha_img'] = $img;
            return json_encode( array('errors' => $new_array));
        }
        try {
            // Создаём пользователя
            $user = Sentry::createUser(array(
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'username' => $data['username'],
                'email' => $data['email'],
                'password' => $data['password'],
                'activated' => 1
            ));
        } catch (Exception $e) {
            return $e;
        }

        // Находим группу пользователей(Users)
        $group = Sentry::findGroupByName('Users');

        // Назначаем его в группу пользователей
        $user->addGroup($group);

        // Если создание пользователя прошло успешно, возвращаем "ОК"
        return 'OK';
    }
}