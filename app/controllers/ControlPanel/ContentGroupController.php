<?php
namespace ControlPanel;

use Input;
use Music;
use MusicEvent;
use Response;
use Video;
use Comment;
use User;

/**
 * Контроллер групп контента. Обрабатывает смешанные групповые запросы: 
 * удаление и изменение статуса публикации.
 */

class ContentGroupController extends \BaseController {


    /**
     * Удалить объект(ы) из БД.
     * @return bool
     */
    public function deleteData()
    {
        $items = Input::get('items', false);

        if (empty($items)) {
            return Response::json(array(
                'status' => false,
                'message' => 'Not specified elements.'
            ));
        }

        $status = true;

        foreach ($items as $item) {

            switch ($item['type']) {
                case 'music':
                    if (!Music::destroy($item['id'])) {
                        $status = false;
                    }
                    break;

                case 'video':
                    if (!Video::destroy($item['id'])) {
                        $status = false;
                    }
                    break;

                case 'comment':
                    if (!Comment::destroy($item['id'])) {
                        $status = false;
                    }
                    break;

                case 'user':
                    if (!User::destroy($item['id'])) {
                        $status = false;
                    }
                    break;

                default:
                    if (!MusicEvent::destroy($item['id'])) {
                        $status = false;
                    }
                    break;
            }
        }

        return Response::json(array('status' => $status));
    }


    /**
     * Опубликовать объект(ы).
     * @return bool
     */
    public function publishData()
    {
        $items = Input::get('items', false);

        if (empty($items)) {
            return Response::json(array(
                'status' => false,
                'message' => 'Not specified elements.'
            ));
        }

        $status = true;

        foreach ($items as $item) {

            switch ($item['type']) {
                case 'music':
                    $object = Music::findOrFail($item['id']);
                    break;

                case 'video':
                    $object = Video::findOrFail($item['id']);
                    break;

                case 'event':
                    $object = MusicEvent::findOrFail($item['id']);
                    break;

                default:
                    $status = false;
                    break;
            }
            $object->published = 1;
            $object->save();
        }

        return Response::json(array('status' => $status));
    }


    /**
     * Снять с публикации объект(ы).
     * @return bool
     */
    public function unpublishData()
    {
        $items = Input::get('items', false);

        if (empty($items)) {
            return Response::json(array(
                'status' => false,
                'message' => 'Not specified elements.'
            ));
        }

        $status = true;

        foreach ($items as $item) {

            switch ($item['type']) {
                case 'music':
                    $object = Music::findOrFail($item['id']);
                    break;

                case 'video':
                    $object = Video::findOrFail($item['id']);
                    break;

                case 'event':
                    $object = MusicEvent::findOrFail($item['id']);
                    break;
                
                default:
                    $status = false;
                    break;
            }
            $object->published = 0;
            $object->save();
        }

        return Response::json(array('status' => $status));
    }

}