<?php
namespace ControlPanel;

use Carbon\Carbon;
use DateTime;
use Input;
use MusicEvent;
use Redirect;
use Response;
use Sentry;
use URLify;

/**
 * Контроллер событий в админ.панели.
 */
class EventController extends ContentController {

    /**
     * Добавить в БД новое событие.
     * @return Response
     */
    public function create()
    {
        // Сохраняем обложку
        if (Input::hasFile('img')) {
            $filename = URLify::filter(Input::file('img')->getClientOriginalName(), 60, "", true);
            Input::file('img')->move(base_path() . '/event-img/', $filename);
            $poster = '/event-img/' . $filename;
        } else {
            return Redirect::to('/adminka')->with('errors', 'Failed to upload the poster');
        }

        $date = new Carbon(Input::get('date'));

        $event = new MusicEvent(array(

            // Данные из формы
            'title' => Input::get('title'),
            'url' => Input::get('url'),
            'description' => Input::get('description'),
            'place' => Input::get('place'),
            'city' => Input::get('city'),
            'country' => Input::get('country'),
            'guest' => Input::get('guest'),
            'crosspublish' => (bool) Input::get('crosspublish', false),

            // предустановленные значения
            'user_id' => Sentry::getUser()->id,
            'published' => 0,

            // сгенерированные данные
            'poster' => $poster,
            'date' => $date->toDateTimeString(),
            'slug' => URLify::filter(Input::get('title'))
        ));

        // Валидируем событие.
        if (!$event->validate()) {
            return Redirect::to('/adminka')->with('errors', $event->getValidationErrorMessages());
        }

        // Сохраняем.
        try {
            $event->save();
            $result = array('status' => true);
        } catch (Exception $e) {
            Log::error("An error occured while saving event: {$e->getMessage()}");

            return Redirect::to('/adminka')->with('errors', $e->getMessage());
        }

        // Ставим в очередь запланированной публикации.
        $isScheduled = Input::get('pdate') == Input::get('hidden-pdate');
        if ($isScheduled) {
            $this->queue($event);
        } else {
            $this->queue($event, Input::get('pdate'));
        }

        return Redirect::to('/adminka')->with('message', 'Event successfully added!');
    }


    /**
     * Обновить информацию о событии.
     * @param  integer $id
     * @param  array $formData
     * @return bool
     */
    public function update($id, $formData)
    {
        $event = MusicEvent::find($id);
        if (!$event) {
            return Response::json(array(
                'status' => false,
                'errors' => array('update' => 'Edited event was not found.')
            ));
        }
        
        // Обновляем параметры.
        $date = new Carbon($formData['date']);
        $event->fill(array(
            'title' => $formData['title'],
            'url' => $formData['url'],
            'slug' => URLify::filter($formData['title']),
            'description' => $formData['description'],
            'date' => $date->toDateTimeString(),
            'place' => $formData['place'],
            'city' => $formData['city'],
            'country' => $formData['country'],
            'guest' => $formData['guest']
        ));

        // Валидируем событие.
        if (!$event->validate()) {
            return Response::json(array(
                'status' => false,
                'errors' => $event->getValidationErrorMessages()
            ));
        }

        // Сохраняем.
        try {
            $event->save();
            $result = array('status' => true);
        } catch (Exception $e) {
            $result = array(
                'status' => false,
                'errors' => array('save' => 'An error occured while saving event.')
            );
            Log::error("An error occured while saving event: {$e->getMessage()}");
        }

        return Response::json($result);
    }

}