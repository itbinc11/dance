<?php
namespace ControlPanel;

use PublicationQueue;
use Tag;

/**
 * Контроллер контента для админ.панели.
 */

abstract class ContentController extends \BaseController {

    /**
     * Обновить контент.
     * @return Response
     */
    abstract public function update($id, $form);


    /**
     * Интервал между публикациями в автоматической очереди.
     * @var integer
     */
    protected $publicationInterval = 15;


    /**
     * Постановка в очередь на публикацию.
     * @param  Music $track
     * @param  string $date дата публикации
     * @return bool
     */
    protected function queue($track, $date = NULL)
    {
        // Если не указана дата публикации, добавляем контент в автоматическую 
        // очередь с интервалом между публикациями в $publicationInterval минут.
        if (!$date) {
            $queue = 'auto';

            // Ищем «крайнего» в очереди на публикацию.
            $last = PublicationQueue::withTrashed()
                ->where('queue', 'auto')
                ->orderBy('publication_date', 'desc')
                ->first();

            // И, если он есть, используем за точку отсчёта.
            if ($last) {
                $pdate = new \Carbon\Carbon($last->publication_date);
                $pdate->addMinutes($this->publicationInterval);
            } else {
                $pdate = \Carbon\Carbon::now();
            }
        } else {
            $queue = 'manual';
            $pdate = $date;
        }

        $pDate = new PublicationQueue(array(
            'queue' => $queue,
            'publication_date' => $pdate
        ));

        $pDate->content()->associate($track);

        return $pDate->save();
    }


    /**
     * Разобрать csv-строку тегов, сгенерированную jQuery.tagsinput.
     * @return Array id тегов
     */
    protected function parseTags($csv)
    {
        $tags = array();

        foreach (str_getcsv($csv) as $tagTitle) {

            $tagLowerTitle = mb_strtolower($tagTitle);

            $tag = Tag::where('search', $tagLowerTitle)->first();
            if (!$tag) { // Если тега не существует, создаём.
                $tag = Tag::create(array(
                    'title' => $tagTitle,
                    'search' => $tagLowerTitle
                ));
            }

            $tags[] = $tag->id;
        }

        return $tags;
    }

}