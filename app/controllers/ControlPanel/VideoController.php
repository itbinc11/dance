<?php
namespace ControlPanel;

use Input;
use Video;
use Response;
use Sentry;
use URLify;
use Validator;

/**
 * Контроллер клипов в админ.панели.
 */
class VideoController extends ContentController {

    /**
     * Добавить в БД новый трек.
     * @return Response
     */
    public function create()
    {
        // Данные из формы и Youtube-описание клипа.
        $input = Input::get('form');
        $youTubeInfo = Input::get('tempObjectData');

        $clip = new Video(array(
            // данные из формы
            'title' => $input['title'],
            'description' => $input['description'],
            'subcategory_id' => $input['subcategory_id'],
            'search' => strtolower($input['title']),
            'crosspublish' => isset($input['crosspublish']),

            // данные от Youtube
            'url' => $youTubeInfo['player']['default'],
            'youtube_id' => $youTubeInfo['id'],

            // предустановленные значения
            'user_id' => Sentry::getUser()->id,
            'published' => 0,

            // сгенерированные данные
            'slug' => URLify::filter($input['title']),
        ));

        // Валидируем клип.
        if (!$clip->validate()) {
            return Response::json(array(
                'status' => false,
                'errors' => $clip->getValidationErrorMessages()
            ));
        }

        // Сохраняем.
        try {
            $clip->save();
            $result = array('status' => true);
        } catch (Exception $e) {
            $result = array(
                'status' => false,
                'errors' => array('save' => 'An error occured while saving clip.')
            );
            Log::error("An error occured while saving clip: {$e->getMessage()}");
        }

        // Добавляем теги.
        $clip->tags()->sync($this->parseTags($input['tags']));

        // Ставим в очередь запланированной публикации.
        $isScheduled = $input['pdate'] == $input['hidden-pdate'];
        if ($isScheduled) {
            $this->queue($clip);
        } else {
            $this->queue($clip, $input['pdate']);
        }

        return Response::json($result);
    }


    /**
     * Обновить информацию о клипе.
     * @param  integer $id
     * @param  array $formData
     * @return bool
     */
    public function update($id, $formData)
    {
        $clip = Video::find($id);
        if (!$clip) {
            return Response::json(array(
                'status' => false,
                'errors' => array('update' => 'Edited clip was not found.')
            ));
        }
        
        // Обновляем параметры.
        $clip->fill(array(
            'title' => $formData['title'],
            'slug' => URLify::filter($formData['title']),
            'description' => $formData['description'],
            'subcategory_id' => $formData['category'],
            'search' => mb_strtolower($formData['title'])
        ));

        // Валидируем трек.
        if (!$clip->validate()) {
            return Response::json(array(
                'status' => false,
                'errors' => $clip->getValidationErrorMessages()
            ));
        }

        // Актуализируем теги.
        $clip->tags()->sync($this->parseTags($formData['tags']));

        // Сохраняем.
        try {
            $clip->save();
            $result = array('status' => true);
        } catch (Exception $e) {
            $result = array(
                'status' => false,
                'errors' => array('save' => 'An error occured while saving clip.')
            );
            Log::error("An error occured while saving clip: {$e->getMessage()}");
        }

        return Response::json($result);
    }

}