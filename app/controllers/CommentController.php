<?php
/**
 * Отображение страницы Playlists с возможностью управления.
 */

class CommentController extends BaseController {

    /**
     * Количество объектов, выводимых на странице.
     * @var integer
     */
    public $step = 8;


    /**
     * Срок годности информации от SC. По истечении этого срока
     * SC-описание трека будет обновлено.
     * @var integer
     */
    public $expiration = 3;

    /**
     * Сохранение нового комментария.
     * @return View
     */
    public function addComment() {

        $data = Input::all();
        $img = '';

        $rules = array(
            'comment' => 'required|min:1',
            'item_type' => 'required',
            'captcha' => 'required|captcha'
        );

        // Проверяем данные пользователя с учётом правил
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $errors = $validator
                ->messages()
                ->toArray();
            $new_array = array();
            foreach ($errors as $key => $value) {

                // Формирование текста ошибок
                switch ($value[0]) {
                    case 'validation.unique':
                        $value[0] = 'Already exists.';
                        break;
                    case 'validation.required':
                        $value[0] = 'Empty field.';
                        break;
                    case 'validation.same':
                        $value[0] = 'Password and password confirm not the same.';
                        break;
                    case 'It seems that you have entered an invalid captcha code. Enter the code that you see in the image below.':
                        $value[0] = 'Captcha error.';
                        break;
                    default:
                        break;
                }
                $new_array[$key] = $value[0];
            }
            $img = Captcha::img();
            return Response::json(array('captcha'=>$img, 'status' => 'Error.'));
        }


        $comment = new Comment;
        $comment->user_id = (int) Sentry::getUser()->id;
        $comment->item_id = (int) $data['item_id'];
        $comment->item_type = $data['item_type'];
        $comment->comment = $data['comment'];

        try {
            $result = $comment->save();
        } catch (Exception $e) {
            return $e;
        }

        return Response::json(array('captcha'=>$img, 'status' => 'OK'));
    }

    public function showComments() {
        $data = Input::all();

        $comments = Comment::select(
            'comments.id as comment_id',
            'comments.user_id as comment_user_id',
            'comments.item_id as comment_item_id',
            'comments.item_type as comment_item_type',
            'comments.comment as comment_comment',
            'comments.created_at as comment_created_at',
            'comments.updated_at as comment_updated_at',
            'users.id as user_id',
            'users.email as user_email',
            'users.first_name as user_first_name',
            'users.last_name as user_last_name',
            'users.username as user_username',
            'users.avatar as user_avatar'
            )
            ->join('users', 'comments.user_id', '=', 'users.id')
            ->where('item_type', '=', $data['item_type'])
            ->where('item_id', '=', $data['item_id'])
            ->orderBy('comment_created_at', 'desc')
            ->get();


        $view = View::make('controlPanel/showComments')
            ->with(compact('comments', 'categories'))
            ->render();

        return Response::json($view);
    }

}