<?php
/**
 * Отображение главной страницы списка музыкальных треков с возможностью 
 * фильтрации по категории.
 */

class MusicController extends BaseController {

    /**
     * Количество объектов, выводимых на странице.
     * @var integer
     */
    public $step = 8;


    /**
     * Срок годности информации от SC. По истечении этого срока 
     * SC-описание трека будет обновлено.
     * @var integer
     */
    public $expiration = 3;


    /**
     * Отобразить страницу со списком треков.
     * @return View
     */
    public function showMusic()
    {
        $music = Music::where('published', 1)
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->take($this->step)
            ->with('tags')
            ->get();

        $metaTags = MetaTags::generateTags();

        return View::make('music/music')->with(compact('music', 'metaTags'));
    }


    /**
     * Отобразить страницу трека.
     * @param  string $slug
     * @return View
     */
    public function showTrack($slug)
    {
        $singleTrack = Music::where('slug', $slug)->firstOrFail();

        // Прячем неопубликованные треки
        if (!$singleTrack->published) { App::abort(404); };

        $singleTrack->increment('internal_playback_count');
        $singleTrack->increment('internal_weekly_playback_count');

        $music = Music::where('published', 1)
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->take($this->step)
            ->with('tags')
            ->get();

        $metaTags = MetaTags::generateTags(array(
            'title' => $singleTrack->title,
            'description' => $singleTrack->description
        ));

        return View::make('music/musicTrack')->with(compact(
            'music',
            'singleTrack',
            'metaTags'
        ));
    }


    /**
     * Возвращает html-код проигрывателя для отображения в popup'е.
     * @param  string $slug
     * @return View
     */
    public function getTrack($slug)
    {
        $track = Music::where('slug', $slug)->firstOrFail();

        // Прячем неопубликованные треки
        if (!$track->published) { App::abort(404); };

        $track->increment('internal_playback_count');
        $track->increment('internal_weekly_playback_count');

        // Если SC-информация о треке давно не обновлялась — обновляем её.
        if ($track->updated_at->diffInDays() > $this->expiration) {
            $track->updateSCInfo();
        }

        $relatedTracks = $track->getRelated();

        // Статус лайка для трека.
        $like = 'popup-music-likes';
        if(Sentry::check()) {
            $like_status = Like::where('user_id', '=', Sentry::getUser()->id)
                ->where('type', '=', 'music')
                ->where('item_id', '=', $track->id)
                ->first();
            if(!empty($like_status)) {
                $like = 'popup-music-liked';
            }
        }

        // Комментарии к данному трэку + инфо о владельце комментария
        $comments = Comment::select(
            'comments.id as comment_id',
            'comments.user_id as comment_user_id',
            'comments.item_id as comment_item_id',
            'comments.item_type as comment_item_type',
            'comments.comment as comment_comment',
            'comments.created_at as comment_created_at',
            'comments.updated_at as comment_updated_at',
            'users.id as user_id',
            'users.email as user_email',
            'users.first_name as user_first_name',
            'users.last_name as user_last_name',
            'users.username as user_username',
            'users.avatar as user_avatar'
        )
            ->join('users', 'comments.user_id', '=', 'users.id')
            ->where('item_type', '=', 'music')
            ->where('item_id', '=', $track->id)
            ->orderBy('comment_created_at', 'desc')
            ->get();

        return View::make('music/musicAjax')->with(compact('track', 'like', 'comments', 'relatedTracks'));
    }


    /**
     * Возвращает html-код списка треков, для вывода на странице по нажатию 
     * кнопки «view more».
     * @param  integer $page Номер страницы
     * @return View/Response
     */
    public function getPage($page)
    {
        $music = Music::where('published', 1)
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->skip($this->step*($page-1))
            ->take($this->step)
            ->with('tags')
            ->get();

        if($music->isEmpty()) { App::abort(404); };

        return View::make('music/musicPageAjax')->with(compact(
            'music',
            'page'
        ));
    }


    /**
     * Отобразить категорию.
     * @return View
     */
    public function showSubMusic($subSlug)
    {
        $sub = Subcategory::where('slug', $subSlug)->firstOrFail();

        $music = Music::where('published', 1)
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->where('subcategory_id', $sub->id)
            ->take($this->step)
            ->with('tags')
            ->get();

        $metaTags = MetaTags::generateTags(array(
            'title' => $sub->title
        ));

        return View::make('music/musicSub')->with(compact(
            'music',
            'sub',
            'metaTags'
        ));
    }


    /**
     * Возвращает html-код списка треков определённой категории, для вывода 
     * на странице по нажатию кнопки «view more».
     * @param  integer $page Номер страницы
     * @return View/Response
     */
    public function getSubPage($subSlug, $page)
    {
        $sub = Subcategory::where('slug', $subSlug)->firstOrFail();

        $music = Music::where('published', 1)
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->where('subcategory_id', $sub->id)
            ->skip($this->step*($page-1))
            ->take($this->step)
            ->with('tags')
            ->get();

        if($music->isEmpty()) { App::abort(404); };

        return View::make('music/musicSubPageAjax')->with(compact(
            'music',
            'sub',
            'page'
        ));
    }


    /**
     * Отметить лайк музыкальной композиции
     * @return Exception|string
     */
    public function setLike() {
        // Поиск лайка пользователя данной композиции
        $like = Like::where('user_id', '=', Sentry::getUser()->id)
            ->where('item_id', '=', Input::get('id'))
            ->where('type', '=', 'music')
            ->first();
        // Удаление лайка
        if($like) {
            try {
                $like->delete();
            } catch (Exception $e) {
                return $e;
            }
            return Response::json(false);
        }

        // Создание лайка
        $like = new Like;
        $like->user_id = Sentry::getUser()->id;
        $like->item_id = Input::get('id');
        $like->type = 'music';
        try {
            $result = $like->save();
        } catch (Exception $e) {
            return $e;
        }
        return Response::json(true);
    }

}