<?php
/**
 * Отображение страницы списка мероприятий.
 */

class MusicEventController extends BaseController {

    /**
     * Количество объектов, выводимых на странице.
     * @var integer
     */
    public $step = 8;


    /**
     * Вывести список мероприятий
     * @return View
     */
    public function showEvents()
    {
        $events = MusicEvent::where('published', 1)
            ->orderBy('date', 'desc')
            ->orderBy('id', 'desc')
            ->take($this->step)
            ->get();

        $metaTags = MetaTags::generateTags();

        return View::make('events/events')->with(compact(
            'events',
            'metaTags'
        ));
    }


    /**
     * Вывести страницу мероприятия.
     * @param  string $slug
     * @return View
     */
    public function showEvent($slug)
    {
        $singleEvent = MusicEvent::where('slug', $slug)->firstOrFail();

        // Прячем неопубликованные мероприятия
        if (!$singleEvent->published) { App::abort(404); };

        $events = MusicEvent::where('published', 1)
            ->orderBy('date', 'desc')
            ->orderBy('id', 'desc')
            ->take($this->step)
            ->get();

        $metaTags = MetaTags::generateTags(array(
            'title' => $singleEvent->title
        ));

        return View::make('events/event')->with(compact(
            'events',
            'singleEvent',
            'metaTags'
        ));
    }


    /**
     * Возвращает html-код описания события для попапа.
     * @param  string $slug
     * @return View
     */
    public function getEvent($slug)
    {
        $event = MusicEvent::where('slug', $slug)->firstOrFail();

        // Прячем неопубликованные мероприятия
        if (!$event->published) { App::abort(404); };

        // Статус лайка для клипа.
        $like = 'popup-event-likes';
        if(Sentry::check()) {
            $liked = Like::where('user_id', '=', Sentry::getUser()->id)
                ->where('type', '=', 'video')
                ->where('item_id', '=', $event->id)
                ->first();
            if($liked) {
                $like = 'popup-event-liked';
            }
        }
        
        return View::make('events/eventAjax')->with(compact('event', 'like'));
    }


    /**
     * Возвращает html-код списка мероприятий, для вывода на странице по нажатию 
     * кнопки «view more».
     * @param  integer $page Номер страницы
     * @return View/Response
     */
    public function getPage($page)
    {
        $events = MusicEvent::where('published', 1)
            ->orderBy('date', 'desc')
            ->orderBy('id', 'desc')
            ->skip($this->step*($page-1))
            ->take($this->step)
            ->get();

        if($events->isEmpty()) { App::abort(404); };

        return View::make('events/eventsPageAjax')->with(compact(
            'events',
            'page'
        ));
    }

    /**
     * Отметить лайк музыкальному ивенту
     * @return Exception|string
     */
    public function setLike() {

        if(!Sentry::check()) { return false; }

        // Поиск лайка пользователя данного ивента
        $like = Like::where('user_id', '=', Sentry::getUser()->id)
            ->where('item_id', '=', Input::get('id'))
            ->where('type', '=', 'event')
            ->first();
        // Удаление лайка
        if($like) {
            try {
                $like->delete();
            } catch (Exception $e) {
                return $e;
            }
            return Response::json(false);
        }

        // Создание лайка
        $like = new Like;
        $like->user_id = Sentry::getUser()->id;
        $like->item_id = Input::get('id');
        $like->type = 'event';
        try {
            $result = $like->save();
        } catch (Exception $e) {
            return $e;
        }
        return Response::json('OK');
    }

}