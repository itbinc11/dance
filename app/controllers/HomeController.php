<?php
/**
 * Контроллер главной страницы.
 */

class HomeController extends BaseController {

    /**
     * Количество треков, выводимых на главной.
     * @var integer
     */
    public $musicStep = 8;


    /**
     * Количество клипов, выводимых на главной.
     * @var integer
     */
    public $videoStep = 4;


    /**
     * Отобразить главную страницу.
     * @return View home
     */
    public function showHome()
    {
        /**
         * Дополнительная сортировка по полю id, позволяет избежать 
         * «мёртвой зоны» в случае когда количество треков с одинаковой датой 
         * создания > количества треков, отображаемого на странице.
         */
        
        $events = MusicEvent::where('published', 1)
            ->orderBy('date', 'desc')
            ->orderBy('id')
            ->take(10)
            ->get();

        $top10 = Music::where('published', 1)
            ->orderBy('internal_weekly_playback_count', 'desc')
            ->orderBy('created_at', 'desc')
            ->take(10)
            ->get();

        $music = Music::where('published', 1)
            ->orderBy('created_at', 'desc')
            ->orderBy('id')
            ->take($this->musicStep)
            ->with('tags')
            ->get();

        $video = Video::where('published', 1)
            ->orderBy('created_at', 'desc')
            ->orderBy('id')
            ->take($this->videoStep)
            ->with('tags')
            ->get();

        $metaTags = MetaTags::generateTags();

        $categories = Subcategory::all();

        return View::make('home')->with(compact(
            'events',
            'top10',
            'music',
            'video',
            'metaTags',
            'categories'
        ));
    }

}