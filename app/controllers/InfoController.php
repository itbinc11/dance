<?php
/**
 * Отображение страницы About с возможностью редактирования данных.
 */

class InfoController extends BaseController {

    /**
     * Вывод формы страницы About.
     * @return View
     */
    public function showMyInformation() {

        $metaTags = MetaTags::generateTags();

        return View::make('pages/info')->with(compact('metaTags'));
    }

    // Изменение данных пользователя
    public function changeInfo() {
        $data = Input::all();
        $alerts = array();
        // Проверка пароля
        $emptyUser = Sentry::getUserProvider()->getEmptyUser();
        $user = $emptyUser->whereUsername($data['username'])->first();
        $passwordMatch = $user->checkPassword($data['password']);

        if($passwordMatch) {
            // Сохранение новых данных
            $user->first_name = $data['first_name'];
            $user->last_name = $data['last_name'];
            $user->username = $data['username'];
            $user->email = $data['email'];
            $user->about = $data['about'];

            try {
                $user->save();
            } catch (Exception $e) {
                return $e;
            }

            $alerts['success'] = 'Information has been SAVED.';
            return Redirect::to('/myinfo')->with('alerts', $alerts);
        } else {
            $alerts['password_error'] = 'Incorrect Password.';
            return Redirect::to('/myinfo')->with('alerts', $alerts);
        }
    }

}