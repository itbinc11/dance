<?php
/**
 * Отображение страницы About с возможностью редактирования данных.
 */

class FavoritesController extends BaseController {

    /**
     * Количество объектов, выводимых на странице.
     * @var integer
     */
    public $step = 8;

    /**
     * Вывод формы страницы About.
     * @return View
     */
    public function showFavorites() {

        $music = Music::where('published', 1)
            ->join('likes', 'likes.item_id', '=', 'music.id')
            ->where('likes.type', '=', 'music')
            ->where('likes.user_id', '=', Sentry::getUser()->id)
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->take($this->step)
            ->with('tags')
            ->get();

        $video = Video::where('published', 1)
            ->join('likes', 'likes.item_id', '=', 'videos.id')
            ->where('likes.type', '=', 'video')
            ->where('likes.user_id', '=', Sentry::getUser()->id)
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->take($this->step)
            ->with('tags')
            ->get();

        $metaTags = MetaTags::generateTags();

        return View::make('pages/favorites')->with(compact('music', 'video', 'metaTags'));

    }

    // Изменение
    public function changeInfo() {
        $data = Input::all();
        $alerts = array();
        // Проверка пароля
        $emptyUser = Sentry::getUserProvider()->getEmptyUser();
        $user = $emptyUser->whereUsername($data['username'])->first();
        $passwordMatch = $user->checkPassword($data['password']);

        if($passwordMatch) {
            // Сохранение новых данных
            $user->first_name = $data['first_name'];
            $user->last_name = $data['last_name'];
            $user->username = $data['username'];
            $user->email = $data['email'];
            $user->about = $data['about'];

            try {
                $user->save();
            } catch (Exception $e) {
                return $e;
            }

            $alerts['success'] = 'Information has been SAVED.';
            return Redirect::to('/myinfo')->with('alerts', $alerts);
        } else {
            $alerts['password_error'] = 'Incorrect Password.';
            return Redirect::to('/myinfo')->with('alerts', $alerts);
        }
    }


}