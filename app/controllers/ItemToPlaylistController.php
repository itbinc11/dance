<?php
/**
 * Отображение страницы Playlists с возможностью управления.
 */

class ItemToPlaylistController extends BaseController {

    /**
     * Количество объектов, выводимых на странице.
     * @var integer
     */
    public $step = 8;

    /**
     * Вывод формы страницы About.
     * @return View
     */
    public function showPlaylists() {

        $metaTags = MetaTags::generateTags();

        $playlists = Playlist::where('user_id', '=', Sentry::getUser()->id)
            ->get();

        return View::make('pages/playlists')->with(compact('metaTags', 'playlists'));

    }

    // Вывод содержимого плэйлиста
    public function getPlayList() {
        $data = Input::all();
        $musics = Music::join('item_to_playlist', 'item_to_playlist.item_id', '=', 'music.id')

            ->get();
        return Response::json(Sentry::getUser()->id);
    }

    // Добавление плэйлиста
    public function addPlaylist() {
        $data = Input::all();

        // Проверка на пустое имя прэйлиста
        if($data['name'] == '') {
            return Response::json('Empty name.');
        }

        // Проверка на наличие плэйлиста с таким именем у данного пользователя
        $playlist = Playlist::where('name', '=', $data['name'])
            ->where('user_id', '=', Sentry::getUser()->id)
            ->first();
        if($playlist) {
            return Response::json('Playlist with name "' . $data['name'] . '" already exists.');
        }

        // Создаем и сохраняем новый плэйлист
        $playlist = new Playlist;
        $playlist->name = $data['name'];
        $playlist->user_id = Sentry::getUser()->id;
        try {
            $playlist->save();
        } catch (Exception $e) {
            return $e;
        }
        return Response::json(true);
    }

    // Добавление элемента в плэйлист
    public function addItemTooPlaylist() {
        $data = Input::all();

        try {
            $add = ItemToPlaylist::create(array(
                'playlist_id' => $data['playlist_id'],
                'item_id' => $data['item_id'],
                'sort' => $data['sort']
            ));
        } catch (Exception $e) {
            return $e;
        }
        return Response::json(true);

    }

    /**
     * Изменение очереди элемента в плэйлисте.
     * @incoming var id, sort.
     * @return Exception
     */
    public function setSort() {
        $data = Input::all();

        $current = ItemToPlaylist::find($data['id']);
        $current_sort = $current->sort;
        $current->sort = $data['sort'];

        // Переносиим элемент в перед по списку
        if ($current_sort > $data['sort']) {
            try {
                DB::update('update itemtoplaylists set sort = sort+1 where sort >= ? and sort < ?', array($data['sort'], $current_sort));
                $current->save();
            } catch (Exception $e) {
                return $e;
            }

        }
        // Переноситим элемент в конец по списку
        elseif ($current_sort < $data['sort']) {
            try {
                DB::update('update itemtoplaylists set sort = sort-1 where sort > ? and sort <= ?', array($current_sort, $data['sort']));
                $current->save();
            } catch (Exception $e) {
                return $e;
            }
        } else {
            //
            return Response::json(false);
        }
        return Response::json(true);
    }


}