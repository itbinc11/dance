<?php
/**
 * Отображение главной страницы списка видеоклипов с возможностью 
 * фильтрации по категории.
 */

class VideoController extends BaseController {

    /**
     * Количество объектов на страницу.
     * @var integer
     */
    public $step = 8;


    /**
     * Интервал проверки доступности клипа на Youtube.
     * @var integer
     */
    public $expiration = 3;


    /**
     * Показать страницу со списком видео.
     * @return View
     */
    public function showVideo()
    {
        $video = Video::where('published', 1)
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->take($this->step)
            ->with('tags')
            ->get();

        $metaTags = MetaTags::generateTags();

        return View::make('video/video')->with(compact(
            'video',
            'metaTags'
        ));
    }


    /**
     * Показать страницу клипа.
     * @param  string $slug
     * @return View
     */
    public function showClip($slug)
    {
        $singleClip = Video::where('slug', $slug)->firstOrFail();

        // Прячем неопубликованные клипы
        if (!$singleClip->published) { App::abort(404); };

        $video = Video::where('published', 1)
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->take($this->step)
            ->with('tags')
            ->get();

        $metaTags = MetaTags::generateTags(array(
            'title' => $singleClip->title
        ));

        return View::make('video/videoClip')->with(compact(
            'video',
            'singleClip',
            'metaTags'
        ));
    }


    /**
     * Возвращает html-код проигрывателя для отображения в popup'е.
     * @param  string $slug
     * @return View
     */
    public function getClip($slug)
    {
        $clip = Video::where('slug', $slug)->firstOrFail();

        // Прячем неопубликованные клипы
        if (!$clip->published) { App::abort(404); };

        $clip->increment('internal_playback_count');

        // Проверка доступности клипа на youtube
        if ($clip->updated_at->diffInDays() > $this->expiration) {
            if ($clip->availableOnYoutube()) { // если доступен — обновляем updated_at
                $clip->touch();
                $clip->save();
            } else { // иначе — снимаем с публикации и возвращаем 404
                $clip->published = 0;
                $clip->save;
                App::abort(404);
            }
        }

        $relatedVideos = $clip->getRelated();

        // Статус лайка для клипа.
        $like = 'popup-video-likes';
        if(Sentry::check()) {
            $liked = Like::where('user_id', '=', Sentry::getUser()->id)
                ->where('type', '=', 'video')
                ->where('item_id', '=', $clip->id)
                ->first();
            if($liked) {
                $like = 'popup-video-liked';
            }
        }

        // Комментарии к данному трэку + инфо о владельце комментария
        $comments = Comment::select(
            'comments.id as comment_id',
            'comments.user_id as comment_user_id',
            'comments.item_id as comment_item_id',
            'comments.item_type as comment_item_type',
            'comments.comment as comment_comment',
            'comments.created_at as comment_created_at',
            'comments.updated_at as comment_updated_at',
            'users.id as user_id',
            'users.email as user_email',
            'users.first_name as user_first_name',
            'users.last_name as user_last_name',
            'users.username as user_username',
            'users.avatar as user_avatar'
        )
            ->join('users', 'comments.user_id', '=', 'users.id')
            ->where('item_type', '=', 'videos')
            ->where('item_id', '=', $clip->id)
            ->orderBy('comment_created_at', 'desc')
            ->get();


        return View::make('video/videoAjax')->with(compact('clip', 'like', 'comments', 'relatedVideos'));
    }


    /**
     * Возвращает html-код списка клипов, для вывода на странице 
     * по нажатию «view more».
     * @param  integer $page Номер страницы
     * @return View/Response
     */
    public function getPage($page)
    {
        $video = Video::where('published', 1)
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->skip($this->step*($page-1))
            ->take($this->step)
            ->with('tags')
            ->get();

        if($video->isEmpty()) { App::abort(404); };

        return View::make('video/videoPageAjax')->with(compact('video', 'page'));
    }


    /**
     * Отобразить страницу категории.
     * @return View
     */
    public function showSubVideo($subSlug)
    {
        $sub = Subcategory::where('slug', $subSlug)->firstOrFail();

        $video = Video::where('published', 1)
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->where('subcategory_id', $sub->id)
            ->take($this->step)
            ->with('tags')
            ->get();

        $metaTags = MetaTags::generateTags(array(
            'title' => $sub->title
        ));

        return View::make('video/videoSub')->with(compact(
            'video',
            'sub',
            'metaTags'
        ));
    }


    /**
     * Возвращает html-код списка клипов определённой категории, для вывода 
     * по нажатию «view more».
     * @param  integer $page Номер страницы
     * @return View/Response
     */
    public function getSubPage($subSlug, $page)
    {
        $sub = Subcategory::where('slug', $subSlug)->firstOrFail();

        $video = Video::where('published', 1)
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->where('subcategory_id', $sub->id)
            ->skip($this->step*($page-1))
            ->take($this->step)
            ->with('tags')
            ->get();

        if($video->isEmpty()) { App::abort(404); };

        return View::make('video/videoSubPageAjax')->with(compact(
            'video',
            'sub',
            'page'
        ));
    }


    /**
     * Отметить лайк видео композиции
     * @return Exception|string
     */
    public function setLike() {
        // Поиск лайка пользователя данной композиции
        $like = Like::where('user_id', '=', Sentry::getUser()->id)
            ->where('item_id', '=', Input::get('id'))
            ->where('type', '=', 'video')
            ->first();
        // Удаление лайка
        if($like) {
            try {
                $like->delete();
            } catch (Exception $e) {
                return $e;
            }
            return Response::json(false);
        }

        // Создание лайка
        $like = new Like;
        $like->user_id = Sentry::getUser()->id;
        $like->item_id = Input::get('id');
        $like->type = 'video';
        try {
            $result = $like->save();
        } catch (Exception $e) {
            return $e;
        }
        return Response::json(true);
    }

}