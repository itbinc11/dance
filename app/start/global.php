<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	Log::error($exception);
});


/*
|--------------------------------------------------------------------------
| Пользовательские обработчики ошибок
|--------------------------------------------------------------------------
*/

use Illuminate\Database\Eloquent\ModelNotFoundException;

App::error(function(ModelNotFoundException $e)
{
    return Response::view('404', array('metaTags' => MetaTags::generateTags()), 404);
    //return Response::make('Not Found (ModelNotFoundException)', 404);
});

App::missing(function($exception)
{
    return Response::view('404', array('metaTags' => MetaTags::generateTags()), 404);
    //return Response::make('Not Found (missing)', 404);
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return Response::make("Be right back!", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';

/*
|--------------------------------------------------------------------------
| Метка для подсчёта прослушиваний за неделю
|--------------------------------------------------------------------------
|
| Статистика для top-10 считается в течении одной недели. Для этого 
| используется метка в кэше, с датой истечения в конце текущей недели.
*/
App::before(function($request)
{
    // Если метки нет, тогда
    if (!Cache::get('topTenCounting', false)) {
        // обнуляем счётчик воспроизведения у всех треков
        DB::table('music')->update(array('internal_weekly_playback_count' => 0));

        // устанавливаем новую метку, истекающую в конце недели и содержащую дату начала недели.
        $dt = \Carbon\Carbon::now();
        Cache::put('topTenCounting', $dt->startOfWeek()->toDateTimeString(), $dt->endOfWeek());

        Log::info('Reset internal_weekly_playback_count!');
    }
});

/*
|--------------------------------------------------------------------------
| Передача в шаблоны списка top-5 для отображения в подвале
|--------------------------------------------------------------------------
*/
View::creator('index', function($view)
{
    $view->with(
        'popular',
        Music::where('published', 1)
            ->orderBy('internal_playback_count', 'desc')
            ->orderBy('updated_at', 'desc')
            ->take(5)
            ->get()
    );
});

/*
|--------------------------------------------------------------------------
| Передача в шаблоны списков категорий по типам (all, video, music)
|--------------------------------------------------------------------------
*/
View::creator('*', function($view)
{
    $view->with(
        'videoSubcategories',
        Cache::get('videoSubcategories', function() { 
            $videoSubcategories = Subcategory::where('type', 'video')
                ->orWhere('type', 'all')
                ->orderBy('id')
                ->get();
            Cache::put('videoSubcategories', $videoSubcategories, 60*24*30);
            return $videoSubcategories;
        })
    );

    $view->with(
        'musicSubcategories',
        Cache::get('musicSubcategories', function() { 
            $musicSubcategories = Subcategory::where('type', 'music')
                ->orWhere('type', 'all')
                ->orderBy('id')
                ->get();
            Cache::put('musicSubcategories', $musicSubcategories, 60*24*30);
            return $musicSubcategories;
        })
    );

    $view->with(
        'allSubcategories',
        Cache::get('allSubcategories', function() { 
            $allSubcategories = Subcategory::where('type', 'all')->orderBy('id')->get();
            Cache::put('allSubcategories', $allSubcategories, 60*24*30);
            return $allSubcategories;
        })
    );
});

/*
|--------------------------------------------------------------------------
| Отложенная публикация контента
|--------------------------------------------------------------------------
| 
| Из списка выбираются задачи со временем публикации <= текущему времени.
| После публикации связанного с ними контента, задачи помечаются для 
| последующего удаления, но остаются в списке ещё в течении 5 минут, это 
| нужно для планирования времени вновь добавляемых публикации.
*/
App::before(function() {

    // выбор актуальных тасков
    $requirePublication = PublicationQueue::where(
        'publication_date',
        '<=',
        Carbon\Carbon::now()
    )->with('content')->get();

    if ($requirePublication->count()) {

        // публикация контента
        $requirePublication->each(function($pInfo) {
            if (is_a($pInfo->content, 'Publication')) {
                $pInfo->content->published = 1;
                $pInfo->content->created_at = new DateTime; // нужно для правильной сортировки
                $pInfo->content->save();

                // Добавление в очередь кросспубликации
                if ($pInfo->content->crosspublish) {
                    $cross = new CrosspostingQueue;
                    $pInfo->content->crosspostingQueue()->save($cross);
                }
            }

            $pInfo->delete(); // мягкое удаление таска публикации
        });

    };

    // очистка списка от тасков выполненных более 5 минут назад
    $expiredPublication = PublicationQueue::onlyTrashed()
        ->where(
            'publication_date',
            '<',
            Carbon\Carbon::now()->subMinutes(5)
        )
        ->get();

    $expiredPublication->each(function($pInfo) {
        $pInfo->forceDelete();
    });

    unset($requirePublication, $expiredPublication, $pInfo, $content);

});


/*
|--------------------------------------------------------------------------
| Логирование действий админов
|--------------------------------------------------------------------------
| 
| В основе логирования лежат события, генерируемые моделями.
| В качестве временного решения события пишутся в общий лог.
*/
MusicEvent::created(function($model) {
    Log::info("Event added by “" . Sentry::getUser()->username . "”: {$model->title}");
});

Music::created(function($model) {
    Log::info("Track added by “" . Sentry::getUser()->username . "”: {$model->title}");
});

Video::created(function($model) {
    Log::info("Clip added by “" . Sentry::getUser()->username . "”: {$model->title}");
});