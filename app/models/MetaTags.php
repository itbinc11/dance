<?php

class MetaTags extends Eloquent {

    protected $fillable = array('url_pattern', 'title', 'description', 'keywords');


    /**
     * Генерация мета-тегов в соответствии с запрошенным URL
     * @param  array $data данные для подстановки в шаблоны мета-тегов
     * @return stdClass
     */
    public static function generateTags($data = false)
    {
        $urls = MetaTags::all();
        if ($urls->isEmpty()) { return false; }

        // сортировка по убыванию «специфичности»
        $urls->sortByDesc('url_pattern');

        $path = Request::path();

        // мета-теги по умолчанию берутся из самого неспецифичного набора
        $result = $urls->last();

        foreach ($urls as $url) {
            if (fnmatch($url->url_pattern, $path, FNM_CASEFOLD)) {
                $result = $url;
                break;
            }
        }

        $metaTags = new stdClass();
        $metaTags->title = $result->title;
        $metaTags->description = $result->description;
        $metaTags->keywords = $result->keywords;

        // Если переданы дополнительные данные — заменить ими placeholder'ы
        if ($data) {
            foreach($data as $key => $value) {
                $metaTags->title = str_replace('%' . $key . '%', $value, $metaTags->title);
                $metaTags->description = str_replace('%' . $key . '%', $value, $metaTags->description);
                $metaTags->keywords = str_replace('%' . $key . '%', $value, $metaTags->keywords);
            }
        }

        return $metaTags;
    }

}