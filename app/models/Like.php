<?php

class Like extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'likes';

    /**
     * Лайки поставленные пользователем.
     */
    public function likes()
    {
        return $this->belongsTo('User', 'user_id');
    }


}