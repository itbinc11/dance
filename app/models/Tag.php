<?php

class Tag extends Eloquent {

    protected $fillable = array('title', 'search');

    /**
     * Найти треки помеченные тегом.
     * Отношение один-ко-многим.
     * @return Collection
     */
    public function music()
    {
        return $this->belongsToMany('Music', 'tag_music', 'tag_id', 'music_id');
    }


    /**
     * Найти клипы помеченные тегом.
     * Отношение один-ко-многим.
     * @return Collection
     */
    public function video()
    {
        return $this->belongsToMany('Video', 'tag_video', 'tag_id', 'video_id');
    }

}