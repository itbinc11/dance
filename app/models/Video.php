<?php

class Video extends Publication {

    protected $guarded = array();

    /**
     * Найти теги, которыми отмечен клип.
     * Отношение один-ко-многим.
     * @return Collection
     */
    public function tags()
    {
        return $this->belongsToMany('Tag', 'tag_video', 'video_id', 'tag_id');
    }


    /**
     * Найти категорию клипа.
     * @return Subcategory
     */
    public function subcategory()
    {
        return $this->belongsTo('Subcategory');
    }


    /**
     * Проверить доступность ролика на youtube.
     * @return boolean
     */
    public function availableOnYoutube()
    {
        $url = "http://gdata.youtube.com/feeds/api/videos/". $this->attributes['youtube_id'];

        $xml = simplexml_load_file($url);

        return $xml ? true : false;
    }


    /**
     * Валидация описания клипа.
     * @return bool
     */
    public function validate()
    {
        // Определяем, клип создается или редактируется,
        // на основе этого выбираем правила валидации.
        if ($this->id) {

            // Редактирование
            $this->validator = Validator::make(
                $this->attributes,
                array(
                    'title' => "required|unique:videos,title,{$this->id}",
                    'subcategory_id' => 'required|exists:subcategories,id'
                ),
                $this->validationMessages
            );

        } else {

            // Создание
            $this->validator = Validator::make(
                $this->attributes,
                array(
                    'title' => "required|unique:videos,title",
                    'url' => "required|unique:videos,url",
                    'subcategory_id' => 'required|exists:subcategories,id'
                ),
                $this->validationMessages
            );

        }

        return $this->validator->passes();

    }


    /**
     * Сгенерировать html из plainText описания клипа.
     * @return string
     */
    public function htmlDescription() {
        $ttl = new Eventviva\TextToLinks;
        $description = $ttl->makeLinks($this->description);
        return nl2br(htmlspecialchars_decode($description));
    }


    /**
     * Найти похожие по названию и тегам ролики.
     * @param integer $num количество клипов
     * @return Collection
     */
    public function getRelated($num = 3)
    {
        // Найти ролики с похожим названием.
        // подготовка запроса
        $query = Video::orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->take($num);

        // поиск роликов, содержащих в названии аналогичные слова
        $searchTerms = explode(' ', $this->title);
        foreach ($searchTerms as $term)
        {
            // отбрасываем слова короче 3 символов
            if (strlen($term) < 3) {
                continue;
            }
            $query->orWhere('search', 'LIKE', '%'. ($term) .'%');
            $terms[] = $term;
        }
        $videos = $query->get();

        // отфильтровываем снятые с публикации материалы
        $videos = $videos->filter(function($video) {
            return $video->published;
        });


        // Если роликов менее $num, дополнить по тегам.
        if ($videos->count() < $num and $this->tags()->count() > 0) {
            $tags = $this->tags;

            foreach($tags as $tag) {
                $videos = $videos->merge($tag->video()->where('published', 1)->get());

                // лишнего нам не надо)
                if ($videos->count() > $num) {
                    break;
                }
            }
        }

        return $videos->take($num);
    }


    /**
     * Получить ссылку на клип.
     * @return string
     */
    public function getUrl() {
        return URL::route('videos/clip', $this->slug);
    }


    /**
     * Литера типа контента.
     * @return string
     */
    public function getShortUrlLetter()
    {
        return 'v';
    }

}