<?php

/**
 * Модель публикации на сайте: Трека, Клипа, События.
 *
 * Реализует функциональность соц.ссылок и счётчиков.
 */
abstract class Publication extends Eloquent {

    /**
     * Интервал обновления социальных счётчиков в минутах.
     * @var integer
     */
    protected $cacheExpiration = 30;


    /**
     * Валидатор модели.
     * @var Validator
     */
    protected $validator = NULL;


    /**
     * Валидация модели.
     * @return bool
     */
    abstract public function validate();


    /**
     * Сообщения об ошибках валидации.
     * @var array
     */
    protected $validationMessages = array(
        'required' => 'The :attribute field is required.',
        'unique' => 'The :attribute must be unique.',
        'exists' => 'Invalid :attribute.'
    );


    /**
     * Получить список ошибок валидации.
     * @return MessageBag
     */
    public function getValidationErrorMessages()
    {
        if ($this->validator) {
            return $this->validator->messages();
        } else {
            return NULL;
        }
    }


    /**
     * Найти пользователя опубликовавшего трек. Отношение один-к-одному.
     * @return User
     */
    public function author() {
        return $this->belongsTo('User', 'user_id');
    }


    /**
     * Информация об отложенной публикации объекта
     * @return PublicationQueue
     */
    public function queue()
    {
        return $this->morphMany('PublicationQueue', 'content');
    }


    /**
     * Информация о кросспостинге объекта
     * @return CrosspostingQueue
     */
    public function crosspostingQueue()
    {
        return $this->morphMany('CrosspostingQueue', 'content');
    }


    /**
     * Получить ссылку на ресурс.
     * @return string
     */
    abstract public function getUrl();


    /**
     * Возвращает букву-обозначение типа контента для короткой ссылки. 
     * Контент хранится в разных таблицах и одного id недостаточно для 
     * однозначной идентификации контента, поэтому используем литеру типа.
     * @return string
     */
    abstract public function getShortUrlLetter();


    /**
     * Получить короткую ссылку на контент.
     * @return string
     */
    public function getShortUrl()
    {
        $letter = $this->getShortUrlLetter();
        return url('/', array($letter, $this->id));
    }


    /**
     * Получить количество шэрингов в Google+. Значение кэшируется.
     * @return integer
     */
    public function getGPlusShareCount() {

        $cacheKey = get_class($this) . $this->id . 'gplus';

        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://clients6.google.com/rpc");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"'.$this->getUrl().'","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));

        $curl_results = curl_exec ($curl);

        curl_close ($curl);

        $json = json_decode($curl_results, true);
        $result = isset($json[0]['result']['metadata']['globalCounts']['count'])?intval( $json[0]['result']['metadata']['globalCounts']['count'] ):0;

        Cache::put($cacheKey, $result, $this->cacheExpiration);

        return $result;
    }


    /**
     * Получить количество шэрингов в Facebook. Значение кэшируется.
     * @return integer
     */
    public function getFbShareCount() {

        $cacheKey = get_class($this) . $this->id . 'fb';

        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $json_string = $this->file_get_contents_curl('http://api.facebook.com/restserver.php?method=links.getStats&format=json&urls='.$this->getUrl());
        $json = json_decode($json_string, true);

        $result = isset($json[0]['total_count'])?intval($json[0]['total_count']):0;

        Cache::put($cacheKey, $result, $this->cacheExpiration);

        return $result;
    }


    /**
     * Получить количество твитов со ссылкой на трек. Значение кэшируется.
     * @return integer
     */
    public function getTwShareCount() {

        $cacheKey = get_class($this) . $this->id . 'tw';

        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $json_string = $this->file_get_contents_curl('http://urls.api.twitter.com/1/urls/count.json?url=' . $this->getShortUrl());
        $json = json_decode($json_string, true);

        $result = isset($json[0]['total_count'])?intval($json[0]['total_count']):0;

        Cache::put($cacheKey, $result, $this->cacheExpiration);

        return $result;
    }


    /**
     * Получить количество шэрингов в VK. Значение кэшируется.
     * @return integer
     */
    public function getVkShareCount() {

        $cacheKey = get_class($this) . $this->id . 'vk';

        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $response = $this->file_get_contents_curl('http://vk.com/share.php?act=count&index=1&url=' . $this->getUrl());

        $result = array();
        preg_match('/^VK.Share.count\(1, (\d+)\);$/i', $response, $result);
        $result = isset($result[1]) ? $result[1] : 1;

        Cache::put($cacheKey, $result, $this->cacheExpiration);

        return $result;
    }


    /**
     * Запрос содержимого указанного URL.
     * @param  string  $url
     * @param  integer $retries кол-во попыток
     * @return string
     */
    private function file_get_contents_curl($url, $retries = 0) {
        $ua = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36';
        $ua = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/39.0.2171.65 Chrome/39.0.2171.65 Safari/537.36';

        if (extension_loaded('curl') === true)
        {
            $ch = curl_init();
     
            curl_setopt($ch, CURLOPT_URL, $url); // The URL to fetch. This can also be set when initializing a session with curl_init().
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2); // The number of seconds to wait while trying to connect.
            curl_setopt($ch, CURLOPT_USERAGENT, $ua); // The contents of the "User-Agent: " header to be used in a HTTP request.
            curl_setopt($ch, CURLOPT_FAILONERROR, TRUE); // To fail silently if the HTTP code returned is greater than or equal to 400.
            // Злая штука, из-за которой не работает функционал :(
            //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE); // To follow any "Location: " header that the server sends as part of the HTTP header.
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE); // To follow any "Location: " header that the server sends as part of the HTTP header.
            curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE); // To automatically set the Referer: field in requests where it follows a Location: redirect.
            curl_setopt($ch, CURLOPT_TIMEOUT, 3); // The maximum number of seconds to allow cURL functions to execute.
            curl_setopt($ch, CURLOPT_MAXREDIRS, 3); // The maximum number of redirects
     
            $result = trim(curl_exec($ch));
     
            curl_close($ch);
        }
     
        else
        {
            $result = trim(file_get_contents($url));
        }        
     
        if (empty($result) === true)
        {
            $result = false;
     
            if ($retries > 0)
            {
                usleep(500000);
                return $this->file_get_contents_curl($url, $retries--);
            }
        }    
     
        return $result;
    }

}