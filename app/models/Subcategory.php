<?php

class Subcategory extends Eloquent {

    /**
     * Найти треки относящиеся к категории.
     * Отношение один-ко-многим.
     * @return Collection
     */
    public function music()
    {
        return $this->hasMany('Music');
    }


    /**
     * Найти клипы относящиеся к категории.
     * Отношение один-ко-многим.
     * @return Collection
     */
    public function video()
    {
        return $this->hasMany('Video');
    }

}