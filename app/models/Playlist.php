<?php

class Playlist extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'playlists';

    /**
     * Лайки поставленные пользователем.
     */
    public function playlists()
    {
        return $this->belongsTo('User', 'user_id');
    }


}