<?php

class ItemToPlaylist extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'itemtoplaylists';


    /**
     * The guarded fields in table
     * @var array
     */
    protected $guarded = array('id', 'timestamp');
}