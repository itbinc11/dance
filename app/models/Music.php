<?php

/**
 * Модель трека.
 */
class Music extends Publication {

    /**
     * Имя таблицы в БД.
     * @var string
     */
    protected $table = 'music';


    /**
     * Защищённые от массового присваивания свойства.
     * @var array
     */
    protected $guarded = array();


    /**
     * Теги (текстовые метки) трека. Отношение один-ко-многим.
     * @return Tag
     */
    public function tags()
    {
        return $this->belongsToMany('Tag', 'tag_music', 'music_id', 'tag_id');
    }


    /**
     * Категория трека. Отношение один-к-одному.
     * @return Subcategory
     */
    public function subcategory()
    {
        return $this->belongsTo('Subcategory');
    }


    /**
     * Меняем размер обложки трека, согласно API: 
     * https://developers.soundcloud.com/docs/api/reference#artwork_url
     * Если обложка трека не указана, устанавливаем url обложки по умолчанию.
     * @param  string/null $value
     * @return void
     */
    public function setArtworkUrlAttribute($value)
    {
        if (!$value or empty($value)) {
            $this->attributes['artwork_url'] = asset('/img/artwork-default.jpg');
        } else {
            $value = str_replace('-large', '-t300x300', $value);
            $this->attributes['artwork_url'] = $value;
        }
    }


    /**
     * Валидация описания трека.
     * @return bool
     */
    public function validate()
    {
        // Определяем, трек создается или редактируется,
        // на основе этого выбираем правила валидации.
        if ($this->id) {

            // Редактирование
            $this->validator = Validator::make(
                $this->attributes,
                array(
                    'title' => "required|unique:music,title,{$this->id}",
                    'subcategory_id' => 'required|exists:subcategories,id'
                ),
                $this->validationMessages
            );

        } else {

            // Создание
            $this->validator = Validator::make(
                $this->attributes,
                array(
                    'title' => "required|unique:music,title",
                    'url' => "required|unique:music,url",
                    'subcategory_id' => 'required|exists:subcategories,id'
                ),
                $this->validationMessages
            );

        }

        return $this->validator->passes();

    }


    /**
     * Обновить SC-информацию о треке.
     * @return boolean
     */
    public function updateSCInfo()
    {
        $get = http_build_query(array(
                'url' => $this->attributes['url'],
                'client_id' => Config::get('app.apiKey')
            ));

        // разрешаем API URL из share URL'а
        $url = 'http://api.soundcloud.com/resolve.json';
        $url = $url . '?' . $get;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $var = curl_exec($ch);

        if (curl_errno($ch) or curl_getinfo($ch, CURLINFO_HTTP_CODE) == '404') { 
            return false;
        };

        $var = json_decode($var);

        curl_close($ch);
            
        $this->fill(array(
            'api_url' => $var->uri,
            'artwork_url' => $var->artwork_url,
            'download_url' => property_exists($var, 'download_url') ? $var->download_url : 0,
            'duration' => $var->duration,
            'favoritings_count' => property_exists($var, 'favoritings_count') ? $var->favoritings_count : 0,
            'playback_count' => property_exists($var, 'playback_count') ? $var->playback_count : 0,
            'purchase_url' => $var->purchase_url,
            'stream_url' => $var->stream_url,
            'url' => $var->permalink_url,
            'waveform_url' => $var->waveform_url,
        ));

        if (!$var->streamable) {
            $this->attributes['published'] = 0;
        }

        $this->save();

        return true;
    }


    /**
     * Сгенерировать html из plainText описания трека.
     * @return string
     */
    public function htmlDescription() {
        $ttl = new Eventviva\TextToLinks;
        $description = $ttl->makeLinks($this->description);
        return nl2br(htmlspecialchars_decode($description));
    }

    
    /**
     * Найти похожие по названию и тегам треки.
     * @param  integer $num количество треков
     * @return Collection
     */
    public function getRelated($num = 6)
    {
        // Найти треки с похожим названием.
        // подготовка запроса
        $query = Music::orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->take($num);

        // поиск треков, содержащих в названии аналогичные слова
        $searchTerms = explode(' ', $this->title);
        foreach ($searchTerms as $term)
        {
            // отбрасываем слова короче 3 символов
            if (strlen($term) < 3) {
                continue;
            }
            $query->orWhere('search', 'LIKE', '%'. ($term) .'%');
            $terms[] = $term;
        }
        $tracks = $query->get();

        // отфильтровываем снятые с публикации материалы
        $tracks = $tracks->filter(function($track) {
            return $track->published;
        });


        // Если треков менее $num, дополнить по тегам.
        if ($tracks->count() < $num and $this->tags()->count() > 0) {
            $tags = $this->tags;

            foreach($tags as $tag) {
                $tracks = $tracks->merge($tag->music()->where('published', 1)->get());

                // лишнего нам не надо)
                if ($tracks->count() > $num) {
                    break;
                }
            }
        }

        return $tracks->take($num);
    }


    /**
     * Получить ссылку на трек.
     * @return string
     */
    public function getUrl() {
        return URL::route('music/track', $this->slug);
    }


    /**
     * Литера типа контента.
     * @return string
     */
    public function getShortUrlLetter()
    {
        return 'm';
    }


    /**
     * Конвертировать длительность из мсек в человекочитаемый формат часы:минуты:секунды.
     * @return string
     */
    public function humanReadableDuration()
    {
        $duration = $this->attributes['duration'];

        $duration = floor($duration / 1000);

        $seconds = $duration % 60;
        $duration = floor($duration / 60);

        $minutes = $duration % 60;
        $duration = floor($duration / 60);

        $hours = $duration % 24;

        $humanReadableDuration = $hours > 0 ?
            $hours . ':' . sprintf('%02s', $minutes) . ':' . sprintf('%02s', $seconds):
            $minutes . ':' . sprintf('%02s', $seconds);

        return $humanReadableDuration;
    }


    /**
     * Конвертировать количество прослушиваний на soundcloud.com в 
     * человекочитаемый формат с аббревиатурой числа.
     * @return string
     */
    public function humanReadablePlaybackCount()
    {
        $count = $this->attributes['playback_count'];

        $HRCount = $this->countToAbbreviation($count);

        return $HRCount;

    }


    /**
     * Конвертировать количество добавлений в избранное на soundcloud.com 
     * в человекочитаемый формат с аббревиатурой числа.
     * @return string
     */
    public function humanReadableFavoritingsCount()
    {
        $count = $this->attributes['favoritings_count'];

        $HRCount = $this->countToAbbreviation($count);

        return $HRCount;

    }


    /**
     * Конвертировать число в человекочитаемый формат с абревиатурой числа.
     * @param  integer $count
     * @return string
     */
    private function countToAbbreviation($count)
    {
        if ($count > 1000000000) {
            $countWithAbbr = floor($count / 1000000000) . ' B';
        } elseif ($count > 1000000) {
            $countWithAbbr = floor($count / 1000000) . ' M';
        } elseif ($count > 1000) {
            $countWithAbbr = floor($count / 1000) . ' K';
        } else {
            $countWithAbbr = $count;
        }

        return $countWithAbbr;
    }

}