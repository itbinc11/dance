<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class PublicationQueue extends Eloquent {

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];

    protected $fillable = array('publication_date', 'queue');

    protected $table = 'publication_queue';


    public function content()
    {
        return $this->morphTo();
    }

}