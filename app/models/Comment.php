<?php



class Comment extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments';

    /**
     * Комментарии поставленные пользователем.
     */
    public function comments()
    {
        return $this->belongsTo('users', 'user_id');
    }


    /**
     * Получение последних комментариев для сайдбара
     * @return mixed
     */
    public static function lastComments() {

        $step = 4; //количество комметариев на сайдбаре

        // Полечение комментариев для музыкальных трэков
        $query_music = DB::table('comments')
            ->select(
                'comments.id as comment_id',
                'comments.user_id as comment_user_id',
                'comments.item_id as comment_item_id',
                'comments.item_type as comment_item_type',
                'comments.comment as comment_comment',
                'comments.created_at as comment_created_at',
                'comments.updated_at as comment_updated_at',
                'users.id as user_id',
                'users.email as user_email',
                'users.first_name as user_first_name',
                'users.last_name as user_last_name',
                'users.username as user_username',
                'users.avatar as user_avatar',
                'users.about as user_about',
                'users.created_at as user_created_at',
                'users.updated_at as user_updated_at',
                'music.title as item_title',
                'music.slug as item_slug',
                'music.url as item_url',
                'music.description as item_description',
                'music.user_id as item_user_id',
                'music.published as item_published',
                'music.created_at as item_created_at',
                'music.updated_at as item_updated_at'

            )
            ->join('users', 'users.id', '=', 'comments.user_id')
            ->join('music', 'music.id', '=', 'comments.item_id')
            ->where('item_type', 'music');

        // Полечение комментариев для видео клипов
        $query_video = DB::table('comments')
            ->select(
                'comments.id as comment_id',
                'comments.user_id as comment_user_id',
                'comments.item_id as comment_item_id',
                'comments.item_type as comment_item_type',
                'comments.comment as comment_comment',
                'comments.created_at as comment_created_at',
                'comments.updated_at as comment_updated_at',
                'users.id as user_id',
                'users.email as user_email',
                'users.first_name as user_first_name',
                'users.last_name as user_last_name',
                'users.username as user_username',
                'users.avatar as user_avatar',
                'users.about as user_about',
                'users.created_at as user_created_at',
                'users.updated_at as user_updated_at',
                'videos.title as item_title',
                'videos.slug as item_slug',
                'videos.url as item_url',
                'videos.description as item_description',
                'videos.user_id as item_user_id',
                'videos.published as item_published',
                'videos.created_at as item_created_at',
                'videos.updated_at as item_updated_at'

            )
            ->join('users', 'users.id', '=', 'comments.user_id')
            ->join('videos', 'videos.id', '=', 'comments.item_id')
            ->where('item_type', 'videos');

        // Полечение комментариев для событий
        $query_events = DB::table('comments')
            ->select(
                'comments.id as comment_id',
                'comments.user_id as comment_user_id',
                'comments.item_id as comment_item_id',
                'comments.item_type as comment_item_type',
                'comments.comment as comment_comment',
                'comments.created_at as comment_created_at',
                'comments.updated_at as comment_updated_at',
                'users.id as user_id',
                'users.email as user_email',
                'users.first_name as user_first_name',
                'users.last_name as user_last_name',
                'users.username as user_username',
                'users.avatar as user_avatar',
                'users.about as user_about',
                'users.created_at as user_created_at',
                'users.updated_at as user_updated_at',
                'music_events.title as item_title',
                'music_events.slug as item_slug',
                'music_events.url as item_url',
                'music_events.description as item_description',
                'music_events.user_id as item_user_id',
                'music_events.published as item_published',
                'music_events.created_at as item_created_at',
                'music_events.updated_at as item_updated_at'

            )
            ->join('users', 'users.id', '=', 'comments.user_id')
            ->join('music_events', 'music_events.id', '=', 'comments.item_id')
            ->where('item_type', 'music');

        // Объединение комментариев в одну коллекцию
        $query = $query_music->union($query_video)->union($query_events);

        // Вернуть последние комментарии, относительно $step
        return $query->orderBy('comment_created_at', 'desc')
            ->take($step)
            ->get();

    }

}