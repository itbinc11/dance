<?php

class MusicEvent extends Publication {

    protected $guarded = array();

    /**
     * Найти пользователя разместившего информацию о мероприятии. 
     * Отношение один-к-одному.
     * @return User
     */
    public function author() {
        return $this->belongsTo('User', 'user_id');
    }


    /**
     * Валидация описания события.
     * @return bool
     */
    public function validate()
    {
        // Определяем, событие создается или редактируется,
        // на основе этого выбираем правила валидации.
        if ($this->id) {

            // Редактирование
            $this->validator = Validator::make(
                $this->attributes,
                array(
                    'title' => "required|unique:music_events,title,{$this->id}",
                    'url' => "required|unique:music_events,url,{$this->id}"
                ),
                $this->validationMessages
            );

        } else {

            // Создание
            $this->validator = Validator::make(
                $this->attributes,
                array(
                    'title' => 'required|unique:music_events,title',
                    'url' => 'required|unique:music_events,url'
                ),
                $this->validationMessages
            );

        }

        return $this->validator->passes();

    }


    /**
     * Получить ссылку на событие.
     * @return string
     */
    public function getUrl() {
        return URL::route('events/event', $this->slug);
    }


    /**
     * Литера типа контента.
     * @return string
     */
    public function getShortUrlLetter()
    {
        return 'e';
    }

}