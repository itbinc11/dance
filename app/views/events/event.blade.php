@extends('index')

@section('head')
<meta property="og:title" content="{{ $singleEvent->title }}" />
<meta property="og:image" content="{{ asset($singleEvent->poster) }}" />
<meta property="og:description" content="More info about {{ $singleEvent->title }} on DanceProject.info" />
<meta property="og:url" content="{{ $singleEvent->getUrl() }}" />
@stop

@section('content')
            <div class="column content-full">
                
                <div class="column-title clearfix">
                    <div class="column-title-title">Events</div>
                </div>

                <div class="events-row clearfix">
                    <div class="popup-container" style="margin: 0 16px 16px 0; width: inherit; background: #2996C9 url(/img/publication-bg.png) left top no-repeat;">
                        <div class="popup-head clearfix">
                            <ul class="popup-soc-stats" style="margin: 5px 0 0 133px;">
                                <li class="popup-soc-stat popup-soc-gplus" onclick="popUp=window.open('https://plus.google.com/share?url={{ $singleEvent->getUrl() }}', 'Опубликовать ссылку в Google Plus', 'width=800,height=300');popUp.focus();return false">{{ $singleEvent->getGPlusShareCount() }}</li>
                                <li class="popup-soc-stat popup-soc-facebook" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{ $singleEvent->getUrl() }}', 'Опубликовать ссылку в Facebook', 'width=640,height=436,toolbar=0,status=0'); return false">{{ $singleEvent->getFbShareCount() }}</li>
                                <?php
                                    $hashtags =  '#events #danceproject';
                                    $url = $singleEvent->getUrl();
                                    $tweet = implode(' ', array(
                                        mb_substr($singleEvent->title, 0, (140 - 2 - 22 - mb_strlen($hashtags))),
                                        $url,
                                        $hashtags
                                    ));
                                ?>
                                <li class="popup-soc-stat popup-soc-sntw" onclick="window.open('http://twitter.com/intent/tweet?text={{ urlencode($tweet) }}', 'Опубликовать ссылку в Twitter', 'width=800,height=300'); return false">{{ $singleEvent->getTwShareCount() }}</li>
                                <li class="popup-soc-stat popup-soc-vk" onclick="window.open('http://vk.com/share.php?url={{ $singleEvent->getUrl() }}', 'Опубликовать ссылку во Вконтакте', 'width=800,height=300'); return false">{{ $singleEvent->getVkShareCount() }}</li>
                            </ul>
                        </div>

                        <div class="popup-event">
                            <div class="popup-event-card">
                                <img class="popup-event-poster" src="{{ $singleEvent->poster }}" alt="">
                                <div class="popup-event-info clearfix" style="width:450px;">
                                    <div class="popup-event-title">{{ $singleEvent->title }}</div>
                                    <ul class="popup-event-details">
                                        <li class="popup-event-detail"><div class="popup-detail-bg"><div class="popup-detail-title">Data</div></div><div class="popup-detail-value">{{ \Carbon\Carbon::parse($singleEvent->date)->format('G:i, j F Y') }}</div></li>
                                        <li class="popup-event-detail"><div class="popup-detail-bg"><div class="popup-detail-title">Place</div></div><div class="popup-detail-value">{{ $singleEvent->country }}, {{ $singleEvent->city }}, {{ $singleEvent->place }}</div></li>
                                        <li class="popup-event-detail"><div class="popup-detail-bg"><div class="popup-detail-title">Guest</div></div><div class="popup-detail-value">{{ $singleEvent->guest }}</div></li>
                                        <li class="popup-event-detail"><div class="popup-detail-bg"><div class="popup-detail-title">Registration</div></div><div class="popup-detail-value"><a href="{{ url($singleEvent->url) }}" target="_blank" class="popup-detail-link">URL</a></div></li>
                                    </ul>
                                </div>

                                <div class="popup-event-likes"> </div>

                                <ul class="popup-event-soc" style="width: 494px;">
                                    <li class="popup-event-soc-item"><a href="#" class="popup-video-soc-link popup-video-soc-sntw"> </a></li>
                                    <li class="popup-event-soc-item"><a href="#" class="popup-video-soc-link popup-video-soc-facebook"> </a></li>
                                    <li class="popup-event-soc-item"><a href="#" class="popup-video-soc-link popup-video-soc-web"> </a></li>
                                </ul>
                            </div>

                            <div class="popup-event-about">
                                <div class="popup-about-title">About event</div>
                                <div class="popup-about-description">{{ $singleEvent->description }}</div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="events-row clearfix">
                    @foreach($events as $event)
                    <div class="column event-column">

                        <div class="event-card">
                            <a href="{{ URL::route('events/event', $event->slug) }}" class="imglink"><img src="{{ $event->poster }}" alt="" class="event-card-poster"></a>
                            <div class="event-card-title">{{ $event->title }}</div>
                            <div class="event-card-info">{{ $event->date }}</div>
                            <div class="event-card-info">{{ $event->place }}</div>
                            <div class="event-card-info">{{ $event->guest }}</div>
                            <a href="{{ URL::route('events/event', $event->slug) }}" class="event-card-button js-ajax-popup">Learn more</a>
                        </div>

                    </div>
                    @endforeach
                </div>

                <a href="{{ URL::to('/events/page', 2) }}" class="view-more-button js-ajax-more">View more events</a>
                
            </div>
@stop