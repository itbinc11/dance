<div class="popup-container">
    <div class="popup-head clearfix">
        <a href="/" class="popup-logo">Dance project</a>
        <ul class="popup-soc-stats">
            <li class="popup-soc-stat popup-soc-gplus" onclick="popUp=window.open('https://plus.google.com/share?url={{ $event->getUrl() }}', 'Опубликовать ссылку в Google Plus', 'width=800,height=300');popUp.focus();return false">{{ $event->getGPlusShareCount() }}</li>
            <li class="popup-soc-stat popup-soc-facebook" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{ $event->getUrl() }}', 'Опубликовать ссылку в Facebook', 'width=640,height=436,toolbar=0,status=0'); return false">{{ $event->getFbShareCount() }}</li>
            <?php
                $hashtags =  '#events #danceproject';
                $url = $event->getUrl();
                $tweet = implode(' ', array(
                    mb_substr($event->title, 0, (140 - 2 - 22 - mb_strlen($hashtags))),
                    $url,
                    $hashtags
                ));
            ?>
            <li class="popup-soc-stat popup-soc-sntw" onclick="window.open('http://twitter.com/intent/tweet?text={{ urlencode($tweet) }}', 'Опубликовать ссылку в Twitter', 'width=800,height=300'); return false">{{ $event->getTwShareCount() }}</li>
            <li class="popup-soc-stat popup-soc-vk" onclick="window.open('http://vk.com/share.php?url={{ $event->getUrl() }}', 'Опубликовать ссылку во Вконтакте', 'width=800,height=300'); return false">{{ $event->getVkShareCount() }}</li>
        </ul>
        <a href="#" class="popup-close"> </a>
    </div>

    <div class="popup-event">
        <div class="popup-event-card">
            <a href="{{ URL::route('events/event', $event->slug) }}" class="imglink"><img class="popup-event-poster" src="{{ $event->poster }}" alt=""></a>
            <div class="popup-event-info clearfix">
                <div class="popup-event-title">{{ $event->title }}</div>
                <ul class="popup-event-details">
                    <li class="popup-event-detail"><div class="popup-detail-bg"><div class="popup-detail-title">Data</div></div><div class="popup-detail-value">{{ \Carbon\Carbon::parse($event->date)->format('G:i, j F Y') }}</div></li>
                    <li class="popup-event-detail"><div class="popup-detail-bg"><div class="popup-detail-title">Place</div></div><div class="popup-detail-value">{{ $event->country }}, {{ $event->city }}, {{ $event->place }}</div></li>
                    <li class="popup-event-detail"><div class="popup-detail-bg"><div class="popup-detail-title">Guest</div></div><div class="popup-detail-value">{{ $event->guest }}</div></li>
                    <li class="popup-event-detail"><div class="popup-detail-bg"><div class="popup-detail-title">Registration</div></div><div class="popup-detail-value"><a href="{{ url($event->url) }}" target="_blank" class="popup-detail-link">URL</a></div></li>
                </ul>
            </div>
            
            <div class="{{$like}}" data-id="{{ $event->id }}"> </div>

            <ul class="popup-event-soc">
                <li class="popup-event-soc-item"><a href="#" class="popup-video-soc-link popup-video-soc-sntw"> </a></li>
                <li class="popup-event-soc-item"><a href="#" class="popup-video-soc-link popup-video-soc-facebook"> </a></li>
                <li class="popup-event-soc-item"><a href="#" class="popup-video-soc-link popup-video-soc-web"> </a></li>
            </ul>

            <a href="#" class="popup-event-prev js-event-change"> </a>
            <a href="#" class="popup-event-next js-event-change"> </a>
        </div>

        <div class="popup-event-about">
            <div class="popup-about-title">About event</div>
            <div class="popup-about-description">{{ $event->description }}</div>
        </div>

    </div>
</div>