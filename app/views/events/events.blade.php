@extends('index')

@section('content')
            <div class="column content-full">
                
                <div class="column-title clearfix">
                    <div class="column-title-title">Events</div>
                </div>

                <div class="events-row clearfix">
                    @foreach($events as $event)
                    <div class="column event-column">

                        <div class="event-card">
                            <a href="{{ URL::route('events/event', $event->slug) }}" class="imglink"><img src="{{ $event->poster }}" alt="" class="event-card-poster"></a>
                            <div class="event-card-title">{{ $event->title }}</div>
                            <div class="event-card-info">{{ \Carbon\Carbon::parse($event->date)->format('G:i, j F Y') }}</div>
                            <div class="event-card-info">{{ $event->place }}</div>
                            <div class="event-card-info">{{ $event->guest }}</div>
                            <a href="{{ URL::route('events/event', $event->slug) }}" class="event-card-button js-ajax-popup">Learn more</a>
                        </div>

                    </div>
                    @endforeach
                </div>

                <a href="{{ URL::to('/events/page', 2) }}" class="view-more-button js-ajax-more">View more events</a>

            </div>
@stop