<div class="popup-music-card">
    <img class="popup-music-preview" src="{{ $track->artwork_url }}" alt="">
    <div class="popup-music-info clearfix">
        <div class="popup-music-title">{{ $track->title }}</div>
        <div class="popup-music-artist">{{ $track->artist }}</div>
        <div class="progBarWrapper">
            <div class="loadBar"></div>
            <div class="progBar"></div>
            <img class="waveBar" src="{{ $track->waveform_url or '/img/track-gist.png' }}" alt="">
        </div>
        <div class="popup-music-controls">
            <div type="range" class="popup-music-volume"></div>
            <div class="popup-music-indicators">
                <span class="popup-music-position">0:00</span>
                /
                <span class="popup-music-duration">{{ $track->humanReadableDuration() }}</span>
            </div>
            @if (!empty($track->purchase_url))
            <a class="popup-music-download-link popup-music-purchase-link" href="{{ $track->purchase_url }}" target="_blank" rel="nofollow">Purchase</a>
            @elseif (!empty($track->download_url))
            <a class="popup-music-download-link" href="{{ $track->download_url . '?client_id=' . Config::get('app.apiKey') }}">Download</a>
            @else
            <span class="popup-music-download-link popup-music-download-disabled" href="#">Download</span>
            @endif
        </div>
    </div>

    @if (!empty($track->purchase_url))
    <a class="purchase-icon" href="{{ $track->purchase_url }}" target="_blank" rel="nofollow"> </a>
    @elseif (!empty($track->download_url))
    <a class="download-icon" href="{{ $track->download_url . '?client_id=' . Config::get('app.apiKey') }}"> </a>
    @else
    <span class="download-icon download-icon-disabled"> </span>
    @endif

    <div class="{{$like}}" data-id="{{ $track->id }}"> </div>

    <a href="#" class="popup-music-prev js-music-change"> </a>
    <a href="#" class="popup-music-play" data-stream-url="{{ $track->stream_url }}"> </a>
    <a href="#" class="popup-music-next js-music-change"> </a>
</div>
@if(Sentry::check())
<script>
$('.popup-music-likes, .popup-music-liked').click(function() {
    var like = $(this);
    $.ajax({
        type: 'post',
        data: { id: $(this).data('id'), user: {{ Sentry::getUser()->id }} },
        url: "/musiclike",
        success: function(result){
            if(result) {
                like.attr('class', 'popup-music-liked');
            } else {
                like.attr('class', 'popup-music-likes');
            }
        }
    });
});
</script>
@endif