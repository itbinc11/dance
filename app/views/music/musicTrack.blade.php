@extends('music/music')

{{-- Open Graph описание трека  --}}
@section('head')
<meta property="og:title" content="{{ $singleTrack->title }}" />
<meta property="og:image" content="{{ $singleTrack->artwork_url }}" />
<meta property="og:description" content="More {{ $singleTrack->subcategory->title }} stuff you will find on Danceproject.info" />
<meta property="og:url" content="{{ $singleTrack->getUrl() }}" />
@stop

@section('content')
<div class="column content-full">

                <div class="column-title clearfix">
                    <div class="column-title-title">Music</div>
                </div>

                <div class="music-row clearfix">

                <div class="popup-container" style="margin: 0 16px 16px 0; width: inherit; background: #2996C9 url(/img/publication-bg.png) left top no-repeat;">
                    <div class="popup-head clearfix">
                        <ul class="popup-soc-stats" style="margin: 5px 0 0 133px;">
                            <li class="popup-soc-stat popup-soc-gplus" onclick="popUp=window.open('https://plus.google.com/share?url={{ $singleTrack->getUrl() }}', 'Опубликовать ссылку в Google Plus', 'width=800,height=300');popUp.focus();return false">{{ $singleTrack->getGPlusShareCount() }}</li>
                            <li class="popup-soc-stat popup-soc-facebook" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{ $singleTrack->getUrl() }}', 'Опубликовать ссылку в Facebook', 'width=640,height=436,toolbar=0,status=0'); return false">{{ $singleTrack->getFbShareCount() }}</li>
                            <?php
                                    $hashtags =  '#' . $singleTrack->subcategory->slug . ' #music #danceproject';
                                    $url = $singleTrack->getUrl();
                                    $tweet = implode(' ', array(
                                        mb_substr($singleTrack->title, 0, (140 - 2 - 22 - mb_strlen($hashtags))),
                                        $url,
                                        $hashtags
                                    ));
                                ?>
                            <li class="popup-soc-stat popup-soc-sntw" onclick="window.open('http://twitter.com/intent/tweet?text={{ urlencode($tweet) }}', 'Опубликовать ссылку в Twitter', 'width=800,height=300'); return false">{{ $singleTrack->getTwShareCount() }}</li>
                            <li class="popup-soc-stat popup-soc-vk" onclick="window.open('http://vk.com/share.php?url={{ $singleTrack->getUrl() }}', 'Опубликовать ссылку во Вконтакте', 'width=800,height=300'); return false">{{ $singleTrack->getVkShareCount() }}</li>
                        </ul>
                    </div>

                    <div class="popup-music">
                        <div class="popup-music-card">
                            <img class="popup-music-preview" src="{{ $singleTrack->artwork_url }}" alt="">
                            <div class="popup-music-info clearfix" style="width: 450px;">
                                <div class="popup-music-title" style="width: auto;">{{ $singleTrack->title }}</div>
                                <div class="popup-music-artist">{{ $singleTrack->artist }}</div>
                                <div class="progBarWrapper--main" style="width: auto;">
                                    <div class="loadBar--main"></div>
                                    <div class="progBar--main"></div>
                                    <img class="waveBar" src="{{ $singleTrack->waveform_url or '/img/track-gist.png' }}" alt="">
                                </div>
                                <div class="popup-music-controls">
                                    <div type="range" class="popup-music-volume--main"></div>
                                    <div class="popup-music-indicators">
                                        <span class="popup-music-position--main">0:00</span>
                                        /
                                        <span class="popup-music-duration">{{ $singleTrack->humanReadableDuration() }}</span>
                                    </div>
                                    @if (!empty($singleTrack->purchase_url))
                                    <a class="popup-music-download-link popup-music-purchase-link" href="{{ $singleTrack->purchase_url }}" target="_blank" rel="nofollow">Purchase</a>
                                    @elseif (!empty($singleTrack->download_url))
                                    <a class="popup-music-download-link" href="{{ $singleTrack->download_url . '?client_id=' . Config::get('app.apiKey') }}">Download</a>
                                    @else
                                    <span class="popup-music-download-link popup-music-download-disabled" href="#">Download</span>
                                    @endif
                                </div>
                            </div>

                            @if (!empty($singleTrack->purchase_url))
                            <a class="purchase-icon" href="{{ $singleTrack->purchase_url }}" target="_blank" rel="nofollow"> </a>
                            @elseif (!empty($singleTrack->download_url))
                            <a class="download-icon" href="{{ $singleTrack->download_url . '?client_id=' . Config::get('app.apiKey') }}"> </a>
                            @else
                            <span class="download-icon download-icon-disabled"> </span>
                            @endif

                            <div class="popup-music-likes"> </div>

                            <a href="#" class="popup-music-play--main pause" data-stream-url="{{ $singleTrack->stream_url }}"> </a>
                        </div>

                        <div class="popup-video-about">
                            <div class="popup-about-title">about music</div>
                            <div class="popup-about-description">{{ $singleTrack->htmlDescription() }}</div>
                            <ul class="popup-video-soc">
                                <li class="popup-video-soc-item"><a href="{{ $singleTrack->url }}" target="_blank" rel="nofollow" class="popup-video-soc-link popup-video-soc-soundcloud"> </a></li>
                                <li class="popup-video-soc-item"><a href="{{ $singleTrack->user_twitter or '#' }}" target="_blank" rel="nofollow" class="popup-video-soc-link popup-video-soc-sntw"> </a></li>
                                <li class="popup-video-soc-item"><a href="{{ $singleTrack->user_facebook or '#' }}" target="_blank" rel="nofollow" class="popup-video-soc-link popup-video-soc-facebook"> </a></li>
                                <li class="popup-video-soc-item"><a href="{{ $singleTrack->user_personal or '#' }}" target="_blank" rel="nofollow" class="popup-video-soc-link popup-video-soc-web"> </a></li>
                            </ul>
                            <ul class="popup-video-tags">
                                @foreach($singleTrack->tags()->orderBy('title')->get() as $tag)
                                <li class="popup-video-tag"><a href="/search?q={{ urlencode('"'.$tag->title.'"') }}" class="popup-video-link">{{ $tag->title }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="music-related">
                            <a href="/search?q={{ urlencode($singleTrack->title) }}" class="music-related-button">Related music</a>
                            @foreach ($singleTrack->getRelated(5) as $relatedTrack)
                                <a href="{{ URL::route('music/track', $relatedTrack->slug) }}" class="music-related-preview music-related-preview--slim">
                                    <img class="music-related-preview-img" src="{{ $relatedTrack->artwork_url }}" alt="">
                                </a>
                            @endforeach
                        </div>

                    </div>
                </div>

                    @foreach($music as $track)
                    <div class="music-card">
                        <img class="music-card-cover" src="{{ $track->artwork_url }}" alt="">
                        <a href="{{ URL::route('music/track', $track->slug) }}" class="music-card-play js-ajax-popup"> </a>
                        <div class="music-card-description">
                            <div class="music-card-title">{{{ $track->title }}}</div>
                            <div class="music-card-misc clearfix">
                                <ul class="music-card-stats">
                                    <li class="music-card-listens">{{ $track->humanReadablePlaybackCount() }}</li>
                                    <li class="music-card-likes">{{ $track->humanReadableFavoritingsCount() }}</li>
                                    <li class="music-card-comments">0</li>
                                </ul>
                                <ul class="music-card-social">
                                    <li class="music-card-social-item"><a href="{{ $track->url }}" target="_blank" class="music-card-soundcloud"> </a></li>
                                    <li class="music-card-social-item"><a href="{{ $track->user_facebook or '#' }}" target="_blank" class="music-card-facebook"> </a></li>
                                    <li class="music-card-social-item"><a href="{{ $track->user_twitter or '#' }}" target="_blank" class="music-card-twitter"> </a></li>
                                    <li class="music-card-social-item"><a href="{{ $track->user_personal or '#' }}" target="_blank" class="music-card-web"> </a></li>
                                </ul>
                            </div>
                            <ul class="music-card-tags">
                                @foreach($track->tags()->orderBy('title')->get() as $tag)
                                <li class="music-card-tag">
                                    <a class="music-card-link" href="/search?q={{ urlencode('"'.$tag->title.'"') }}">{{ $tag->title }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endforeach

                </div>

                <a href="{{ URL::to('/music/page', 2) }}" class="view-more-button js-ajax-more">View more music</a>

            </div>

@stop

@section('documentEnd')
    <script type="text/javascript">
    $(document).ready(function() {

        soundManager.onready(function() {

            window.mainSound = soundManager.createSound({
                url: $('.popup-music-play--main').data('streamUrl') + '?client_id=7e370803e1ff451fe67265a2bccbdb3a',
                autoPlay: false,
                volume: 75,
                whileplaying: function() {
                    $('.progBar--main').css('width', ((this.position/this.duration) * 100) + '%');
                    $('.popup-music-position--main').html(timecode(Math.floor(this.position)));
                },
                whileloading: function() {
                    $('.loadBar--main').css('width', ((this.bytesLoaded/this.bytesTotal)*100)+'%');
                },
                onfinish: function() {
                    $('.progBar--main').css('width', '0');
                    $('.popup-music-play--main').toggleClass('pause');
                }
            });

            $('.popup-music-volume--main').noUiSlider({
                start: 75,
                range: {
                    'min': 0,
                    'max': 100
                }
            });

            $(document).on('change', '.popup-music-volume--main', function() {
                mainSound.setVolume(Math.floor($(this).val()));
            });

            // Play/pause по нажатию кнопки плеера.
            $(document).on('click', '.popup-music-play--main', function() {
                if ($(this).hasClass('pause')) {
                    if (mainSound.playState) {
                        mainSound.resume();
                        $(this).removeClass('pause');
                    } else {
                        mainSound.play();
                        $(this).removeClass('pause');
                    }
                } else {
                    mainSound.pause();
                    $(this).addClass('pause');
                }

                return false;
            });


            // Перемотка трека.
            $(document).on('click', '.progBarWrapper--main', function (e) {
                var $this = $(this);
                var posX = $this.offset().left;
                var relPosX = e.pageX - posX;

                var position = Math.floor(relPosX / $this.innerWidth() * 100) / 100;
                var loading = Math.floor(mainSound.bytesLoaded/mainSound.bytesTotal * 100) / 100;

                if (loading > position) {
                    mainSound.setPosition(Math.floor(mainSound.duration * position));
                }
            });

            // Отключаем drag-n-drop гистограммы трека.
            $(document).on('dragstart', '.waveBar', function(event) { event.preventDefault(); });

            var timecode = function (ms) {
                var hms = function (ms) {
                        return {
                            h: Math.floor(ms / (60 * 60 * 1000)),
                            m: Math.floor((ms / 60000) % 60),
                            s: Math.floor((ms / 1000) % 60)
                        };
                    }(ms),
                    tc = []; // Timecode array to be joined with '.'

                if (hms.h > 0) {
                    tc.push(hms.h);
                }

                tc.push((hms.m < 10 && hms.h > 0 ? "0" + hms.m : hms.m));
                tc.push((hms.s < 10 ? "0" + hms.s : hms.s));

                return tc.join(':');
            };

            $(document).on('click', '.js-ajax-popup', function(event) {
                mainSound.pause();
                $('.popup-music-play--main').addClass('pause');
            })
        
        });
    
    });

    </script>
@stop