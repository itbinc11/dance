<div class="popup-container popup-music-wrap">
    <div class="popup-head clearfix">
        <a href="/" class="popup-logo">Dance project</a>
        <ul class="popup-soc-stats">
            <li class="popup-soc-stat popup-soc-gplus" onclick="popUp=window.open('https://plus.google.com/share?url={{ $track->getUrl() }}', 'Опубликовать ссылку в Google Plus', 'width=800,height=300');popUp.focus();return false">{{ $track->getGPlusShareCount() }}</li>
            <li class="popup-soc-stat popup-soc-facebook" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{ $track->getUrl() }}', 'Опубликовать ссылку в Facebook', 'width=640,height=436,toolbar=0,status=0'); return false">{{ $track->getFbShareCount() }}</li>
            <?php
                    $hashtags =  '#' . $track->subcategory->slug . ' #music #danceproject';
                    $url = $track->getUrl();
                    $tweet = implode(' ', array(
                        mb_substr($track->title, 0, (140 - 2 - 22 - mb_strlen($hashtags))),
                        $url,
                        $hashtags
                    ));
                ?>
            <li class="popup-soc-stat popup-soc-sntw" onclick="window.open('http://twitter.com/intent/tweet?text={{ urlencode($tweet) }}', 'Опубликовать ссылку в Twitter', 'width=800,height=300'); return false">{{ $track->getTwShareCount() }}</li>
            <li class="popup-soc-stat popup-soc-vk" onclick="window.open('http://vk.com/share.php?url={{ $track->getUrl() }}', 'Опубликовать ссылку во Вконтакте', 'width=800,height=300'); return false">{{ $track->getVkShareCount() }}</li>
        </ul>
        <a href="#" class="popup-close arcticmodal-close"> </a>
    </div>

    <div class="popup-music">
        <div class="popup-music-card">
            <img class="popup-music-preview" src="{{ $track->artwork_url }}" alt="">
            <div class="popup-music-info clearfix">
                <div class="popup-music-title">{{ $track->title }}</div>
                <div class="popup-music-artist">{{ $track->artist }}</div>
                <div class="progBarWrapper">
                    <div class="loadBar"></div>
                    <div class="progBar"></div>
                    <img class="waveBar" src="{{ $track->waveform_url or '/img/track-gist.png' }}" alt="">
                </div>
                <div class="popup-music-controls">
                    <div type="range" class="popup-music-volume"></div>
                    <div class="popup-music-indicators">
                        <span class="popup-music-position">0:00</span>
                        /
                        <span class="popup-music-duration">{{ $track->humanReadableDuration() }}</span>
                    </div>
                    @if (!empty($track->purchase_url))
                    <a class="popup-music-download-link popup-music-purchase-link" href="{{ $track->purchase_url }}" target="_blank" rel="nofollow">Purchase</a>
                    @elseif (!empty($track->download_url))
                    <a class="popup-music-download-link" href="{{ $track->download_url . '?client_id=' . Config::get('app.apiKey') }}">Download</a>
                    @else
                    <span class="popup-music-download-link popup-music-download-disabled" href="#">Download</span>
                    @endif
                </div>
            </div>

            @if (!empty($track->purchase_url))
            <a class="purchase-icon" href="{{ $track->purchase_url }}" target="_blank" rel="nofollow"> </a>
            @elseif (!empty($track->download_url))
            <a class="download-icon" href="{{ $track->download_url . '?client_id=' . Config::get('app.apiKey') }}"> </a>
            @else
            <span class="download-icon download-icon-disabled"> </span>
            @endif

            <div class="{{$like}}" data-id="{{ $track->id }}"> </div>

            <a href="#" class="popup-music-prev js-music-change"> </a>
            <a href="#" class="popup-music-play" data-stream-url="{{ $track->stream_url }}"> </a>
            <a href="#" class="popup-music-next js-music-change"> </a>
        </div>

        <div class="popup-video-about">
            <div class="popup-about-title">about music</div>
            <div class="popup-about-description">{{ $track->htmlDescription() }}</div>
            <ul class="popup-video-soc">
                <li class="popup-video-soc-item"><a href="{{ $track->url }}" target="_blank" rel="nofollow" class="popup-video-soc-link popup-video-soc-soundcloud"> </a></li>
                <li class="popup-video-soc-item"><a href="{{ $track->user_twitter or '#' }}" target="_blank" rel="nofollow" class="popup-video-soc-link popup-video-soc-sntw"> </a></li>
                <li class="popup-video-soc-item"><a href="{{ $track->user_facebook or '#' }}" target="_blank" rel="nofollow" class="popup-video-soc-link popup-video-soc-facebook"> </a></li>
                <li class="popup-video-soc-item"><a href="{{ $track->user_personal or '#' }}" target="_blank" rel="nofollow" class="popup-video-soc-link popup-video-soc-web"> </a></li>
            </ul>
            <ul class="popup-video-tags">
                @foreach($track->tags()->orderBy('title')->get() as $tag)
                <li class="popup-video-tag"><a href="/search?q={{ urlencode('"'.$tag->title.'"') }}" class="popup-video-link">{{ $tag->title }}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="music-related">
            <a href="/search?q={{ urlencode($track->title) }}" class="music-related-button">Related music</a>
            @foreach ($relatedTracks as $relatedTrack)
                <a href="{{ URL::route('music/track', $relatedTrack->slug) }}" class="music-related-preview">
                    <img class="music-related-preview-img" src="{{ $relatedTrack->artwork_url }}" alt="">
                </a>
            @endforeach
        </div>        
        </div>
    </div>
<div class="form-music-wrap">
    @if(Sentry::check())
    <div class="form-music">
        <strong class="com-qty">2 comments</strong>
        {{ Form::open(array('route' => 'addComment')) }}
        <div class="redactor-wrapper">
            <textarea class="content-form-field content-form-field-textarea content-form-field-comment js-redactor" name="comment"></textarea>
            {{ Form::submit('SEND', array('name'=>'submit', 'class' => 'send-comment music-related-button')) }}
        </div>
        <div class="column captcha">
            <span class="form-description" >CAPTCHA </span>
            {{ HTML::image(Captcha::img(), 'Captcha image', array('id' => 'captcha')) }}
        </div>
        <div class="column">
            <span class="form-description" >WHAT CODE IS IN IMAGE? </span>
            {{ Form::text('captcha', null, array('class' => 'form__input captcha_register')) }}
            <div class="captcha_alert input-caption input-alert"></div>
            <span class="input-caption" >Enter the characters(without spaces) shown in the image.</span>
        </div>
        {{ Form::text('item_id', $track->id, array('hidden'=>'hidden')) }}
        {{ Form::text('item_type', 'music', array('hidden'=>'hidden')) }}
        {{ Form::close() }}
    </div>
    @endif
    <div class="content-comments">
    @if($comments)
    <ul class="comments-list">
        @foreach($comments as $item)
        <li class="clearfix">
            @if($item->user_avatar)
                <img class="content-form-image" height="40" width="auto" alt="avatar" src="/img/avatars/{{ $item->user_avatar }}">
            @else
                <img class="content-form-image" height="40" width="auto" alt="avatar" src="/img/avatars/noavatar.png">
            @endif
            <div class="content-wrap">
                <div class="content-header-title">{{ $item->user_username }}</div>
                <div class="content-header-time">{{ $item->comment_created_at }}</div>
                <div class="clearfix"></div>
                <div class="content-header-comment">{{ $item->comment_comment }}</div>
            </div>     
        </li>
        @endforeach
    </ul>
    @else
    No comments.
    @endif
    </div>
</div>