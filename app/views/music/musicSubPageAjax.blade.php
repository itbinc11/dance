                
                <div class="column-title clearfix">
                    <div class="column-title-title">Page {{ $page }}</div>
                </div>

                <div class="music-row clearfix">

                    @foreach($music as $track)
                    <div class="music-card">
                        <img class="music-card-cover" src="{{ $track->artwork_url }}" alt="">
                        <a href="{{ URL::route('music/track', $track->slug) }}" class="music-card-play js-ajax-popup"> </a>
                        <div class="music-card-description">
                            <div class="music-card-title">{{{ $track->title }}}</div>
                            <div class="music-card-misc clearfix">
                                <ul class="music-card-stats">
                                    <li class="music-card-listens">{{ $track->humanReadablePlaybackCount() }}</li>
                                    <li class="music-card-likes">{{ $track->humanReadableFavoritingsCount() }}</li>
                                    <li class="music-card-comments">0</li>
                                </ul>
                                <ul class="music-card-social">
                                    <li class="music-card-social-item"><a href="{{ $track->url }}" target="_blank" class="music-card-soundcloud"> </a></li>
                                    <li class="music-card-social-item"><a href="{{ $track->user_facebook or '#' }}" target="_blank" class="music-card-facebook"> </a></li>
                                    <li class="music-card-social-item"><a href="{{ $track->user_twitter or '#' }}" target="_blank" class="music-card-twitter"> </a></li>
                                    <li class="music-card-social-item"><a href="{{ $track->user_personal or '#' }}" target="_blank" class="music-card-web"> </a></li>
                                </ul>
                            </div>
                            <ul class="music-card-tags">
                                @foreach($track->tags()->orderBy('title')->get() as $tag)
                                <li class="music-card-tag">
                                    <a class="music-card-link" href="/search?q={{ urlencode('"'.$tag->title.'"') }}">{{ $tag->title }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endforeach

                </div>

                <a href="{{ URL::to('/music', array($sub->slug, $page+1)) }}" class="view-more-button js-ajax-more">View more music</a>