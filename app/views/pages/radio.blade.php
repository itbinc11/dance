<head>
<!-- Мета-теги -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
	<title>Radio Danceproject - First radio for Dancers | Danceproject</title>
	<meta name="description" content="Radio Danceproject is the first radio for the dancers. We broadcasting daily different music such as Hip-Hop, House, Popping, Locking and other. Check it out daily from 21:00-24:00 CET">
<!-- Подключенные стили -->
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/js/arcticModal/jquery.arcticmodal-0.3.css">
    <link rel="stylesheet" type="text/css" href="/js/nouislider/jquery.nouislider.css">
	<style>
		audio {width: 600px;}
		#vk_groups {float: right; margin-top: 50px; margin-right: 50px; height: 100%; width:300px;}
		.footer2 {margin-top: 100px;}
		.radio {width: 600px; height: 100%; margin-left: 180px;}
	</style>
<!-- Подключение скрипта ВК -->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>

</head>

<body>
<!-- Описание скрипта Facebook -->
    <div id="fb-root"></div>
		<script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=508246125873045&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
		</script>

<!-- Подключение меню -->
    @yield('submenu')
    <div class="head-navigation-b">
        <div class="head-navigation container">
            <a class="head-navigation-textlogo" href="/">Dance Project</a>
            <img class="head-navigation-logo" src="/img/logo.png" alt="Dance Project logo">
            <ul class="head-category">
                <li class="head-category-item"><a href="/music" class="head-category-link">Music</a></li>
                <li class="head-category-item"><a href="/videos" class="head-category-link">Video</a></li>
                <li class="head-category-item"><a href="/events" class="head-category-link">Events</a></li>
            </ul>
            <form action="/search" method="get">
                <input type="text" name="q" class="head-search" placeholder="search">
            </form>
            <span class="head-sign-in">Sign in</span>
        </div>
    </div>
	
<!-- VK Widget -->
	<div id="vk_groups">
		<script type="text/javascript">
			VK.Widgets.Group("vk_groups", {mode: 0, width: "300", height: "200", color1: 'FFFFFF', color2: '000000', color3: '3E71AC'}, 26484927);
		</script>
		<div class="fb-like-box" data-href="https://www.facebook.com/danceproject.info" data-width="300" data-height="200" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true"></div>
	</div>


<center>
		<h1 class="head-category-item">DANCEPROJECT RADIO</h1>
		<p class="credits-copyright">Now you are listening to:</p>
	<img style="width: 400px; height: 300px;" src="http://img1.jurko.net/wall/paper/donald_duck_4.jpg"> 
		</center>
<p>

	<div class="radio">
		<audio id=music controls="controls">
		<source src="http://46.98.82.250:8000/stream" type="audio/mp3" />
		<em>Sorry, your browser doesn't support HTML5 audio.</em>
		</audio>
		<p>
		<!-- <iframe src="http://46.98.82.250:8000/durshlag.ogg" width="600" height="50" frameborder="0">   <p>Your browser does not support iframes.</p> </iframe> -->
<p>
	<!-- Подключение комментов -->
	<div class="fb-comments" data-href="http://danceproject.info/radio" data-width="600" data-numposts="10" data-colorscheme="light"></div>
	</div>


<!-- Подключение меню подвала -->
<div class="footer2">
<div class="footer-menu-b">
	<ul class="footer-menu container">
		<li class="footer-menu-item"><a href="/" class="footer-menu-link">Home</a></li>
		<li class="footer-menu-item"><a href="#" class="footer-menu-link">Contact us</a></li>
		<li class="footer-menu-item"><a href="#" class="footer-menu-link">About us</a></li>
		<li class="footer-menu-item"><a href="#" class="footer-menu-link">DMCA</a></li>
		<li class="footer-menu-item"><a href="#" class="footer-menu-link">Sitemap</a></li>
	</ul>
</div>
<!-- Подключение подвала, подписи и тд -->
<div class="footer-b">
	<div class="footer container">
		<div class="credits">
			<a class="credits-logo" href="/">Dance Project</a>
			<p class="credits-copyright">© Durshlag 2014</p>
		</div>
	</div>
</div>
</div>


