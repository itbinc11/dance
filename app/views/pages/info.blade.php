@extends('index')

@section('submenu')
    <div class="head-tag-b">
        <ul class="head-tag container">
            @foreach($musicSubcategories as $cat)
            <li class="head-tag-item"><a class="head-tag-link" href='/music/{{ $cat->slug }}'>{{ $cat->title }}</a></li>
            @endforeach
        </ul>
    </div>
@stop

@section('content')
            <div class="column content-full">

                <div class="clearfix">
                    <div class="alert">
                        @if(Session::get('alerts'))
                        <?php $alerts = Session::get('alerts'); ?>

                        @foreach($alerts as $key => $alert)
                        {{ $alert }}
                        @endforeach
                        @endif
                    </div>

                    {{ Form::open(array('id' => 'info'), array('route' => array('changeInfo') )) }}
                                            <div class="column-title clearfix">
                                                <div class="column-title-title">MY INFORMATION</div>
                                            </div>
                    <div class="profile-card clearfix">

                        <div class="card-row">
                            <img height="100" width="auto" src="/img/avatars/<?php if(!empty(Sentry::getUser()->avatar)) { echo Sentry::getUser()->avatar; } else { echo 'noavatar.png'; } ?>" alt="avatar"/>

                            <!--span class="profile-picture">
                                <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/img/avatars/{{ Sentry::getUser()->avatar }}" />
                            </span-->

                            <a class="profile-card-button">CHANGE IMAGE</a>
                            <a class="profile-card-button">GET PREMIUM</a>
                        </div>
                        <div class="card-row">
                            <div class="sign-name">
                                <span class="form-description">FIRST NAME: </span>
                                {{ Form::text('first_name', Sentry::getUser()->first_name, array('class' => 'form__input first_name' )) }}
                                <div class="firstname_alert input-caption input-alert"></div>
                            </div>
                            <div class="sign-name">
                                <span class="form-description">LAST NAME: </span>
                                {{ Form::text('last_name', Sentry::getUser()->last_name, array('class' => 'form__input last_name' )) }}
                                <div class="lastname_alert input-caption input-alert"></div>
                            </div>
                            <div>
                                <span class="form-description">LOGIN: </span>
                                {{ Form::text('username', Sentry::getUser()->username, array('class' => 'form__input username' )) }}
                                <div class="username_alert input-caption input-alert"></div>
                            </div>
                            <div>
                                <span class="form-description">E-MAIL: </span>
                                {{ Form::text('email', Sentry::getUser()->email, array('class' => 'form__input email' )) }}
                                <div class="email_alert input-caption input-alert"></div>
                            </div>
                            <div>
                                <span class="form-description">PASSWORD: </span>
                                {{ Form::password('password', array('class' => 'form__input password' )) }}
                                <div class="password_alert input-caption input-alert"></div>
                            </div>
                            <div>
                                <span class="form-description">ABOUT ME: </span>
                                {{ Form::textarea('about', Sentry::getUser()->about, array('class' => 'form__input about' )) }}
                                <div class="about_alert input-caption input-alert"></div>
                            </div>

                        </div>
                    </div>
                    <div class="form-footer">
                        <a class="add-new-button" onclick="infoSubmit()">SAVE</a>
                    </div>
                    {{ Form::close() }}
                </div>
<script>
function infoSubmit() {
    document.getElementById("info").submit();
}
</script>

                <!--a href="{{ URL::to('/music/page', 2) }}" class="view-more-button js-ajax-more">View more music</a-->

            </div>

@stop
