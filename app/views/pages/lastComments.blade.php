<div class="sidebar-comments-title">Last comments</div>
<ul class="sidebar-comments-list">
    @if($lastComments = Comment::lastComments())
        @foreach($lastComments as $comment)
        <li class="sidebar-comments-item">
            <div class="sidebar-comment-author clearfix">
                @if($comment->user_avatar)
                    <img class="sidebar-comment-userpic" src="/img/avatars/{{ $comment->user_avatar }}" alt="">
                @else
                    <img class="sidebar-comment-userpic" src="/img/avatars/noavatar.png" alt="">
                @endif
                <div class="sidebar-comment-username">{{ $comment->user_username }}</div>
                <div class="sidebar-comment-date">3 weeks ago({{ $comment->comment_created_at }})</div>
            </div>
            <div class="sidebar-comment-text">{{ $comment->comment_comment }}</div>
            @if($comment->comment_item_type == 'music')
            <a href="/music/track/{{ $comment->item_slug }}" class="sidebar-comment-subject js-ajax-popup">{{ $comment->item_title }}</a>
            @elseif($comment->comment_item_type == 'videos')
            <a href="/videos/clip/{{ $comment->item_slug }}" class="sidebar-comment-subject js-ajax-popup">{{ $comment->item_title }}</a>
            @elseif($comment->comment_item_type == 'event')
            <a href="/events/{{ $comment->item_slug }}" class="sidebar-comment-subject js-ajax-popup">{{ $comment->item_title }}</a>
            @else
            @endif
        </li>
        @endforeach
    @endif
</ul>