@extends('index')

@section('submenu')
    <div class="head-tag-b">
        <ul class="head-tag container">
            @foreach($musicSubcategories as $cat)
            <li class="head-tag-item"><a class="head-tag-link" href='/music/{{ $cat->slug }}'>{{ $cat->title }}</a></li>
            @endforeach
        </ul>
    </div>
@stop

@section('content')
<div class="column content-full user_playlist">

    <div class="profile-card clearfix">

        <div class="column-title clearfix">

            <div class="column-title-title">

                PLAYLISTS

            </div>

        </div>

        <div class="clearfix">

            <div class="column content-half">

                @if($playlists)
                <ul class="playlist-filter-list">
                    <li class="playlist-filter-item"><a data-playlist="ALL">ALL</a></li>
                    @foreach($playlists as $playlist)
                    <li class="playlist-filter-item"><a class="playlist-filter-link js-playlist-filter" data-playlist="{{ $playlist->name }}">{{ $playlist->name }}</a></li>
                    @endforeach
                </ul>
                @endif

                <div>
                    <div class="input-caption playlist_notice"></div>
                    {{ Form::text('playlist', null, array('class' => 'form__input', 'placeholder' => 'Add Playlist')) }}
                    <div class="playlist_alert input-caption input-alert"></div>
                </div>
            </div>
            <div class="column content-list-container">
              
              <div class="playlist-title clearfix"></div>
              <div class="playlist-content"></div>

            </div>
        </div>
        <div class="ajax-player-content"></div>
        <div class="add-to-playlist"></div>
        <div class="topbar-controls">
            <p href="#" class="topbar-control topbar-control-add"> + </p>
        </div>
    </div>
</div>
@stop
@section('documentEnd')
<script>
$("input").bind("enterKey",function(e){
    var playlistName = playList.val();
    $('.playlist_alert').html('');
    $('.playlist_notice').html('');
    $.ajax({
      type: "POST",
      url: '/addPlaylist',
      data: { name: playlistName },
      success: function(responce) {
      console.log(responce)
        if(responce == true) {
            $('.playlist_notice').html('Playlist added.');
            $('.playlist-filter-list').append("<li class='playlist-filter-item'><a class='playlist-filter-link js-playlist-filter' data-playlist="+
            playlistName+
            ">"+
            playlistName+
            "</a></li>");

        } else {
            $('.playlist_alert').html(responce);

        }
        playList.val('');
      }
    });
});
$("input").keyup(function(e){
    playList = $(this);
    if(e.keyCode == 13) {
      $(this).trigger("enterKey");
    }
});

$('body').on('click', '.playlist-filter-item', function() {

    var name = $(this).find('a').data('playlist');
    $('.playlist-title').html('<a class="music-card-play" href="#"></a>' + '<span>' + name + '</span>' + '<a class="topbar-control topbar-control-edit" href="#"> </a>' + '<a class="topbar-control topbar-control-delete" href="#"> </a>');
    $.ajax({
      type: "POST",
      url: '/getPlaylist',
      data: { name: name },
      success: function(responce) {
          $('.playlist-content').html(responce['view']);
          $('.playlist-image').html(responce['image']);
          $("#playlist-image").removeAttr("src").attr("src", responce['image']);

      }
    });

});


</script>
@stop