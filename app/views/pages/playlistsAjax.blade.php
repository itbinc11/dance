@if(count($musics))
<ul>
    @foreach($musics as $item)
        <li class="content-item clearfix">
        	<span class="content-number">{{ $item->sort }}</span>
        	<div class="content-header">{{ $item->title }}</div>
        	<a class="js-ajax-player" href="/musicPlaylistAjax/{{ $item->slug }}">PLAY</a>
        	<a class="topbar-control topbar-control-delete" href="#"></a>
        </li>
    @endforeach
</ul>
@else
    <li class="content-item">Empty playlist.</li>
@endif