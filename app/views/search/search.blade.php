@extends('index')

@section('content')
            <div class="column content-full">


                <div class="search-card">
                    <form class="search-card-form" action="/search" method="get">
                        <input type="hidden" name="type" value="{{{ Input::get('type', '') }}}" class="search-card-type">
                        <input type="text" name="q" value="{{{ Input::get('q', '') }}}" class="search-card-input">
                    </form>
                    <ul class="search-card-selector">
                        <li class="search-card-item"><a href="/search?q={{ urlencode(Input::get('q')) }}" class="search-card-music"> </a></li>
                        <li class="search-card-item"><a href="/search?type=video&q={{ urlencode(Input::get('q')) }}" class="search-card-video"> </a></li>
                    </ul>
                </div>

                <!-- search result -->

                @if(isset($result))

                    @if(Input::get('type', false) == 'video')

                        @if(!empty($result) and !$result->isEmpty())
                            <div class="video-row clearfix">

                                @foreach($result as $clip)
                                <div class="video-card">
                                    <div class="video-card-preview" style="background-image: url('http://img.youtube.com/vi/{{ $clip->youtube_id }}/hqdefault.jpg')"> </div>
                                    <a href="{{ URL::route('videos/clip', $clip->slug) }}" class="video-card-play js-ajax-popup"> </a>
                                    <div class="video-card-description">
                                        <div class="video-card-title">{{{ $clip->title }}}</div>
                                        <div class="video-card-comment">{{{ $clip->description }}}</div>
                                    </div>
                                    <ul class="video-card-tags">
                                        @foreach($clip->tags()->orderBy('title')->get() as $tag)
                                        <li class="video-card-tag">
                                            <a class="video-card-link" href="/search?type=video&q={{ urlencode('"'.$tag->title.'"') }}">{{ $tag->title }}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endforeach

                            </div>

                            <a href="{{ URL::to('/search/page', 2) . '?type=video&q=' . urlencode(Input::get('q')) }}" class="view-more-button js-ajax-more">View more video</a>
                        @else
                            <div class="column-title clearfix">
                                <div class="column-title-title">Sorry, nothing found<br> Try changing the query and try again</div>
                            </div>
                        @endif
                    @else
                        @if(!empty($result) and !$result->isEmpty())
                            <div class="music-row clearfix">

                                @foreach($result as $track)
                                <div class="music-card">
                                    <img class="music-card-cover" src="{{ $track->artwork_url }}" alt="">
                                    <a href="{{ URL::route('music/track', $track->slug) }}" class="music-card-play js-ajax-popup"> </a>
                                    <div class="music-card-description">
                                        <div class="music-card-title">{{{ $track->title }}}</div>
                                        <div class="music-card-misc clearfix">
                                            <ul class="music-card-stats">
                                                <li class="music-card-listens">{{ $track->humanReadablePlaybackCount() }}</li>
                                                <li class="music-card-likes">{{ $track->humanReadableFavoritingsCount() }}</li>
                                                <li class="music-card-comments">0</li>
                                            </ul>
                                            <ul class="music-card-social">
                                                <li class="music-card-social-item"><a href="{{ $track->url }}" target="_blank" class="music-card-soundcloud"> </a></li>
                                                <li class="music-card-social-item"><a href="{{ $track->user_facebook or '#' }}" target="_blank" class="music-card-facebook"> </a></li>
                                                <li class="music-card-social-item"><a href="{{ $track->user_twitter or '#' }}" target="_blank" class="music-card-twitter"> </a></li>
                                                <li class="music-card-social-item"><a href="{{ $track->user_personal or '#' }}" target="_blank" class="music-card-web"> </a></li>
                                            </ul>
                                        </div>
                                        <ul class="music-card-tags">
                                            @foreach($track->tags()->orderBy('title')->get() as $tag)
                                            <li class="music-card-tag">
                                                <a class="music-card-link" href="/search?q={{ urlencode('"'.$tag->title.'"') }}">{{ $tag->title }}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                @endforeach

                            </div>

                            <a href="{{ URL::to('/search/page', 2) . '?q=' . urlencode(Input::get('q')) }}" class="view-more-button js-ajax-more">View more music</a>
                        @else
                            <div class="column-title clearfix">
                                <div class="column-title-title">Sorry, nothing found<br> Try changing the query and try again</div>
                            </div>
                        @endif
                    @endif

                @else
                    <div class="column-title clearfix">
                        <div class="column-title-title">To find track or clip, type its name<br> To search by tag, type its name in quotes</div>
                    </div>
                @endif

            </div>

@stop

@section('documentEnd')
    <script type="text/javascript">
        $(function() {
            // Актуализируем состояние фильтра поиска
            $('.search-card-type').val() == 'video' ?
                $('.search-card-video').addClass('active'):
                $('.search-card-music').addClass('active');


            // Обработчики-переключатели фильтров поиска
            $('.search-card-music').on('click', function() {
                window.location.href = '/search?q=' + encodeURIComponent($('.search-card-input').val());
                return false;
            });

            $('.search-card-video').on('click', function() {
                window.location.href = '/search?type=video&q=' + encodeURIComponent($('.search-card-input').val());
                return false;
            });
        });
    </script>
@stop