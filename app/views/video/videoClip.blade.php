@extends('index')

@section('head')
<meta property="og:title" content="{{ $singleClip->title }}" />
<meta property="og:image" content="http://img.youtube.com/vi/{{ $singleClip->youtube_id }}/hqdefault.jpg" />
<meta property="og:description" content="More {{ $singleClip->subcategory->title }} stuff you will find on Danceproject.info" />
<meta property="og:url" content="{{ $singleClip->getUrl() }}" />
@stop

@section('submenu')
    <div class="head-tag-b">
        <ul class="head-tag container">
            @foreach($videoSubcategories as $cat)
            <li class="head-tag-item"><a class="head-tag-link" href='/videos/{{ $cat->slug }}'>{{ $cat->title }}</a></li>
            @endforeach
        </ul>
    </div>
@stop

@section('content')
            <div class="column content-full">
                
                <div class="column-title clearfix">
                    <div class="column-title-title">Video</div>
                </div>

                <div class="video-row clearfix">
                    <div class="popup-container" style="margin: 0 16px 16px 0; width: inherit; background: #2996C9 url(/img/publication-bg.png) left top no-repeat;">
                        <div class="popup-head clearfix">
                            <ul class="popup-soc-stats" style="margin: 5px 0 0 133px;">
                                <li class="popup-soc-stat popup-soc-gplus" onclick="popUp=window.open('https://plus.google.com/share?url={{ $singleClip->getUrl() }}', 'Опубликовать ссылку в Google Plus', 'width=800,height=300');popUp.focus();return false">{{ $singleClip->getGPlusShareCount() }}</li>
                                <li class="popup-soc-stat popup-soc-facebook" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{ $singleClip->getUrl() }}', 'Опубликовать ссылку в Facebook', 'width=640,height=436,toolbar=0,status=0'); return false">{{ $singleClip->getFbShareCount() }}</li>

                                <?php
                                    $hashtags =  '#' . $singleClip->subcategory->slug . ' #video #danceproject';
                                    $url = $singleClip->getUrl();
                                    $tweet = implode(' ', array(
                                        mb_substr($singleClip->title, 0, (140 - 2 - 22 - mb_strlen($hashtags))),
                                        $url,
                                        $hashtags
                                    ));
                                ?>
                                <li class="popup-soc-stat popup-soc-sntw" onclick="window.open('http://twitter.com/intent/tweet?text={{ urlencode($tweet) }}', 'Опубликовать ссылку в Twitter', 'width=800,height=300'); return false">{{ $singleClip->getTwShareCount() }}</li>
                                <li class="popup-soc-stat popup-soc-vk" onclick="window.open('http://vk.com/share.php?url={{ $singleClip->getUrl() }}', 'Опубликовать ссылку во Вконтакте', 'width=800,height=300'); return false">{{ $singleClip->getVkShareCount() }}</li>
                            </ul>
                        </div>

                        <div class="popup-video">
                            <div class="popup-video-preview" style="height: auto;">
                                <div class="lazyYT" data-youtube-id="{{ $singleClip->youtube_id }}" data-width="686" data-height="308">loading...</div>
                            </div>
                            <div class="popup-video-info">
                                <div class="popup-video-published">Published {{ $singleClip->created_at->format('d.m.Y') }} by <a class="popup-video-publisher" href="{{ URL::to('/user', $singleClip->author->id) }}">{{ $singleClip->author->username }}</a></div>
                                <div class="popup-video-likes">75</div>
                                <div class="popup-video-title">{{ $singleClip->title }}</div>
                            </div>
                            <div class="popup-video-about">
                                <div class="popup-about-title">about Video</div>
                                <div class="popup-about-description">{{ $singleClip->htmlDescription() }}</div>
                                <ul class="popup-video-tags">
                                    @foreach($singleClip->tags()->orderBy('title')->get() as $tag)
                                    <li class="popup-video-tag"><a href="/search?type=video&q={{ urlencode('"'.$tag->title.'"') }}" class="popup-video-link">{{ $tag->title }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="video-related">
                                <a href="/search?type=video&q={{ urlencode($singleClip->title) }}" class="video-related-button">Related video</a>
                                @foreach ($singleClip->getRelated(2) as $relatedVideo)
                                    <a href="{{ URL::route('videos/clip', $relatedVideo->slug) }}" class="video-related-preview">
                                        <img class="music-related-preview-img" src="http://img.youtube.com/vi/{{ $relatedVideo->youtube_id }}/hqdefault.jpg" alt="">
                                    </a>
                                @endforeach
                            </div>
                           
                        </div>
                    </div>
                    
                </div>

                <div class="video-row clearfix">

                    @foreach($video as $clip)
                    <div class="video-card">
                        <div class="video-card-preview" style="background-image: url('http://img.youtube.com/vi/{{ $clip->youtube_id }}/hqdefault.jpg')"> </div>
                        <a href="{{ URL::route('videos/clip', $clip->slug) }}" class="video-card-play js-ajax-popup"> </a>
                        <div class="video-card-description">
                            <div class="video-card-title">{{{ $clip->title }}}</div>
                            <div class="video-card-comment">{{{ $clip->description }}}</div>
                        </div>
                        <ul class="video-card-tags">
                            @foreach($clip->tags()->orderBy('title')->get() as $tag)
                            <li class="video-card-tag">
                                <a class="video-card-link" href="/search?type=video&q={{ urlencode('"'.$tag->title.'"') }}">{{ $tag->title }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endforeach

                </div>

                <a href="{{ URL::to('/videos/page', 2) }}" class="view-more-button js-ajax-more">View more video</a>

            </div>
@stop

@section('documentEnd')
    <script>
        $(document).ready(function() {
            $('.lazyYT').lazyYT();
        });
    </script>
@stop