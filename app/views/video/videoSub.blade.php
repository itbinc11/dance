@extends('index')

@section('submenu')
    <div class="head-tag-b">
        <ul class="head-tag container">
            @foreach($videoSubcategories as $cat)
            <li class="head-tag-item"><a class="head-tag-link" href='/videos/{{ $cat->slug }}'>{{ $cat->title }}</a></li>
            @endforeach
        </ul>
    </div>
@stop

@section('content')
            <div class="column content-full">
                
                <div class="column-title clearfix">
                    <div class="column-title-title">{{ $sub->title }}</div>
                </div>

                <div class="video-row clearfix">

                    @foreach($video as $clip)
                    <div class="video-card">
                        <div class="video-card-preview" style="background-image: url('http://img.youtube.com/vi/{{ $clip->youtube_id }}/hqdefault.jpg')"> </div>
                        <a href="{{ URL::route('videos/clip', $clip->slug) }}" class="video-card-play js-ajax-popup"> </a>
                        <div class="video-card-description">
                            <div class="video-card-title">{{{ $clip->title }}}</div>
                            <div class="video-card-comment">{{{ $clip->description }}}</div>
                        </div>
                        <ul class="video-card-tags">
                            @foreach($clip->tags()->orderBy('title')->get() as $tag)
                            <li class="video-card-tag">
                                <a class="video-card-link" href="/search?type=video&q={{ urlencode('"'.$tag->title.'"') }}">{{ $tag->title }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endforeach

                </div>

                <a href="{{ URL::to('/videos', array($sub->slug, 2)) }}" class="view-more-button js-ajax-more">View more video</a>

            </div>
@stop