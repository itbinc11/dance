<div class="popup-container popup-music-wrap">
    <div class="popup-head clearfix">
        <a href="/" class="popup-logo">Dance project</a>
        <ul class="popup-soc-stats">
        <li class="popup-soc-stat popup-soc-gplus" onclick="popUp=window.open('https://plus.google.com/share?url={{ $clip->getUrl() }}', 'Опубликовать ссылку в Google Plus', 'width=800,height=300');popUp.focus();return false">{{ $clip->getGPlusShareCount() }}</li>
        <li class="popup-soc-stat popup-soc-facebook" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{ $clip->getUrl() }}', 'Опубликовать ссылку в Facebook', 'width=640,height=436,toolbar=0,status=0'); return false">{{ $clip->getFbShareCount() }}</li>
            <?php
                $hashtags =  '#' . $clip->subcategory->slug . ' #video #danceproject';
                $url = $clip->getUrl();
                $tweet = implode(' ', array(
                    mb_substr($clip->title, 0, (140 - 2 - 22 - mb_strlen($hashtags))),
                    $url,
                    $hashtags
                ));
            ?>
            <li class="popup-soc-stat popup-soc-sntw" onclick="window.open('http://twitter.com/intent/tweet?text={{ urlencode($tweet) }}', 'Опубликовать ссылку в Twitter', 'width=800,height=300'); return false">{{ $clip->getTwShareCount() }}</li>
            <li class="popup-soc-stat popup-soc-vk" onclick="window.open('http://vk.com/share.php?url={{ $clip->getUrl() }}', 'Опубликовать ссылку во Вконтакте', 'width=800,height=300'); return false">{{ $clip->getVkShareCount() }}</li>
        </ul>
        <a href="#" class="popup-close arcticmodal-close"> </a>
    </div>

    <div class="popup-video">
        <div class="popup-video-preview">
            <div class="lazyYT" data-youtube-id="{{ $clip->youtube_id }}" data-width="784" data-height="352">loading...</div>
            <a href="#" class="popup-video-prev js-video-change"> </a>
            <a href="#" class="popup-video-next js-video-change"> </a>
        </div>
        <div class="popup-video-info">
            <div class="popup-video-published">Published {{ $clip->created_at->format('d.m.Y') }} by <a class="popup-video-publisher" href="{{ URL::to('/user', $clip->author->id) }}">{{ $clip->author->username }}</a></div>
            <!--div class="popup-video-likes">75</div-->
            <div class="{{ $like }}"  data-id="{{ $clip->id }}">75</div>
            <div class="popup-video-title">{{ $clip->title }}</div>
        </div>
        <div class="popup-video-about">
            <div class="popup-about-title">about Video</div>
            <div class="popup-about-description">{{ $clip->htmlDescription() }}</div>
            <ul class="popup-video-tags">
                @foreach($clip->tags()->orderBy('title')->get() as $tag)
                <li class="popup-video-tag"><a href="/search?type=video&q={{ urlencode('"'.$tag->title.'"') }}" class="popup-video-link">{{ $tag->title }}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="video-related">
            <a href="/search?type=video&q={{ urlencode($clip->title) }}" class="video-related-button">Related video</a>
            @foreach ($relatedVideos as $relatedVideo)
                <a href="{{ URL::route('videos/clip', $relatedVideo->slug) }}" class="video-related-preview">
                    <img class="music-related-preview-img" src="http://img.youtube.com/vi/{{ $relatedVideo->youtube_id }}/hqdefault.jpg" alt="">
                </a>
            @endforeach
        </div>        
        </div>       
    </div>
    <div class="form-music-wrap">
        @if(Sentry::check())
        <div class="form-music">
            <strong class="com-qty">2 comments</strong>
            {{ Form::open(array('route' => 'addComment')) }}
            <div class="redactor-wrapper">
                <textarea class="content-form-field content-form-field-textarea content-form-field-comment js-redactor" name="comment"></textarea>
                {{ Form::submit('SEND', array('name'=>'submit', 'class' => 'send-comment music-related-button')) }}
            </div>            
            <div class="column captcha">
                <span class="form-description" >CAPTCHA </span>
                {{ HTML::image(Captcha::img(), 'Captcha image', array('id' => 'captcha')) }}
            </div>
            <div class="column">
                <span class="form-description" >WHAT CODE IS IN IMAGE? </span>
                {{ Form::text('captcha', null, array('class' => 'form__input captcha_register')) }}
                <div class="captcha_alert input-caption input-alert"></div>
                <span class="input-caption" >Enter the characters(without spaces) shown in the image.</span>
            </div>
            {{ Form::text('item_id', $clip->id, array('hidden'=>'hidden')) }}
            {{ Form::text('item_type', 'videos', array('hidden'=>'hidden')) }}
            {{ Form::close() }}
        </div>
        @endif
        <div class="content-comments">
        @if($comments)        
        <ul class="comments-list">
            @foreach($comments as $item)
            <li class="clearfix">
                @if($item->user_avatar)
                    <img class="content-form-image" height="40" width="auto" alt="avatar" src="/img/avatars/{{ $item->user_avatar }}">
                @else
                    <img class="content-form-image" height="40" width="auto" alt="avatar" src="/img/avatars/noavatar.png">
                @endif
                <div class="content-wrap">
                    <div class="content-header-title">{{ $item->user_username }}</div>
                    <div class="content-header-time">{{ $item->comment_created_at }}</div>
                    <div class="clearfix"></div>
                    <div class="content-header-comment">{{ $item->comment_comment }}</div>
                </div>     
            </li>
            @endforeach
        </ul>
        @else
        No comments.
        @endif
        </div>
    </div>
</div>

<script>
    $('.lazyYT').lazyYT();
</script>