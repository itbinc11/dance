@extends('index')

@section('head')
<meta property="og:title" content="{{ $metaTags->title }}" />
<meta property="og:image" content="{{ asset('/img/oglogo.jpg') }}" />
<meta property="og:description" content="{{ $metaTags->description }}" />
<meta property="og:url" content="{{ url('/'); }}" />
@stop

@section('content')
            <div class="column content-half">
                <div class="column-title clearfix">
                    <a href="#" class="column-title-prev">prev</a>
                    <a href="#" class="column-title-next">next</a>
                    <div class="column-title-title">Events</div>
                </div>

                <div class="events-row clearfix">
                    <div class="events-frame">
                        <ul class="events-list">
                            @foreach($events as $event)
                            <li class="column event-column">
                                <div class="event-card">
                                <a href="{{ URL::route('events/event', $event->slug) }}" class="imglink"><img src="{{ $event->poster }}" alt="" class="event-card-poster"></a>
                                <div class="event-card-title">{{ $event->title }}</div>
                                <div class="event-card-info">{{ \Carbon\Carbon::parse($event->date)->format('G:i, j F Y') }}</div>
                                <div class="event-card-info">{{ $event->place }}</div>
                                <div class="event-card-info">{{ $event->guest }}</div>
                                <a href="{{ URL::route('events/event', $event->slug) }}" class="event-card-button js-ajax-popup">Learn more</a>
                            </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="column-title clearfix">
                    <div class="column-title-title">Latest music</div>
                </div>

                <div class="card-row clearfix">
                    
                    @foreach($music as $track)
                    <div class="music-card">
                        <img class="music-card-cover" src="{{ $track->artwork_url }}" alt="">
                        <a href="{{ URL::route('music/track', $track->slug) }}" class="music-card-play js-ajax-popup"> </a>
                        <div class="music-card-description">
                            <div class="music-card-title">{{{ $track->title }}}</div>
                            <div class="music-card-misc clearfix">
                                <ul class="music-card-stats">
                                    <li class="music-card-listens">{{ $track->humanReadablePlaybackCount() }}</li>
                                    <li class="music-card-likes">{{ $track->humanReadableFavoritingsCount() }}</li>
                                    <li class="music-card-comments">0</li>
                                </ul>
                                <ul class="music-card-social">
                                    <li class="music-card-social-item"><a href="{{ $track->url }}" target="_blank" class="music-card-soundcloud"> </a></li>
                                    <li class="music-card-social-item"><a href="{{ $track->user_facebook or '#' }}" target="_blank" class="music-card-facebook"> </a></li>
                                    <li class="music-card-social-item"><a href="{{ $track->user_twitter or '#' }}" target="_blank" class="music-card-twitter"> </a></li>
                                    <li class="music-card-social-item"><a href="{{ $track->user_personal or '#' }}" target="_blank" class="music-card-web"> </a></li>
                                </ul>
                            </div>
                            <ul class="music-card-tags">
                                @foreach($track->tags()->orderBy('title')->get() as $tag)
                                <li class="music-card-tag">
                                    <a class="music-card-link" href="/search?q={{ urlencode('"'.$tag->title.'"') }}">{{ $tag->title }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endforeach

                    <a href="{{ URL::to('/music/page', 2) }}" class="view-more-button js-ajax-more">View more music</a>

                </div>

            </div>
            
            <div class="column content-half">
                <div class="column-title">
                    <div class="column-title-title">TOP 10 of the week</div>
                </div>

                <div class="card-row clearfix">
                    <ul class="top-ten-card">
                        <?php $i = 1; ?>
                        @foreach($top10 as $track)
                        <li class="top-ten-item">
                            <img class="top-ten-artwork" src="{{ $track->artwork_url }}" alt="">
                            <div class="top-ten-description">
                                <div class="top-ten-title">{{ $track->title }}</div>
                            </div>
                            <div class="top-ten-duration">{{ $track->humanReadableDuration() }}</div>
                            <a href="{{ URL::route('music/track', $track->slug) }}" class="top-ten-play js-ajax-popup"> </a>
                            <div class="top-ten-position">{{ sprintf('%02s', $i++) }}</div>
                        </li>
                        @endforeach
                    </ul> 
                    
                </div>

                <div class="column-title clearfix">
                    <div class="column-title-title">Latest video</div>
                </div>

                <div class="card-row clearfix">
                    @foreach($video as $clip)
                    <div class="video-card">
                        <div class="video-card-preview" style="background-image: url('http://img.youtube.com/vi/{{ $clip->youtube_id }}/hqdefault.jpg')"> </div>
                        <a href="{{ URL::route('videos/clip', $clip->slug) }}" class="video-card-play js-ajax-popup"> </a>
                        <div class="video-card-description">
                            <div class="video-card-title">{{{ $clip->title }}}</div>
                            <div class="video-card-comment">{{{ $clip->description }}}</div>
                        </div>
                        <ul class="video-card-tags">
                            @foreach($clip->tags()->orderBy('title')->get() as $tag)
                            <li class="video-card-tag">
                                <a class="video-card-link" href="/search?type=video&q={{ urlencode('"'.$tag->title.'"') }}">{{ $tag->title }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endforeach

                    <a href="{{ URL::to('/videos/page', 2) }}" class="view-more-button js-ajax-more">View more video</a>

                </div>

            </div>
@stop

@section('documentEnd')
<script type="text/javascript" src="/js/jcarousellite.js"></script>
<script type="text/javascript">
    $(function(){
    $(".events-frame").jCarouselLite({
            btnNext: ".column-title-next",
            btnPrev: ".column-title-prev",
            circular: false,
            visible: 2,
        });
    });
</script>
@stop