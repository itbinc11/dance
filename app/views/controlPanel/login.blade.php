<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/cp-css/style.css">
    <title>Dance Project Control panel</title>
</head>
<body>
    <div class="container">
    {{ Form::open(array( 'route' => 'controlPanel.login' )) }}
        <div>
            <span class="form-description" style="display: inline-block; width: 85px;">Username: </span>
            {{ Form::text('username', null, array('class' => 'form__input')) }}
        </div>

        <div>
            <span class="form-description" style="display: inline-block; width: 85px;">Password: </span>
            {{ Form::password('password', array('class' => 'form__input')) }}
        </div>

        <div>
            <button style="margin: 4px 0 0 89px; font-size: 14px; padding: 1px 10px;">Login</button>
        </div>
    {{ Form::close() }}
    </div>
</body>
</html>