@if(count($result))
    @foreach($result as $item)
        <li class="content-item">
            <div class="content-header">
                <div class="content-header-edit"><a class="content-header-edit-control js-content-edit" href="#"> </a></div>
                <div class="content-header-id">{{ $item->id }}</div>
        @if ($item->type == 'music' or is_a($item, 'Music'))
                <div class="content-header-music"> </div>
                <div class="content-header-title">{{{ $item->title }}}</div>
                <div class="content-header-category">{{ $categories[$item->subcategory_id-1]->title }}</div>
                <select class="content-header-category-select js-category-select" name="subcategory_id">
                    @foreach($musicSubcategories as $category)
                    <option value="{{ $category->id }}" @if($category->id == $categories[$item->subcategory_id-1]->id) selected="selected" @endif>{{ $category->title }}</option>
                    @endforeach
                </select>
                <div class="content-header-check"> </div>
            </div>
            <form class="content-form clearfix" enctype="multipart/form-data">
                <div class="content-form-pic">
                    <img class="content-form-image" src="{{ $item->artwork_url }}">
                </div>
                <div class="content-form-fields">
                    <input class="content-form-field content-form-field-title" type="text" name="title" value="{{{ $item->title }}}">
                    <div class="textarea-wrapper js-scroll">
                        <textarea class="content-form-field content-form-field-textarea js-elastic" name="description">{{{ $item->description }}}</textarea>
                    </div>
                    <input class="content-form-field content-form-field-tags" type="text" name="tags" value="{{ implode(',', $item->tags()->orderBy('title')->lists('title')) }}">
                    <input class="content-form-type" type="hidden" name="type" value="music">
                    <input class="hidden js-img-input" type="file" name="img">
                </div>

        @elseif ($item->type == 'video' or is_a($item, 'Video'))
                <div class="content-header-video"> </div>
                <div class="content-header-title">{{ $item->title }}</div>
                <div class="content-header-category">{{ $categories[$item->subcategory_id-1]->title }}</div>
                <select class="content-header-category-select js-category-select" name="category">
                    @foreach($videoSubcategories as $category)
                    <option value="{{ $category->id }}" @if($category->id == $categories[$item->subcategory_id-1]->id) selected="selected" @endif>{{ $category->title }}</option>
                    @endforeach
                </select>
                <div class="content-header-check"> </div>
            </div>
            <form class="content-form clearfix" enctype="multipart/form-data">
                <div class="content-form-pic">
                    <img class="content-form-image" src="http://img.youtube.com/vi/{{ $item->youtube_id }}/hqdefault.jpg">
                </div>
                <div class="content-form-fields">
                    <input class="content-form-field content-form-field-title" type="text" name="title" value="{{{ $item->title }}}">
                    <div class="textarea-wrapper js-scroll">
                        <textarea class="content-form-field content-form-field-textarea js-elastic" name="description">{{{ $item->description }}}</textarea>
                    </div>
                    <input class="content-form-field content-form-field-tags" type="text" name="tags" value="{{ implode(',', $item->tags()->orderBy('title')->lists('title')) }}">
                    <input class="content-form-type" type="hidden" name="type" value="video">
                    <input class="hidden js-img-input" type="file" name="img">
                </div>

        @elseif ($item->type == 'event' or is_a($item, 'MusicEvent'))
                <div class="content-header-event"> </div>
                <div class="content-header-title">{{ $item->title }}</div>
                <div class="content-header-category"></div>
                <div class="content-header-check"> </div>
            </div>
            <form class="content-form clearfix" enctype="multipart/form-data">
                <div class="content-form-pic">
                    <img class="content-form-image" src="{{ $item->poster }}">
                    <a class="content-form-choose-image js-img-input-trigger" href="#">Choose image</a>
                </div>
                <div class="content-form-fields">
                    <input class="content-form-field content-form-field-title" type="text" name="title" value="{{ $item->title }}">
                    <div class="redactor-wrapper">
                        <textarea class="content-form-field content-form-field-textarea js-redactor" name="description">{{ $item->description }}</textarea>
                    </div>
                    <input class="content-form-field content-form-field-text content-form-field-url" type="text" name="url" value="{{ $item->url }}">
                    <input class="content-form-field content-form-field-text content-form-field-place" type="text" name="place" value="{{ $item->place }}">
                    <input class="content-form-field content-form-field-text content-form-field-city" type="text" name="city" value="{{ $item->city }}">
                    <input class="content-form-field content-form-field-text content-form-field-country" type="text" name="country" value="{{ $item->country }}">
                    <input class="content-form-field content-form-field-text content-form-field-date js-date-picker" type="datetime" name="date" value="{{ Carbon\Carbon::parse($item->date) }}">
                    <input class="content-form-field content-form-field-text content-form-field-guest" type="text" name="guest" value="{{ $item->guest }}">
                    <input class="content-form-type" type="hidden" name="type" value="event">
                    <input class="hidden js-img-input" type="file" name="img">
                </div>
        @endif
                <div class="content-form-controls">
                    <a href="#" class="content-form-control js-form-cancel">Cancel</a>
                    <a href="#" class="content-form-control js-form-save">Save</a>
                </div>
            </form>
        </li>
    @endforeach
@else
    <li class="content-item">Not found</li>
@endif