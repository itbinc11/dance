<div style="display: none;">
    <div id="sign_popup" class="popup-container">
        <div class="popup-head clearfix">
            <a href="/" class="popup-logo">Dance project</a>
            <a href="#" class="popup-close arcticmodal-close"> </a>
        </div>

        <div class="popup-music">
            <h5>SIGN UP</h5>
            <div class="success_register"></div>
            <div class="music-related">

                {{ Form::open(array( 'route' => 'controlPanel.register', 'class' => 'clearfix' )) }}
                        <div class="sign-name">
                            <span class="form-description" >FIRST NAME </span>
                            {{ Form::text('first_name', null, array('class' => 'form__input first_name_register')) }}
                            <div class="firstname_alert input-caption input-alert"></div>
                        </div>
                        <div class="sign-name">
                            <span class="form-description" >LAST NAME </span>
                            {{ Form::text('last_name', null, array('class' => 'form__input last_name_register')) }}
                            <div class="lastname_alert input-caption input-alert"></div>
                        </div>
                        <div>
                            <span class="form-description" >LOGIN </span>
                            {{ Form::text('username', null, array('class' => 'form__input username_register')) }}
                            <div class="username_alert input-caption input-alert"></div>
                        </div>
                        <div>
                            <span class="form-description" >E-MAIL </span>
                            {{ Form::text('email', null, array('class' => 'form__input email_register')) }}
                            <div class="email_alert input-caption input-alert"></div>
                        </div>
                        <div>
                            <span class="form-description" >PASSWORD </span>
                            {{ Form::password('password', array('class' => 'form__input password_register')) }}
                            <div class="password_alert input-caption input-alert"></div>
                        </div>
                        <div>
                            <span class="form-description" >CONFIRM PASSWORD </span>
                            {{ Form::password('confirm_password', array('class' => 'form__input confirm_password_register')) }}
                        </div>
                        <div>
                            <span class="form-description" >CAPTCHA </span>
                            {{ HTML::image(Captcha::img(), 'Captcha image', array('id' => 'myimg')) }}
                        </div>

                        <div>
                            <span class="form-description" >WHAT CODE IS IN IMAGE? </span>
                            {{ Form::text('captcha', null, array('class' => 'form__input captcha_register')) }}
                            <div class="captcha_alert input-caption input-alert"></div>
                            <span class="input-caption" >Enter the characters(without spaces) shown in the image.</span>
                        </div>
                {{ Form::close() }}
                        <div class="form-footer">
                            <button class="sign-btn" type="button" onclick="submit_register()">SING UP</button>
                        </div>
            </div>
        </div>
    </div>

    <div id="register_complete" class="popup-container">
     <div class="popup-head clearfix">
                <a href="/" class="popup-logo">Dance project</a>
                <a href="#" class="popup-close arcticmodal-close"> </a>
            </div>

            <div class="popup-music">
                Thank you for registration.
                Now you can <a href="#" onclick="open_login()">LOG IN</a>.
            </div>


    </div>
</div>
<script>
$('#head-sign-in').click(function() {
    $('#sign_popup').arcticmodal();
});
function open_login() {
    $.arcticmodal('close');
    $('#login_popup').arcticmodal();
}
function submit_register() {
$('.alert').html('');
$('.form__input').removeClass('input_alert');
    var data = {
        first_name: $('.first_name_register').val(),
        last_name: $('.last_name_register').val(),
        username: $('.username_register').val(),
        email: $('.email_register').val(),
        password: $('.password_register').val(),
        confirm_password: $('.confirm_password_register').val(),
        captcha: $('input[name="captcha"]').val()
    };
    $.ajax({
      type: "POST",
      url: '/register',
      data: data,
      success: function(responce) {
        if(responce == 'OK') {
            $('.success_register').html('Thx for registration.');
            $.arcticmodal('close');
            $('#register_complete').arcticmodal();
        } else {
            var arr = $.parseJSON(responce);
            if(arr['errors']) {
                $.each(arr['errors'], function(key, value) {
                    $('.'+key+'_alert').html(value);
                    $('.'+key+'_register').addClass('input_alert');

                });
                $("#myimg").removeAttr("src").attr("src", arr['errors']['captcha_img']);
            }
        }
      }
    });
}
</script>