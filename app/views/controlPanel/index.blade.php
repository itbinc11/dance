<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/cp-css/style.css">
    <link type="text/css" rel="stylesheet" href="/js/jscrollpane/jquery.jscrollpane.css"/>
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

    <script type="text/javascript" src="/js/humane.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/js/humane/flatty.css">

    <script type="text/javascript" src="/js/jquery.tagsinput.js"></script>
    <link rel="stylesheet" type="text/css" href="/js/tagsinput/jquery.tagsinput.css">

    <script type="text/javascript" src="/js/jquery.easydropdown.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/js/easydropdown/easydropdown.css">

    <script type="text/javascript" src="/js/jquery.simple-dtpicker.js"></script>
    <link type="text/css" href="/js/simple-dtpicker/jquery.simple-dtpicker.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="/js/arcticModal/jquery.arcticmodal-0.3.css">

    <script type="text/javascript" src="/js/redactor.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/js/redactor/redactor.css">


    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/js/arcticModal/jquery.arcticmodal-0.3.css">
    <link rel="stylesheet" type="text/css" href="/js/nouislider/jquery.nouislider.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <title>Dance Project Control panel</title>
</head>
<body>
    <div class="container-admin">
        <div class="column column-1">
            <a href="/adminka" class="logo-link"><img class="logo" src="/cp-img/logo.png"></a>
            <a href="#" class="publish-filter js-published-filter" data-published="1"> </a>
            <a href="#" class="unpublish-filter js-published-filter" data-published="0"> </a>
            <a href="/adminka/metatags" class="metatags-nav"> </a>
            <a href="/adminka/users" class="type-filter-user"> </a>
            <a href="{{ URL::route('controlPanelLogout') }}" class="close-button">Close</a>
        </div>
        <div class="column column-2">
            <ul class="type-filter-list">
                <li class="type-filter-item"><a href="#" class="type-filter-link type-filter-all js-type-filter" data-type="all"> </a></li>
                <li class="type-filter-item"><a href="#" class="type-filter-link type-filter-music js-type-filter" data-type="music"> </a></li>
                <li class="type-filter-item"><a href="#" class="type-filter-link type-filter-videos js-type-filter" data-type="video"> </a></li>
                <li class="type-filter-item"><a href="#" class="type-filter-link type-filter-events js-type-filter" data-type="events"> </a></li>
                <li class="type-filter-item"><a href="#" class="type-filter-link type-filter-comments js-type-filter" data-type="comments"> </a></li>
            </ul>
        </div>
        <div class="column column-3">
            <div class="topbar">
                <input class="topbar-search-input">
                <a href="#" class="topbar-search-button">Search</a>

                <div class="topbar-controls">
                    <ul class="topbar-control-list">
                        <li class="topbar-control-item"><a href="#" class="topbar-control topbar-control-publish"> </a></li>
                        <li class="topbar-control-item"><a href="#" class="topbar-control topbar-control-unpublish"> </a></li>
                        <li class="topbar-control-item"><a href="#" class="topbar-control topbar-control-delete"> </a></li>
                        <li class="topbar-control-item"><a href="#" class="topbar-control topbar-control-edit"> </a></li>
                        <li class="topbar-control-item"><a href="#" class="topbar-control topbar-control-add"> </a></li>
                    </ul>

                    <div class="topbar-check-all">All</div>
                </div>
            </div>
            <div class="content-box">
                <div class="content">
                    <ul class="tag-filter-list">
                        <li class="tag-filter-item"><a href="#" class="tag-filter-link js-tag-filter" data-tag="all">All</a></li>
                        @foreach($categories as $category)
                        <li class="tag-filter-item"><a href="#" class="tag-filter-link js-tag-filter" data-tag="{{ $category->slug }}">{{ $category->title }}</a></li>
                        @endforeach
                        <li class="tag-filter-item comments-item hidden"><a href="#" class="tag-filter-link js-tag-filter" data-tag="videos">Video</a></li>
                        <li class="tag-filter-item comments-item hidden"><a href="#" class="tag-filter-link js-tag-filter" data-tag="music">Track</a></li>
                        <li class="tag-filter-item comments-item hidden"><a href="#" class="tag-filter-link js-tag-filter" data-tag="event">Event</a></li>
                    </ul>

                    <div class="content-list-container">
                        <ul class="content-list js-scroll">
                        </ul>
                        <div class="content-pagination">
                            <div class="prev-page js-prev-page"> </div>
                            <input class="current-page js-current-page" type="text" value="1">
                            <div class="total-pages">of <span class="js-total-pages">538</span></div>
                            <div class="next-page js-next-page"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="display: none">
        <div class="add-content">
            <dl class="tabs">
                <dt class="active">Track</dt>
                <dd class="active">
                        <form class="add-content-form clearfix">
                            <input class="content-form-field content-form-field-text" type="text" name="url">
                            <div class="content-form-controls">
                                    <a href="#" class="content-form-control js-content-form-cancel">Cancel</a>
                                    <a href="#" class="content-form-control js-content-form-add-track">Add</a>
                                </div>
                        </form>
                </dd>

                <dt>Video</dt>
                <dd>
                        <form class="add-content-form clearfix">
                            <input class="content-form-field content-form-field-text" type="text" name="url">
                            <div class="content-form-controls">
                                    <a href="#" class="content-form-control js-content-form-cancel">Cancel</a>
                                    <a href="#" class="content-form-control js-content-form-add-video">Add</a>
                                </div>
                        </form>
                </dd>

                <dt>Event</dt>
                <dd>

                    <form class="add-content-form content-item active" enctype="multipart/form-data" action="/adminka/create/event" method="post">
                        <div class="content-form-pic">
                            <img class="content-form-image" src="">
                            <a class="content-form-choose-image js-img-input-trigger" href="#">Choose image</a>
                        </div>
                        <div class="content-form-fields">
                            <input class="content-form-field content-form-field-title" type="text" name="title">
                            <textarea class="content-form-field content-form-field-textarea-c js-redactor-popup" name="description"></textarea>
                            <input class="content-form-field content-form-field-text content-form-field-url" type="text" name="url">
                            <input class="content-form-field content-form-field-text content-form-field-place" type="text" name="place">
                            <input class="content-form-field content-form-field-text content-form-field-city" type="text" name="city">
                            <input class="content-form-field content-form-field-text content-form-field-country" type="text" name="country">
                            <input class="content-form-field content-form-field-text content-form-field-date js-popup-date-picker" type="text" name="date" value="{{ date(DateTime::RFC3339) }}">
                            <input class="content-form-field content-form-field-text js-popup-date-picker" type="text" name="pdate" value="{{ date(DateTime::RFC3339) }}">
                            <input class="js-hidden-pdate" type="hidden" name="hidden-pdate" value="{{ date(DateTime::RFC3339) }}">
                            <input class="content-form-field content-form-field-text content-form-field-guest" type="text" name="guest">
                            <label><input style="height: 15px; width: 15px; vertical-align: middle;" type="checkbox" name="crosspublish"> — cross-publish</label>
                            <input class="hidden js-img-input" type="file"  accept="image/*" name="img">
                        </div>
                        <div class="content-form-controls">
                            <a href="#" class="content-form-control js-content-form-cancel">Cancel</a>
                            <a href="#" class="content-form-control js-content-event-save">Save</a>
                        </div>
                    </form>

                </dd>
            </dl>
        </div>

    </div>

    <style type="text/css">
        .redactor_toolbar::after {
            content: none;
        }

        /* Быстрофикс. Всплывающее окно перекрывало попапы redactor'а. */
        #redactor_modal_overlay, #redactor_modal, .redactor_dropdown {
            z-index: 1001 !important;
        }


    </style>

    <script id="add-track" type="text/x-jsrender">
        <form class="add-content-form content-item active">
            <div class="content-form-pic">
                <img class="content-form-image" src="@{{:artwork_url}}">
            </div>
            <div class="content-form-fields">
                <select class="js-category-select" name="subcategory_id">
                    @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                    @endforeach
                </select>
                <input class="content-form-field content-form-field-title" type="text" name="title" value="@{{:title}}">
                <textarea class="content-form-field content-form-field-textarea content-form-field-textarea-c" name="description">@{{:description}}</textarea>
                <input class="content-form-field content-form-field-tags" type="text" name="tags">
                <input class="content-form-field content-form-field-text js-popup-date-picker" type="text" name="pdate" value="{{ date(DateTime::RFC3339) }}">
                <input class="js-hidden-pdate" type="hidden" name="hidden-pdate" value="{{ date(DateTime::RFC3339) }}">
                <label><input style="height: 15px; width: 15px; vertical-align: middle;" type="checkbox" name="crosspublish"> — cross-publish</label>
                <input class="hidden js-img-input" type="text" name="img" value="@{{:artwork_url}}">
            </div>
            <div class="content-form-controls">
                <a href="#" class="content-form-control js-content-form-cancel">Cancel</a>
                <a href="#" class="content-form-control js-content-track-save">Save</a>
            </div>
        </form>
    </script>

    <script id="add-video" type="text/x-jsrender">
        <form class="add-content-form content-item active">
            <div class="content-form-pic">
                <img class="content-form-image" src="@{{:thumbnail.sqDefault}}">
            </div>
            <div class="content-form-fields">
                <select class="js-category-select" name="subcategory_id">
                    @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                    @endforeach
                </select>
                <input class="content-form-field content-form-field-title" type="text" name="title" value="@{{:title}}">
                <textarea class="content-form-field content-form-field-textarea content-form-field-textarea-c" name="description">@{{:description}}</textarea>
                <input class="content-form-field content-form-field-tags" type="text" name="tags">
                <input class="content-form-field content-form-field-text js-popup-date-picker" type="text" name="pdate" value="{{ date(DateTime::RFC3339) }}">
                <input class="js-hidden-pdate" type="hidden" name="hidden-pdate" value="{{ date(DateTime::RFC3339) }}">
                <label><input style="height: 15px; width: 15px; vertical-align: middle;" type="checkbox" name="crosspublish"> — cross-publish</label>
                <input class="hidden js-img-input" type="text" name="img" value="@{{:thumbnail.sqDefault}}">
            </div>
            <div class="content-form-controls">
                <a href="#" class="content-form-control js-content-form-cancel">Cancel</a>
                <a href="#" class="content-form-control js-content-video-save">Save</a>
            </div>
        </form>
    </script>

    <script type="text/javascript" src="/js/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="/js/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript" src="/js/jquery.elastic.source.js"></script>
    <script type="text/javascript" src="/js/arcticModal/jquery.arcticmodal-0.3.min.js"></script>
    <script type="text/javascript" src="/js/jsrender.min.js"></script>
    <script type="text/javascript" src="/js/cp-main.js"></script>

</body>
</html>