{{--["comment_id"]=>--}}
{{--["comment_user_id"]=>--}}
{{--["comment_item_id"]=>--}}
{{--["comment_item_type"]=>--}}
{{--["comment_comment"]=>--}}
{{--["comment_created_at"]=>--}}
{{--["comment_updated_at"]=>--}}
{{--["user_id"]=>--}}
{{--["user_email"]=>--}}
{{--["user_first_name"]=>--}}
{{--["user_last_name"]=>--}}
{{--["user_username"]=>--}}
{{--["user_avatar"]=>--}}
{{--["user_about"]=>--}}
{{--["user_created_at"]=>--}}
{{--["user_updated_at"]=>--}}
{{--["item_title"]=>--}}
{{--["item_slug"]=>--}}
{{--["item_url"]=>--}}
{{--["item_description"]=>--}}
{{--["item_user_id"]=>--}}
{{--["item_published"]=>--}}
{{--["item_created_at"]=>--}}
{{--["item_updated_at"]=>--}}

@if(count($result))
    @foreach($result as $item)
        <li class="content-item comm">
            <div class="content-header clearfix">
                <div class="content-header-edit"><a class="content-header-edit-control js-content-edit" href="#"> </a></div>
                <div class="content-header-id">{{ $item->comment_id }}</div>
                    @if($item->user_avatar)
                        <img class="content-form-image" height="40" width="auto" alt="avatar" src="/img/avatars/{{ $item->user_avatar }}">
                    @else
                        <img class="content-form-image" height="40" width="auto" alt="avatar" src="/img/avatars/noavatar.png">
                    @endif
                <div class="content-header-title">
                    {{ $item->user_username }}
                    <span>{{ $item->comment_created_at }}</span>
                </div>
                <div class="content-header-comment">
                    {{ $item->comment_comment }}
                </div>                
                <div class="content-header-item">
                    @if($item->comment_item_type == 'music')
                    <div class="content-header-music"> </div>
                    @elseif($item->comment_item_type == 'videos')
                    <div class="content-header-video"> </div>
                    @elseif($item->comment_item_type == 'event')
                    <div class="content-header-event"> </div>
                    @endif
                    <span>{{ $item->item_title }}</span>                    
                </div>
                <div class="content-header-check"> </div>
            </div>
            <div class="clearfix"></div>
            <form class="content-form clearfix" enctype="multipart/form-data">
                <div>Some info will be here soon.</div>
                <input class="content-form-type" type="hidden" name="type" value="comment">
            </form>
        </li>
    @endforeach
@else
    <li class="content-item">Not found</li>
@endif