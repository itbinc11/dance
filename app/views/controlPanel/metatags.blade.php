<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/cp-css/style.css">
    <link type="text/css" rel="stylesheet" href="/js/jscrollpane/jquery.jscrollpane.css"/>
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

    <script type="text/javascript" src="/js/humane.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/js/humane/flatty.css">

    <script type="text/javascript" src="/js/jquery.tagsinput.js"></script>
    <link rel="stylesheet" type="text/css" href="/js/tagsinput/jquery.tagsinput.css">

    <script type="text/javascript" src="/js/jquery.easydropdown.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/js/easydropdown/easydropdown.css">

    <script type="text/javascript" src="/js/jquery.simple-dtpicker.js"></script>
    <link type="text/css" href="/js/simple-dtpicker/jquery.simple-dtpicker.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="/js/arcticModal/jquery.arcticmodal-0.3.css">

    <script type="text/javascript" src="/js/redactor.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/js/redactor/redactor.css">

    <title>Dance Project Control panel: Meta tags</title>
</head>
<body>
    <div class="container">
        <div class="column column-1">
            <a href="/adminka" class="logo-link"><img class="logo" src="/cp-img/logo.png"></a>
            <a href="/adminka" class="publish-filter" data-published="1"> </a>
            <a href="/adminka" class="unpublish-filter" data-published="0"> </a>
            <a href="#" class="metatags-nav" onclick="return false;"> </a>
            <a href="/adminka/users" class="type-filter-user"> </a>
            <a href="{{ URL::route('controlPanelLogout') }}" class="close-button">Close</a>
        </div>
        <div class="column column-3">
            <div class="topbar">
                <input class="topbar-search-input">
                <a href="#" class="topbar-search-button">Search</a>

                <div class="topbar-controls">
                    <ul class="topbar-control-list">
                        <li class="topbar-control-item"><a href="#" class="topbar-control topbar-control-publish"> </a></li>
                        <li class="topbar-control-item"><a href="#" class="topbar-control topbar-control-unpublish"> </a></li>
                        <li class="topbar-control-item"><a href="#" class="topbar-control topbar-control-delete"> </a></li>
                        <li class="topbar-control-item"><a href="#" class="topbar-control topbar-control-edit"> </a></li>
                        <li class="topbar-control-item"><a href="#" class="topbar-control topbar-control-add"> </a></li>
                    </ul>

                    <div class="topbar-check-all">All</div>
                </div>
            </div>
            <div class="content-box">
                <div class="content">

                    <div class="content-list-container" style="padding-bottom: 16px;">
                        <ul class="content-list js-scroll">

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="display: none">
        <div class="add-content">
            <form class="add-content-form content-item active" enctype="multipart/form-data" action="/adminka/create/metatags" method="post">
                <div class="content-form-fields">
                    <input class="content-form-field content-form-field-text content-form-field-url" type="text" name="url_pattern">
                    <input class="content-form-field content-form-field-title" type="text" name="title">
                    <textarea class="content-form-field content-form-field-textarea content-form-field-textarea-c" name="description"></textarea>
                    <input class="content-form-field content-form-field-text content-form-field-keywords" type="text" name="keywords">
                </div>
                <div class="content-form-controls">
                    <a href="#" class="content-form-control js-content-form-cancel">Cancel</a>
                    <a href="#" class="content-form-control js-content-event-save">Save</a>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript" src="/js/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="/js/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript" src="/js/jquery.elastic.source.js"></script>
    <script type="text/javascript" src="/js/arcticModal/jquery.arcticmodal-0.3.min.js"></script>
    <script type="text/javascript" src="/js/jsrender.min.js"></script>
    <script type="text/javascript" src="/js/cp-tags.js"></script>

</body>
</html>