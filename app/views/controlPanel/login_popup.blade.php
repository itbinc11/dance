<div style="display: none;">
    <div id="login_popup" class="popup-container">
        <div class="popup-head clearfix">
            <a href="/" class="popup-logo">Dance project</a>
            <a href="#" class="popup-close arcticmodal-close"> </a>
        </div>

        <div class="popup-music">
            <h5>LOGIN</h5>
            <div class="music-related">

                {{ Form::open(array( 'route' => 'controlPanel.login', 'class' => 'clearfix', 'id' => 'loginForm'  )) }}

                    <div class="login_alert"></div>
                    <div>
                        <span class="form-description">Username: </span>
                        {{ Form::text('username', null, array('class' => 'form__input username_login')) }}
                    </div>

                    <div>
                        <span class="form-description">Password: </span>
                        {{ Form::password('password', array('class' => 'form__input password_login')) }}
                    </div>

                {{ Form::close() }}
                <div class="form-footer">
                    <button class="sign-btn" type="button" onclick="submit_login()">LOGIN</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $("input").bind("enterKey",function(e){
        submit_login();
    });
    $("input").keyup(function(e){
        playList = $(this);
        if(e.keyCode == 13) {
          $(this).trigger("enterKey");
        }
    });


    $('#head-log-in').click(function() {
        $('#login_popup').arcticmodal();
    });
    function submit_login() {

        var data = {
                username: $('.username_login').val(),
                password: $('.password_login').val()
            };
            $.ajax({
              type: "POST",
              url: '/loginCheck',
              data: data,
              success: function(responce) {
                if(responce) {
                    document.getElementById("loginForm").submit();
                } else {
                   $('.password_login').val('');
                   $('.login_alert').html('Wrong Username or Password.')
                }
              }
            });


    }
</script>