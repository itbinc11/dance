@if(count($metatags))
    @foreach($metatags as $item)
        <li class="content-item">
            <div class="content-header">
                <div class="content-header-edit"><a class="content-header-edit-control js-content-edit" href="#"> </a></div>
                <div class="content-header-id">{{ $item->id }}</div>

                <div class="content-header-event"> </div>
                <div class="content-header-title">{{ $item->url_pattern }}</div>
                <div class="content-header-category"></div>
                <div class="content-header-check"> </div>
            </div>
            <form class="content-form clearfix metatags" enctype="multipart/form-data">
                <div class="content-form-fields">
                    <input class="content-form-field content-form-field-title" type="text" name="title" value="{{ $item->title }}">
                    <div class="redactor-wrapper">
                        <textarea class="content-form-field content-form-field-textarea js-redactor" name="description">{{ $item->description }}</textarea>
                    </div>
                    <input class="content-form-field content-form-field-text content-form-field-keywords" type="text" name="keywords" value="{{ $item->keywords }}">
                </div>
                <div class="content-form-controls">
                    <a href="#" class="content-form-control js-form-cancel">Cancel</a>
                    <a href="#" class="content-form-control js-form-save">Save</a>
                </div>
            </form>
        </li>
    @endforeach
@else
    <li class="content-item">Not found</li>
@endif