@foreach($users_result as $user)
    <li class="content-item">
        <div class="content-header">
            <div class="content-header-edit">
                <a class="content-header-edit-control js-content-edit" href="#"> </a>
            </div>
            <div class="content-header-id">
                {{ $user['id'] }}
            </div>
            <div class="content-header-title">
                {{ $user['username'] }} | <span>Account type</span> {{ $user['tag'] }}
            </div>
            <div class="content-header-category" style="display: inline-block;">
                INFORMATION
            </div>
            <div class="content-header-check"> </div>
        </div>
    </li>
@endforeach