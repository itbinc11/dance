<div class="profile-card">
    <div class="profile-card-pic">
        <img height="50" width="auto" src="/img/avatars/<?php if(!empty(Sentry::getUser()->avatar)) { echo Sentry::getUser()->avatar; } else { echo 'noavatar.png'; } ?>" alt="avatar"/>
    </div>


      <div class="profile-card-username"
          data-container="body"
          data-html="true"
          data-toggle="popover"
          data-placement="bottom"
          data-content="<div class='menu_list profile-card-status'>
                            <p><a href='/adminka'>ADMIN PANEL</a></p>
                            <p><a href='/myinfo'>MY INFORMATION</a></p>
                            <p><a href='/mycontacts'>MY CONTACTS</a></p>
                            <p><a href='/favorites'>FAVORITES</a></p>
                            <p><a href='/playlists'>MANAGE PLAYLISTS</a></p>
                        </div>">
          {{ Sentry::getUser()->username }}
      </div>



</div>

<div class="add-new-card">
    <div class="add-new-title">ADD</div>
    <a class="add-new-button">MUSIC / VIDEO / EVENT</a>
    <p><a class="add-new-link" href="#">VIEW ADDED</a></p>
</div>

<script>
$(function () {
  $('[data-toggle="popover"]').popover()
})
</script>