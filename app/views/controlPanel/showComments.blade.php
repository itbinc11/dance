@if($comments)
@foreach($comments as $item)
        <li>
            @if($item->user_avatar)
                <img class="content-form-image" height="40" width="auto" alt="avatar" src="/img/avatars/{{ $item->user_avatar }}">
            @endif
            <div class="content-header-title">{{ $item->user_username }}</div>
            <div class="content-header-title">{{ $item->comment_created_at }}</div>
            <div class="content-header-title">{{ $item->comment_comment }}</div>
            <div class="content-header"></div>
        </li>

@endforeach
@else
No comments.
@endif