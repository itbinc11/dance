@if(count($result))
    @foreach($result as $item)
        <li class="content-item users">
            <div class="content-header clearfix">
                <div class="content-header-edit"><a class="content-header-edit-control js-content-edit" href="#"> </a></div>                
                <div class="content-header-id">{{ $item->user_id }}</div>                
                @if($item->user_avatar)
                    <img class="content-form-image" height="40" width="auto" alt="avatar" src="/img/avatars/{{ $item->user_avatar }}">
                @else
                    <img class="content-form-image" height="40" width="auto" alt="avatar" src="/img/avatars/noavatar.png">
                @endif
                <div class="content-header-title">{{ $item->user_username }}</div>
                <div class="content-header-check"> </div>
                <div class="content-header-category"><span>Account Type </span> {{ $item->group_name }}</div>
                <div class="content-header-category info">INFORMATION</div>
            </div>
            <div class="clearfix"></div>
            <form class="content-form clearfix" enctype="multipart/form-data">
                <div class="content-form-pic">
                            @if($item->user_avatar)
                                <img class="content-form-image" src="/img/avatars/{{ $item->user_avatar }}">
                            @endif

                    <a class="content-form-choose-image js-img-input-trigger" href="#">Choose image</a>
                </div>
                <div class="content-form-fields">
                    <div class="sign-name1">
                        <span class="form-description" >FIRST NAME </span>
                        <input class="content-form-field content-form-field-text content-form-field-first_name" type="text" name="first_name" value="{{ $item->user_first_name }}">
                    </div>                    
                    <div class="sign-name2">
                        <span class="form-description" >LAST NAME </span>
                        <input class="content-form-field content-form-field-text content-form-field-last_name" type="text" name="last_name" value="{{ $item->user_last_name }}">
                    </div>
                    <div>
                        <span class="form-description" >LOGIN </span>
                        <input class="content-form-field content-form-field-text content-form-field-username" type="text" name="username" value="{{ $item->user_username }}">
                    </div>
                    <div>
                        <span class="form-description" >E-MAIL </span>
                        <input class="content-form-field content-form-field-text content-form-field-email" type="text" name="email" value="{{ $item->user_email }}">
                    </div>
                     <!--div>
                        <span class="form-description" >PASSWORD </span>
                        <input class="content-form-field content-form-field-text content-form-field-user_password" type="password" name="password">
                    </div-->
                    
                    <input class="hidden js-img-input" type="file" name="img">
                    <input class="hidden" type="hidden" name="id" value="{{ $item->user_id }}">
                    <input class="content-form-type" type="hidden" name="type" value="user">
                    <div class="content-form-controls">
                        <a href="#" class="content-form-control js-user-save">Save</a>
                        <!--a href="#" class="content-form-control js-form-cancel">Cancel</a-->
                    </div>
                </div>
                <div class="redactor-wrapper">
                    <span class="form-description" >ABOUT ME </span>
                    <textarea class="content-form-field content-form-field-textarea content-form-field-about js-redactor" name="about">{{ $item->user_about }}</textarea>
                </div>
            </form>
        </li>
    @endforeach
@else
    <li class="content-item">Not found</li>
@endif