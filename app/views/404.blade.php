@extends('index')

@section('title')Page not found — Dance project @stop

@section('content')
            <div class="column content-full">
                
                <div class="column-title clearfix">
                    <div class="column-title-title">Page not found (error 404)</div>
                </div>
            </div>
@stop