<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

        <link rel="stylesheet" href="/cp-css/style.css">
        <link type="text/css" rel="stylesheet" href="/js/jscrollpane/jquery.jscrollpane.css"/>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

        <script type="text/javascript" src="/js/humane.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/js/humane/flatty.css">

        <script type="text/javascript" src="/js/jquery.tagsinput.js"></script>
        <link rel="stylesheet" type="text/css" href="/js/tagsinput/jquery.tagsinput.css">

        <script type="text/javascript" src="/js/jquery.easydropdown.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/js/easydropdown/easydropdown.css">

        <script type="text/javascript" src="/js/jquery.simple-dtpicker.js"></script>
        <link type="text/css" href="/js/simple-dtpicker/jquery.simple-dtpicker.css" rel="stylesheet" />

        <link rel="stylesheet" type="text/css" href="/js/arcticModal/jquery.arcticmodal-0.3.css">

        <script type="text/javascript" src="/js/redactor.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/js/redactor/redactor.css">




    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/js/arcticModal/jquery.arcticmodal-0.3.css">
    <link rel="stylesheet" type="text/css" href="/js/nouislider/jquery.nouislider.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <title>@yield('title', $metaTags->title)</title>
    <meta name="description" content="@yield('description', $metaTags->description)" />
    @yield('head')
</head>
<body>
    @yield('submenu')

    <div class="head-navigation-b">
        <div class="head-navigation container">
            <a class="head-navigation-textlogo" href="/">Dance Project</a>
            <img class="head-navigation-logo" src="/img/logo.png" alt="Dance Project logo">
            <ul class="head-category">
                <li class="head-category-item"><a href="/music" class="head-category-link">Music</a></li>
                <li class="head-category-item"><a href="/videos" class="head-category-link">Video</a></li>
                <li class="head-category-item"><a href="/events" class="head-category-link">Events</a></li>

            </ul>
            <form action="/search" method="get">
                <input type="text" name="q" class="head-search" placeholder="search">
            </form>
            @if(Sentry::check())
            <a class="head-sign-in" href="/logout">Log out</a>
            @else
            <span id="head-sign-in" class="head-sign-in">Sign in</span>
            <span id="head-log-in" class="head-sign-in">Log in</span>
            @endif
        </div>
    </div>

    <div class="main-b">

        <div class="main container clearfix">
            @yield('content')
            @include('sidebar')
        </div>

    </div>

    <div class="footer-menu-b">
        <ul class="footer-menu container">
            <li class="footer-menu-item"><a href="/" class="footer-menu-link">Home</a></li>
            <li class="footer-menu-item"><a href="#" class="footer-menu-link">Contact us</a></li>
            <li class="footer-menu-item"><a href="#" class="footer-menu-link">About us</a></li>
            <li class="footer-menu-item"><a href="#" class="footer-menu-link">DMCA</a></li>
            <li class="footer-menu-item"><a href="#" class="footer-menu-link">Sitemap</a></li>
        </ul>
    </div>

    <div class="footer-b">
        <div class="footer container">

            <div class="footer-music">
                <div class="footer-title">Music</div>
                <ul class="footer-list">
                    @foreach($musicSubcategories as $cat)
                    <li class="footer-list-item"><a href='/music/{{ $cat->slug }}' class="footer-list-link">{{ $cat->title }}</a></li>
                    @endforeach
                </ul>
            </div>

            <div class="footer-video">
                <div class="footer-title">Video</div>
                <ul class="footer-list">
                    @foreach($videoSubcategories as $cat)
                    <li class="footer-list-item"><a href='/videos/{{ $cat->slug }}' class="footer-list-link">{{ $cat->title }}</a></li>
                    @endforeach
                </ul>
            </div>

            <div class="footer-popular">
                <div class="footer-title">Popular</div>
                <ul class="footer-popular-list">
                    @foreach($popular as $track)
                    <li class="footer-popular-item">
                        <a href="{{ URL::route('music/track', $track->slug) }}" class="footer-popular-link js-ajax-popup">{{ $track->title }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>

            <div class="credits">
                <a class="credits-logo" href="/">Dance Project</a>
                <p class="credits-copyright">© Durshlag  2014</p>
            </div>

        </div>
    </div>


    <div style="display: none">
            <div class="add-content">
                <dl class="tabs">
                    <dt class="active">Track</dt>
                    <dd class="active">
                            <form class="add-content-form clearfix">
                                <input class="content-form-field content-form-field-text" type="text" name="url">
                                <div class="content-form-controls">
                                        <a href="#" class="content-form-control js-content-form-cancel">Cancel</a>
                                        <a href="#" class="content-form-control js-content-form-add-track">Add</a>
                                    </div>
                            </form>
                    </dd>

                    <dt>Video</dt>
                    <dd>
                            <form class="add-content-form clearfix">
                                <input class="content-form-field content-form-field-text" type="text" name="url">
                                <div class="content-form-controls">
                                        <a href="#" class="content-form-control js-content-form-cancel">Cancel</a>
                                        <a href="#" class="content-form-control js-content-form-add-video">Add</a>
                                    </div>
                            </form>
                    </dd>

                    <dt>Event</dt>
                    <dd>

                        <form class="add-content-form content-item active" enctype="multipart/form-data" action="/adminka/create/event" method="post">
                            <div class="content-form-pic">
                                <img class="content-form-image" src="">
                                <a class="content-form-choose-image js-img-input-trigger" href="#">Choose image</a>
                            </div>
                            <div class="content-form-fields">
                                <input class="content-form-field content-form-field-title" type="text" name="title">
                                <textarea class="content-form-field content-form-field-textarea-c js-redactor-popup" name="description"></textarea>
                                <input class="content-form-field content-form-field-text content-form-field-url" type="text" name="url">
                                <input class="content-form-field content-form-field-text content-form-field-place" type="text" name="place">
                                <input class="content-form-field content-form-field-text content-form-field-city" type="text" name="city">
                                <input class="content-form-field content-form-field-text content-form-field-country" type="text" name="country">
                                <input class="content-form-field content-form-field-text content-form-field-date js-popup-date-picker" type="text" name="date" value="{{ date(DateTime::RFC3339) }}">
                                <input class="content-form-field content-form-field-text js-popup-date-picker" type="text" name="pdate" value="{{ date(DateTime::RFC3339) }}">
                                <input class="js-hidden-pdate" type="hidden" name="hidden-pdate" value="{{ date(DateTime::RFC3339) }}">
                                <input class="content-form-field content-form-field-text content-form-field-guest" type="text" name="guest">
                                <label><input style="height: 15px; width: 15px; vertical-align: middle;" type="checkbox" name="crosspublish"> — cross-publish</label>
                                <input class="hidden js-img-input" type="file"  accept="image/*" name="img">
                            </div>
                            <div class="content-form-controls">
                                <a href="#" class="content-form-control js-content-form-cancel">Cancel</a>
                                <a href="#" class="content-form-control js-content-event-save">Save</a>
                            </div>
                        </form>

                    </dd>
                </dl>
            </div>

        </div>

        <style type="text/css">
            .redactor_toolbar::after {
                content: none;
            }

            /* Быстрофикс. Всплывающее окно перекрывало попапы redactor'а. */
            #redactor_modal_overlay, #redactor_modal, .redactor_dropdown {
                z-index: 1001 !important;
            }


        </style>


<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript" src="/js/soundmanager2-nodebug-jsmin.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/arcticModal/jquery.arcticmodal-0.3.min.js"></script>
<script type="text/javascript" src="/js/jquery.nouislider.min.js"></script>
<script type="text/javascript" src="/js/lazyYT.js"></script>
<script type="text/javascript" src="/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="/js/jquery.elastic.source.js"></script>
<script type="text/javascript" src="/js/arcticModal/jquery.arcticmodal-0.3.min.js"></script>
<script type="text/javascript" src="/js/jsrender.min.js"></script>
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.js"></script>




@if(!Sentry::check())
@include('controlPanel.login_popup')
@include('controlPanel.register_popup')
@endif

@yield('documentEnd')


@if (!App::environment('local'))
    <!-- Google analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-20881796-4', 'danceproject.info');
        ga('send', 'pageview');

    </script>
@endif
</body>
</html>