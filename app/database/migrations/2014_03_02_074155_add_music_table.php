<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMusicTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('music', function($table)
        {
            $table->increments('id');
            $table->text('url')->unique();
            $table->text('slug');
            $table->text('api_url')->unique();
            $table->text('title')->unique();
            $table->integer('duration');
            $table->text('artwork_url')->nullable();
            $table->text('waveform_url')->nullable();
            $table->text('stream_url');
            $table->text('download_url')->nullable();
            $table->text('purchase_url')->nullable();
            $table->text('description');
            $table->integer('subcategory_id')
                ->foreign()
                ->references('id')
                ->on('subcategories')
                ->unsigned()
                ->index();
            $table->integer('user_id')
                ->foreign()
                ->references('id')
                ->on('users')
                ->unsigned();
            $table->integer('playback_count')->unsigned()->default(0)->nullable();
            $table->integer('internal_playback_count')->unsigned()->default(0)->nullable();
            $table->integer('internal_weekly_playback_count')->unsigned()->default(0)->nullable();
            $table->integer('favoritings_count')->unsigned()->default(0)->nullable();
            $table->integer('comment_count')->unsigned()->default(0)->nullable();
            $table->text('search');
            $table->integer('published');
            $table->boolean('crosspublish');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('music');
    }

}
