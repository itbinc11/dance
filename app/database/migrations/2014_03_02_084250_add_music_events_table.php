<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMusicEventsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('music_events', function($table)
        {
            $table->increments('id');
            $table->text('url')->unique();
            $table->text('slug');
            $table->text('title')->unique();
            $table->text('description');
            $table->text('poster')->nullable();
            $table->dateTime('date');
            $table->text('place');
            $table->text('city');
            $table->text('country');
            $table->text('guest');
            $table->integer('user_id')
                ->foreign()
                ->references('id')
                ->on('users')
                ->unsigned();
            $table->integer('published');
            $table->boolean('crosspublish');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('music_events');
    }

}
