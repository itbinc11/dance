<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVideosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function($table)
        {
            $table->increments('id');
            $table->text('url')->unique();
            $table->text('slug');
            $table->text('youtube_id')->unique();
            $table->text('title')->unique();
            $table->text('description');
            $table->integer('subcategory_id')
                ->foreign()
                ->references('id')
                ->on('subcategories')
                ->unsigned()
                ->index();
            $table->integer('user_id')
                ->foreign()
                ->references('id')
                ->on('users')
                ->unsigned();
            $table->integer('internal_playback_count')->unsigned()->default(0)->nullable();
            $table->text('search');
            $table->integer('published');
            $table->boolean('crosspublish');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos');
    }

}
