<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMetaTagsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_tags', function($table)
        {
            $table->increments('id');
            $table->text('url_pattern');
            $table->text('title');
            $table->text('description')->nullable();
            $table->text('keywords')->nullable();
            $table->timestamps();
        });

        // Мета-теги для сайта.
        MetaTags::create(array(
            'url_pattern' => '*',
            'title' => 'Listen music at DanceProject.info',
            'description' => "DanceProject.info — Listen music online."
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('meta_tags');
    }

}
