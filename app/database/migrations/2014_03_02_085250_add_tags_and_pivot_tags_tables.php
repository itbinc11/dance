<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTagsAndPivotTagsTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function($table)
        {
            $table->increments('id');
            $table->text('title')->unique();
            $table->text('search');
            $table->timestamps();
        });

        Schema::create('tag_music', function($table)
        {
            $table->increments('id');
            $table->integer('tag_id')->index();
            $table->integer('music_id')->index();
        });

        Schema::create('tag_video', function($table)
        {
            $table->increments('id');
            $table->integer('tag_id')->index();
            $table->integer('video_id')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tags');
        Schema::drop('tag_music');
        Schema::drop('tag_video');
    }

}
