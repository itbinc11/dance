<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditAuthScheme extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // Добавляем всем пользователям параметр username
        Schema::table('users', function($table)
        {
            $table->string('username')->nullable();
        });

        // Создаём группу администраторов
		$adminsGroup = Sentry::createGroup(array(
            'name'        => 'Administrators',
            'permissions' => array(
                'admin' => 1,
                'user' => 1,
            ),
        ));

        // Создаём группу пользователей
        Sentry::createGroup(array(
            'name'        => 'Users',
            'permissions' => array(
                'admin' => 0,
                'user' => 1,
            ),
        ));

        // Создаём пользователя
		$user = Sentry::createUser(array(
            'username' => 'admin',
            'email' => 'admin@example.com',
            'password' => 'password',
            'activated' => 1
        ));

        // Назначаем его в группу администраторы
        $user->addGroup($adminsGroup);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		User::find(1)->delete();

        $group = Sentry::findGroupByName('Administrators');
        $group->delete();

        $group = Sentry::findGroupByName('Users');
        $group->delete();

        Schema::table('users', function($table)
        {
            $table->dropColumn('username');
        });
	}

}
