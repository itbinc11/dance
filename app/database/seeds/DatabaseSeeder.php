<?php

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        // $this->call('UserTableSeeder');
        $guest = array('Chris Travis', 'Benedict', 'TWRK');
        
        for ($i=1; $i <= 20; $i++) { 
            MusicEvent::create(array(
                'url' => 'www.trap.dp.ua/' . $i,
                'title' => 'Music event #' . $i,
                'slug' => URLify::filter('Music event #' . $i),
                'description' => 'Event description text',
                'poster' => '/img/event-poster.jpg',
                'date' => date('G:i, j F Y'),
                'place' => 'Pristol',
                'city' => 'Odessa',
                'country' => 'Ukraine',
                'guest' => $guest[array_rand($guest)],
                'user_id' => '1',
                'published' => 1
            ));
        }

        $trackLinks = array(
            'https://soundcloud.com/harryandremusic/skywind-combat-theme-in',
            'https://soundcloud.com/niklasaman/fullfillment',
            'https://soundcloud.com/andrewrayel/andrew-rayel-live-a-state-of-3',
            'https://soundcloud.com/chrisfuckingtravis/chris-travis-free-gucci',
            'https://soundcloud.com/soulblime/benedict-white-noise',
            'https://soundcloud.com/wooxstar/raul-midon-sunshine-wookie',
            'https://soundcloud.com/akshin/akshin-alizadeh-widow-jones-1',
            'https://soundcloud.com/youngandsick/heartache-fetish-1',
            'https://soundcloud.com/ahmed-gado-1/fairouz-daiaano',
            'https://soundcloud.com/dubstep/rest-nest-by-must-die-spag',
            'https://soundcloud.com/missk8/angerfist-miss-k8-live',
            'https://soundcloud.com/troyave/trey-songz-nana-remix-ft-troy',
            'https://soundcloud.com/djpeligroperu/ellameborro',
            'https://soundcloud.com/belheirmusic/free',
            'https://soundcloud.com/ahmed-gado-2/fahir-atako-lu-turkish-music',
            'https://soundcloud.com/edmx/fresh013-samples',
            'https://soundcloud.com/doornrecords/tv-noise-the-hold-original-mix',
            'https://soundcloud.com/8dawn/8dio-claire-english-horn-3',
            'https://soundcloud.com/tiesto/coldplay-midnight-ti-stos',
            'https://soundcloud.com/deepsounds/saw-throat-by-craig-williams',
            'https://soundcloud.com/sidewalk-records/you-know-who-we-are-highway',
            'https://soundcloud.com/manchesterorchestra/top-notch-acoustic',
            'https://soundcloud.com/diplo/boy-oh-boy-twrk-edit',
            'https://soundcloud.com/diplo/boy-oh-boy-twrk-editkjah'
        );

        foreach ($trackLinks as $trackLink) {
            $get = http_build_query(array(
                'url' => $trackLink,
                'client_id' => Config::get('app.apiKey')
            ));

            // разрешаем APi URL из share URL'а
            $url = 'http://api.soundcloud.com/resolve.json';
            $url = $url . '?' . $get;

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $var = curl_exec($ch);

            if (curl_errno($ch) or curl_getinfo($ch, CURLINFO_HTTP_CODE) == '404') { 
                echo 'failed track: ', $trackLink;
                continue; 
            };

            $var = json_decode($var);

            curl_close($ch);
            
            if ($var->streamable) {
                Music::create(array(
                    'url' => $trackLink,
                    'slug' => URLify::filter($var->title),
                    'api_url' => $var->uri,
                    'title' => $var->title,
                    'duration' => $var->duration,
                    'artwork_url' => str_replace('-large', '-t300x300', $var->artwork_url),
                    'waveform_url' => $var->waveform_url,
                    'stream_url' => $var->stream_url,
                    'download_url' => property_exists($var, 'download_url') ? $var->download_url : 0,
                    'purchase_url' => $var->purchase_url,
                    'description' => $var->description,
                    'search' => strtolower($var->title),
                    'subcategory_id' => rand(1,6),
                    'user_id' => '1',
                    'playback_count' => property_exists($var, 'playback_count') ? $var->playback_count : 0,
                    'favoritings_count' => property_exists($var, 'favoritings_count') ? $var->favoritings_count : 0,
                    'published' => 1
                ));
            };

            echo "successfully added track: $trackLink \n";
        };

        
        $videos = array(
            'http://www.youtube.com/watch?v=EHkozMIXZ8w&feature=share&list=PLH6pfBXQXHEDerzgz13A0nnKvepESBsqj',
            'http://www.youtube.com/watch?v=0KSOMA3QBU0&list=PLFgquLnL59an-oQxF1-GKCJ-0eWXYkOoH&feature=share&index=4',
            'http://www.youtube.com/watch?v=y6Sxv-sUYtM&feature=share&list=PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI&index=1',
            'http://www.youtube.com/watch?v=-2U0Ivkn2Ds&feature=share&list=PLDcnymzs18LXX8I3ERVq0cgWnHE5ARBRc&index=2',
            'http://www.youtube.com/watch?v=450p7goxZqg&feature=share&list=PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI&index=3',
            'http://www.youtube.com/watch?v=gCYcHz2k5x0&list=PLFPg_IUxqnZM3uua-YwStHJ1qmlQKBnh0&feature=share&index=1',
            'http://youtu.be/hT_nvWreIhg',
            'http://www.youtube.com/watch?v=YxIiPLVR6NA&list=PLFPg_IUxqnZM3uua-YwStHJ1qmlQKBnh0&feature=share&index=2',
            'http://www.youtube.com/watch?v=McEoTIqoRKk&list=PLFPg_IUxqnZM3uua-YwStHJ1qmlQKBnh0&feature=share&index=8',
            'http://www.youtube.com/watch?v=h5EofwRzit0&feature=share&list=PLFPg_IUxqnZM3uua-YwStHJ1qmlQKBnh0&index=9',
            'http://www.youtube.com/watch?v=5dbEhBKGOtY&feature=share&list=PLFPg_IUxqnZM3uua-YwStHJ1qmlQKBnh0&index=12',
            'http://www.youtube.com/watch?v=4ZHwu0uut3k&feature=share&list=PLFPg_IUxqnZM3uua-YwStHJ1qmlQKBnh0&index=20',
            'http://www.youtube.com/watch?v=e-U1lj57pv8&list=PLFPg_IUxqnZM3uua-YwStHJ1qmlQKBnh0&feature=share&index=44',
            'http://www.youtube.com/watch?v=p6j8fuvQICI&feature=share&list=PLFPg_IUxqnZM3uua-YwStHJ1qmlQKBnh0&index=45',
            'http://www.youtube.com/watch?v=oIwFJNguQgY&list=PLFPg_IUxqnZM3uua-YwStHJ1qmlQKBnh0&feature=share&index=56',
            'http://www.youtube.com/watch?v=45Q4Zk3CN8k&list=PLH6pfBXQXHEDerzgz13A0nnKvepESBsqj&feature=share&index=17'
        );

        foreach ($videos as $video) {

            if(preg_match('/^http:\/\/youtu\.be/', $video)) {
                $ytshorturl = 'youtu.be/';
                $ytlongurl = 'www.youtube.com/watch?v=';
                $video = str_replace($ytshorturl, $ytlongurl, $video);
            };

            $queryString = parse_url($video, PHP_URL_QUERY);
            $query = array();
            parse_str($queryString, $query);

            $youtubeId = $query['v'];


            $url = "http://gdata.youtube.com/feeds/api/videos/". $youtubeId;

            $xml = simplexml_load_file($url);

            $title = $xml->title[0];
            $duration =  $xml->children('media', true)
                          ->group[0]->children('yt', true)
                            ->duration[0]->attributes('', true)->seconds;
            $description =  $xml->children('media', true)
                          ->group[0]->children('media', true)
                            ->description[0];


            Video::create(array(
                'url' => $video,
                'slug' => URLify::filter($title),
                'youtube_id' => $youtubeId,
                'title' => $title,
                'description' => $description,
                'search' => strtolower($title),
                'subcategory_id' => rand(1,6),
                'user_id' => '1',
                'published' => 1
            ));

            echo "successfully added video: $video \n";
        }

        Subcategory::create(array(
            'title' => 'Hip-Hop',
            'slug' => 'hip-hop'
        ));
        Subcategory::create(array(
            'title' => 'House',
            'slug' => 'house'
        ));
        Subcategory::create(array(
            'title' => 'Popping',
            'slug' => 'popping'
        ));
        Subcategory::create(array(
            'title' => 'locking',
            'slug' => 'locking'
        ));
        Subcategory::create(array(
            'title' => 'Dancehall',
            'slug' => 'dancehall'
        ));
        Subcategory::create(array(
            'title' => 'Breaking/Funk',
            'slug' => 'breaking-funk'
        ));
        

        $tag = Tag::create(array(
            'title' => 'Remix',
            'search' => 'remix'
        ));
        $tag->music()->attach(1);
        $tag->music()->attach(2);

        Tag::create(array(
            'title' => 'Orig/Lo-fi',
            'search' => 'orig/lo-fi'
        ));

        $track = Music::find(4);
        $track->tags()->attach(1);
        $track->tags()->attach(2);
        $track = Music::find(6);
        $track->tags()->attach(1);

        $clip = Video::find(1);
        $clip->tags()->attach(1);
        $clip = Video::find(3);
        $clip->tags()->attach(1);
        $clip = Video::find(4);
        $clip->tags()->attach(1);
        $clip = Video::find(6);
        $clip->tags()->attach(1);

    }

}