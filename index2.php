<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ulysse Nardin - мужские наручные часы</title>
	<meta name="viewport" content="width=1000">
	<link href="favicon.html" rel="shortcut icon" type="image/x-icon">
	<link rel="icon" href="favicon.png" type="image/png">
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all">
	<link rel="stylesheet" href="countdown/jquery.countdown.css">	
	<link rel="stylesheet" href="css/jcarousel.responsive.css">
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/jquery.scrollTo-1.4.3.1.js"></script>
	<script src="js/jcarousel.responsive.js"></script>
	<script src="js/jquery.jcarousel.min.js"></script>
	
<script type="text/javascript">
    $(document).ready(function() {
        $('form').submit(function() {
            $('input[name=name]', this).val($.trim($('input[name=name]', this).val()));
            if(!$('input[name=name]', this).val()) {
                alert('Укажите корректные ФИО!');
                return false;
            }

            var phone_val = $('input[name=phone]', this).val();
            var reg1 = new RegExp("[^0-9]*", "g"), reg2 = new RegExp("[^0-9\-\+ \(\)]", "g");
            var phone_txt = phone_val.replace(reg1, '');
            
            if(phone_val.search(reg2) != -1)
            {
                alert('Номер телефона может содержать только цифры, символы "+", "-", "(", ")" и пробелы');
                return false;
            }
            
            if(!phone_txt || phone_txt.length < 7) 
            {
                alert('В вашем телефоне слишком мало цифр!');
                return false;
            }
            return true;
        });
        $('a.order-btn').click(function() {
            $(this).closest('form').submit();
            return false;
        });
    });
</script>

<!-- jQuery/Modal BEGIN -->
<link href="css/modal.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="js/modal.js"></script>
<!-- jQuery/Modal END -->

</head>	
<body>
<script>
    $(document).ready(function(){
       $(".naviblock span a").click(function(){
          var selected = $(this).attr('href');  
          $.scrollTo(selected, 500);        
          return false;
       });//scrollTo
    });
</script>
<script>
    $(document).ready(function(){         
            var $menu = $(".menu");         
            $(window).scroll(function(){
                if ( $(this).scrollTop() > 250 && $menu.hasClass("menu2") ){
                    $menu.removeClass("default").addClass("f-nav");
                } else if($(this).scrollTop() <= 250 && $menu.hasClass("f-nav")) {
                    $menu.removeClass("f-nav").addClass("menu2");
                }
            });//scroll fixed menu
        });
</script>
<header>
	<div class="headerbg">
		<div class="header">
			<div class="lheader">
				<a href="/"><img src="images/logo.png" alt="Ulysse NardiN"></a>
				<p>Потрясающий подарок для любимого человека</p>
			</div>		
				<div class="rheader">
					<span class="tel1">+375 44 567 17 46</span>
					<span class="tel2">+375 29 273 23 21</span>
					<div class="mail">
						<span>e-mail:</span>
						<a href="#">daxiz@tut.by</a>
					</div>			
					<a class="rheadera" href="#win1">Заказать звонок</a>
				</div>
				<div class="clear"></div>
		</div>
	</div>
	<div style='' class='menu menu2'>
		<div class='fixed'>
			<div class='navi'>
				<div class='naviblock'>
					<span><a href='#block3'>ТОВАР ЛИЦОМ </a></span>
					<span><a href='#video'>видеообзор</a></span>
					<span><a href='#block2'>преимущества МОДЕЛИ</a></span>
					<span><a href='#ben'>ПОЧЕМУ МЫ?</a></span>
					<span><a href='#hww'>Схема работы</a></span>
					<span><a href='#block4'>НАШИ ГАРАНТИИ </a></span>
					<span><a href='#comment'>Отзывы</a></span>
				</div>
			</div>
		</div>
	</div>
</header>

<div class="bg1">
	
	<div class="block1">
		<h1>элитные часы <span>Ulysse NardiN Marine</span></h1>
		<h2>олицетворение статуса и успеха</h2>
	<div class="leftblock1">
		<span class="span1"><span>-54%</span></span><br>
		<span class="span2"><span>1099000 Б.Р.</span></span><br>
		<span class="span3">499000 Б.Р.</span>
		<div class="clear"></div>
	</div>
	<div class="form">
		<h3>оформите заказ</h3>
		<p>и купите часы по супер цене - <font color="#dd1e1e">54%</font><br>спешите!</p>
		<div class="lefttimer">
			<p>До конца<br>акции<br>осталось:</p>
		</div>
		<div class="timer">
			<div id="countdown"></div>
			<!-- JavaScript includes -->
			<script src="countdown/jquery.countdown.js"></script>
			<script src="countdown/script.js"></script>
			<table>
				<tr>
					<td class="td1">Часов</td>
					<td class="td2">Минут</td>
					<td class="td3">Секунд</td>
				</tr>
			</table>
		</div>
		<div class="clear"></div>
		  <form method="POST">
		   <input type="hidden" name="z3z" value="1">
		   <input type="hidden" name="q" value="4">
		   <ul class="">
		   <div class="lli">
		   <li class="button-row"><input placeholder="Введите ваше имя" class="i1 required" required name="name" type="text" alt=""/></li>
		   <li class="button-row"><input placeholder="Введите ваш телефон" class="i2 required tel" required name="phone" type="text" alt=""/></li>
		   <li class="button-row"><input placeholder="Адрес доставки" class="i3 address" name="address" type="text" alt=""/></li>
		   </div>
		   <div class="rri">
		   <li class="button-row"><input type="submit" alt="" value="сделать заказ"  class="btn-submit img-swap"></li>
		   </div>
               <i class="alert"></i>
		   <div class="clear"></div>
		   </ul>
		   <div class="clear"></div>
		   <input type="hidden" name="s1" class="price_field_s1" value="499 000" />
		   <input type="hidden" name="s2" class="price_field_s2" value="50 000" />
		   <input type="hidden" name="s3" class="price_field_s3" value="549 000" />
		  </form>
		  <span class="econom">Экономия 600 000 бел.руб.</span>		
	</div>

	</div>
	<div class="clear"></div>
</div>
<div style="overflow:hidden;">
<div class="video" id="video">
	<img class="absolute1" src="images/watches2.png">
	<h2>Наручные часы <font style="color:#dd1e1e;">Ulysse Nardin</font><br>это непревзойденное сочетание<br>совершенства техники и идеального<br>дизайна</h2>
	<!--  <hr color=#dd1e1e size=4 align=left width=54%> <!-->
	<div style="margin-top:40px;height:307px;padding-top:27px;" onclick="thevid=document.getElementById('thevideo'); thevid.style.display='block'; this.style.display='none';  document.getElementById('iframe').src = document.getElementById('iframe').src.replace('autoplay=0','autoplay=1');">
	 <img style="cursor: pointer;" src="images/videoimg.jpg" alt="" />
	</div>

	<div id="thevideo" style="display: none;">
	<iframe id="iframe" width="458" height="290" src="http://www.youtube.com/embed/bfUlNvNOW7M?rel=0&amp;vq=hd720&amp;autoplay=0&amp;wmode=transparent&amp;showinfo=0"
	frameborder="0" allowscriptaccess="always" allowfullscreen="true"></iframe>
	</div>
	<h3>Посмотрите официальный <font style="color:#dd1e1e;">видеообзор</font></h3>
	<p>Нажмите на иконку выше для начала просмотра</p>
</div>
</div>
<div class="bg">
	<div class="bg2">
	<div class="block2" id="block2">
		<h3><font color="#dd1e1e">5 причин</font> выбрать<br>Наручные часы <font color="#dd1e1e">Ulysse Nardin</font></h3>
		<!--<hr color=#dd1e1e size=4 align=left width=43%><!-->
		<table>
			<tr>
				<td class="block2td">1</td>
				<td>
					<h2>качественный корпус</h2>
					<p>Полированная сталь с IPG покрытием, водонепроницаемый</p>
				</td>
			</tr>
			<tr>
				<td class="block2td">2</td>
				<td>
					<h2>надежное стекло</h2>
					<p>Сапфировое, износостойкое, с двусторонним антибликовым покрытием</p>
				</td>
			</tr>
			<tr>
				<td class="block2td">3</td>
				<td>
					<h2>точность хода</h2>
					<p>Более 4300 наград в этой номинации</p>
				</td>
			</tr>
			<tr>
				<td class="block2td">4</td>
				<td>
					<h2>стильный мужской дизайн</h2>
					<p>Тестовый текст, место для краткого описания</p>
				</td>
			</tr>
			<tr>
				<td class="block2td">5</td>
				<td>
					<h2>Уникальный ремень</h2>
					<p>Ремень облегающий запястье, не деформируется при носке</p>
				</td>
			</tr>
			<div class="clear"></div>
		</table>
		<div class="clear"></div>
	</div>
	</div>
</div>
<div class="comments" id="comment">
	<h2><font color="#dd1e1e">Отзывы наших клиентов:</font></h2>
		<!--<hr color=#dd1e1e size=4 align=left width=38%><!-->
	<div class="jcarousel-wrapper">
		<div class="jcarousel">
			<ul>
				<li>
					<img class="c1" src="images/c6.jpg">
					<div class="cis">
						<h3>Наталия Де-Марки</h3>
						<span>г. Заславль</span>
						<p><img src="images/arrow.png">Выбрала мужу именно эту модель. И не прогадала. Смотрятся шикарно. Он у меня любитель таких вещей. На все свои деловые встречи, переговоры одевает именно их. Работа магазина тоже порадовала. Спасибо Вам!</p>		 
					</div>
					<div class="clear"></div>
				</li>			
				<li>
					<img class="c1" src="images/c3.jpg">
					<div class="cis">
						<h3>Александр Трацевский</h3>
						<span>г. Минск</span>
						<p><img src="images/arrow.png">Часы очень стильные и смотрятся на руке дорого. Идеальное дополнение к моим классическим костюмам. Да и по качеству нареканий нет. Все работает, даже несмотря на то, что пару раз ронял. Рекомендую!</p>		 
					</div>
					<div class="clear"></div>
				</li>				
				<li>
					<img class="c1" src="images/c7.jpg">
					<div class="cis">
						<h3>Ольга Станкевич</h3>
						<span>г. Брест</span>
						<p><img src="images/arrow.png">А я парню такие часы на День рождение подарила. Часы ему очень понравились. Он был вне себя от радости! Угодила, значит))) Давно его таким не видела. Спасибо Вам!</p>		 
					</div>
					<div class="clear"></div>
				</li>
				<li>
					<img class="c1" src="images/c4.jpg">
					<div class="cis">
						<h3>Виталий Богдан</h3>
						<span>г. Минск</span>
						<p><img src="images/arrow.png">Заказывал на этом сайте. Все оперативно и вежливо. Доставка произведена вовремя. Очень рад своим выбором. Долго сомневался, но все-таки решился. И не жалею. Дизайн часов просто крут! Спасибо.</p>		 
					</div>
					<div class="clear"></div>
				</li>				
				<li>
					<img class="c1" src="images/c5.jpg">
					<div class="cis">
						<h3>Сергей Ломако</h3>
						<span>г. Витебск</span>
						<p><img src="images/arrow.png">Согласен. Дизайнеры постарались. Ношу с удовольствием уже давно. Все исправно. Отличное соотношение цены и качества!</p>		 
					</div>
					<div class="clear"></div>
				</li>
			</ul>
		</div>
		<div href="#" class="jc11 jcarousel-control-prev"><h4>Предыдущий отзыв</h4></div>
		<div href="#" class="jc11 jcarousel-control-next"><h4>Следующий отзыв</h4></div>
   </div>
</div>
<div class="bg">
	<div class="bg4">
		<div class="block4" id="block4">
			<h2>наши<br><font style="color:#dd1e1e;">гарантии</font></h2>
			<!--<hr color=#dd1e1e size=4 align=left width=15%><!-->
			<table class="block4table">
				<tr>
					<td><img src="images/ok.png"></td>
					<td style="padding-left:20px;">100% проверка товара<br>перед отправкой</td>
				</tr>
				<tr>
					<td><img src="images/ok.png"></td>
					<td style="padding-left:20px;">Соблюдение закона “О защите прав Потребителя”.
					В случае выявления заводского дефекта Вы вправе
					обменять товар на новый в течении 14 дней с
					момента получения заказа.</td>
				</tr>
				<tr>
					<td><img src="images/ok.png"></td>
					<td style="padding-left:20px;">Гарантия доставки. В момент отправки, каждому заказу присваивается номер. 
					 По номеру можно отследить посылки на сайте.
					Мы незамедлительно вышлем номер на Ваш e-mail.</td>
				</tr>
			</table>
			<div class="form">
				<h3>ОФОРМИТЕ ЗАКАЗ</h3>
				<h4>и купите часы по цене - <span>54%</span></h4>
				<p>спешите из 200 комплектов осталось 9</p>
				<form method="post">
					<ul class="">
					<div class="lli">
					<li class="button-row"><input placeholder="Введите ваше имя" class="i1 required" required name="name" type="text" alt=""/></li>
					<li class="button-row"><input placeholder="Введите ваш телефон" class="i2 required tel" required name="phone" type="text" alt=""/></li>
					<li class="button-row"><input placeholder="Адрес доставки" class="i3 address" name="address" type="text" alt=""/></li>
					</div>
					<div class="rri">
					<li class="button-row"><input type="submit" name="submit" alt="" value="сделать заказ"  class="btn-submit img-swap"></li>
					</div>
                        <i class="alert"></i>
					<div class="clear"></div>
					</ul>
					<div class="clear"></div>
					<input type="hidden" name="s1" class="price_field_s1" value="499 000" />
					<input type="hidden" name="s2" class="price_field_s2" value="50 000" />
					<input type="hidden" name="s3" class="price_field_s3" value="549 000" />
				</form>
				<div class="lefttimer">
					<p>До конца<br>акции<br>осталось:</p>
				</div>
				<div class="timer">
					<div id="countdown2"></div>
					<!-- JavaScript includes -->
					<script src="countdown/jquery.countdown2.js"></script>
					<script src="countdown/script2.js"></script>
					<table>
						<tr>
							<td class="td1">Часов</td>
							<td class="td2">Минут</td>
							<td class="td3">Секунд</td>
						</tr>
					</table>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

<div class="ben" id="ben">
	<div class="ben-wrap">
		<h2>Наши<font color="#dd1e1e"> преимущества</font></h2>
		<!--<hr color=#dd1e1e size=4 align=left width=32%><!-->
		<table>
			<tr>
				<td class="block2td">1</td>
				<td>
					<p>мы работаем без предоплаты.<br/> вы оплачиваете посылку только при <br/> получении, в ваши руки по адресу<br/> заказа или в отделении белпочты.</p>
				</td>
			</tr>
			<tr>
				<td class="block2td">2</td>
				<td>
					<p>бесплатно доставим в любой регион<br/> республики беларусь.</p>
				</td>
			</tr>
			<tr>
				<td class="block2td">3</td>
				<td>
					<p style="margin-top:20px;">скидка -54% на часы Ulysse Nardin Marine</p>
				</td>
			</tr>
			<tr>
				<td class="block2td">4</td>
				<td>
					<p style="margin-top:20px;">гарантия 12 месяцев с момента получения.</p>
				</td>
			</tr>
			<tr>
				<td class="block2td">5</td>
				<td>
					<p style="margin-top:-18px;">красивые и качественные часы Ulysse Nardin Marine позволят вам в считанные секунды располагать собеседника к себе на встречах и важных переговорах.</p>
				</td>
			</tr>
			<tr>
				<td class="block2td">6</td>
				<td>
					<p>когда часы Ulyss на вашей руке, противоположный пол не сможет устоять перед вами.</p>
				</td>
			</tr>
			<div class="clear"></div>
		</table>
	</div>
	<div class="clear"></div>
</div>
<div class="bg hww-wrap">
	<div class="hww" id="hww">
		<h2>Как мы <font color="#dd1e1e"><br>работаем</font></h2>
		<!--<hr color=#dd1e1e size=4 align=left width=15%><!-->
		<div class="hww1">
			<p>Вы оставляете заявку</p>
			<a href="#win3" class="rheadera">Оставить заявку</a>
		</div>
		<div class="hww2">
			<p>мы перезваниваем<br>вам</p>
		</div>
		<div class="hww3">
			<p>отправляем ваш<br>заказ</p>
		</div>
		<div class="hww4">
			<p>курьер доставит посылку <br>к двери вашего дома,<br>Вы оплачиваете заказ</p>
		</div>
		<div class="hww5">
			<p style="color:#dd1e1e;">становитесь обладателем<br>роскошного аксессуара</p>
		</div>
	</div>
</div>
<div class="bg3">
	<div class="block3" id="block3">
		<h2><font color="#dd1e1e">Доставка и оплата</font><br/>беслатная доставка по всей рб</h2>
		<!--<hr color=#dd1e1e size=4 align=left width=38%><!-->
		<p>Мы Вам доверяем и не требуем с вас предоплаты. Мы уважаем Ваше время, и поэтому все заказы отправляем только службой EMS, 1 классом.<br/>	
		Вам доставят заказ домой к порогу или вы можете оплатить свой заказ на почте в любое удобное для вас время.<br/><br/>
		Мы отправим Ваш заказ уже сегодня 1 классом. Часы дойдут до Вас быстро и без единой царапинки. Доставка займет 1-3 рабочих дней (зависит от региона). Чтобы ускорить <br/>прибытие Вашего заказа, мы отправляем бандероли с главпочтамта два раза в день, <br/>включая выходные дни.<br/><br/>
		Оплата заказа наложенным платежом<br/><br/>
		Оплата осуществляется у порога Вашего дома либо почтовом отделении по Вашему<br/> желанию, при получении заказа.<br/><br/>
		Получить заказ можно в почтовом отделении<br/>
		или по адресу, который был указан, при оформлении заказа. Вас уведомят по<br/>
		телефону и уточнят время доставки по вашему адресу.<br/><br/>
		Для получения заказа подготовьте паспорт.<br/>
		Срок хранения заказа в почтовом отделении составляет<br/>
		1 месяц с момента поступления.<br/>
		Просим Вас своевременно получить пришедший заказ.<br/><br/>
		<i><font color="#dd1e1e">Мы заботимся о своей репутации,<br/>
		Довольные клиенты для нас - на первом месте!</font></i>
		</p>
	</div>
</div>

<div class="bg footer-wrap">
    <div class="footer">
        <h2>ЗАКАЖИТЕ ЧАСЫ ВАШЕЙ МЕЧТЫ В 1 КЛИК!<font color="#dd1e1e"> со скидкой -54%</font></h2>
        <a href="#win2" class="rheadera">Сделать заказ</a>
        <div class="clear"></div>
    </div>
    <div class="footer-bot">
        <div class="fbl-wrap">
            <div class="fbl">
                <strong>Прием заказов:</strong>
                <span class="tel1">+375 44 567 17 46</span>
                <span class="tel2">+375 29 273 23 21</span>
                <div class="mail">
                    <span>e-mail:</span>
                    <a href="#">daxiz@tut.by</a>
                </div>
            </div>
            <div class="fbr">
                <span>ИП ПОЖАХ И.В.</span>
                <span>УНП: 191525842</span>
            </div>
        </div>
        <div class="clear"></div>
        <div class="fbb-wrap">
        	<span>Создание сайтов, Landing page. разработка продающих сайтов</span>
        	<a href="//itb-inc.net/">itb-inc.net</a>
        </div>
        
    </div>
</div>


<!--div id="polit" class="modal">
    <div class="modal-block">
        <div class="icon-close"></div>
        <div class="title">ПОЛИТИКА КОНФИДЕНЦИАЛЬНОСТИ</div>
        <div class="content">
            <div class="padding">
                <p>Наш интернет-магазин уважительно относится к правам клиента. Соблюдается строгая конфиденциальность
                    при оформлении заказа. Сведения надёжно сохраняются и защищены от передачи. </p>

                <p>Согласием на обработку данных клиента исключительно с целью оказания услуг является размещение заказа
                    на сайте. </p>

                <p>К персональным данным относится личная информация о клиенте: домашний адрес; имя, фамилия, отчество;
                    сведения о рождении; имущественное, семейное положение; личные контакты (телефон, электронная почта)
                    и прочие сведения, которые

                    перечислены в Законе РФ № 152-ФЗ «О персональных данных» от 27 июля 2006 г. </p>

                <p>Клиент вправе отказаться от обработки персональных данных. Нами в данном случае гарантируется
                    удаление с сайта всех персональных данных в трёхдневный срок в рабочее время. Подобный отказ клиент
                    может оформить простым электронным

                    письмом на адрес, указанный на странице нашего сайта. </p>
            </div>
        </div>
    </div>
</div-->
<!--ВСПЛЫВАЮЩИЙ БЛОК-->
	<a href="#x" class="overlay" id="win1"></a>
	<div class="popup">
		<div class="popupform">
		<h2>заказать <font color="#de2720">звонок</font></h2>
		<form method="post">
			<ul>
				<div class="lli">
					<li class="button-row"><input placeholder="Введите ваше имя" class="i1 required" required name="name" type="text" alt=""/></li>
					<li class="button-row"><input placeholder="Введите ваш телефон" class="i2 required tel" required name="phone" type="text" alt=""/></li>
				</div>
				<div class="rri">
					<li class="button-row"><input type="submit" name="submit" alt="" value="сделать заказ"  class="btn-submit img-swap"></li>
				</div>
                <i class="alert"></i>
				<div class="clear"></div>
			</ul>
			<div class="clear"></div>
			<input type="hidden" name="s1" class="price_field_s1" value="499 000" />
			<input type="hidden" name="s2" class="price_field_s2" value="50 000" />
			<input type="hidden" name="s3" class="price_field_s3" value="549 000" />
		</form>
		</div>
	<a class="close" title="Закрыть" href="#close"></a>
	</div>

	<a href="#x" class="overlay" id="win2"></a>
	<div class="popup">
		<div class="popupform">
		<h2>заказать <font color="#de2720">звонок</font></h2>
			<form method="post">
				<ul>
				<div class="lli">
					<li class="button-row"><input placeholder="Введите ваше имя" class="i1 required" required name="name" type="text" alt=""/></li>
					<li class="button-row"><input placeholder="Введите ваш телефон" class="i2 required tel" required name="phone" type="text" alt=""/></li>
				</div>
				<div class="rri">
				<li class="button-row"><input type="submit" name="submit" alt="" value="сделать заказ"  class="btn-submit img-swap"></li>
				</div>
                    <i class="alert"></i>
				<div class="clear"></div>
				</ul>
				<div class="clear"></div>
				<input type="hidden" name="s1" class="price_field_s1" value="499 000" />
				<input type="hidden" name="s2" class="price_field_s2" value="50 000" />
				<input type="hidden" name="s3" class="price_field_s3" value="549 000" />
			</form>
		</div>
	<a class="close" title="Закрыть" href="#close"></a>
	</div>

	<a href="#x" class="overlay" id="win3"></a>
	<div class="popup">
		<div class="popupform">
		<h2>заказать <font color="#de2720">звонок</font></h2>
		<form method="post">
			<ul class="">
			<div class="lli">
			<li class="button-row"><input placeholder="Введите ваше имя" class="i1 required" required name="name" type="text" alt=""/></li>
			<li class="button-row"><input placeholder="Введите ваш телефон" class="i2 required tel" required name="phone" type="text" alt=""/></li>
			</div>
			<div class="rri">
			<li class="button-row"><input type="submit" name="submit" alt="" value="сделать заказ"  class="btn-submit img-swap"></li>
			</div>
                <i class="alert"></i>
			<div class="clear"></div>
			</ul>
			<div class="clear"></div>
			<input type="hidden" name="s1" class="price_field_s1" value="499 000" />
			<input type="hidden" name="s2" class="price_field_s2" value="50 000" />
			<input type="hidden" name="s3" class="price_field_s3" value="549 000" />
		</form>
		</div>
	<a class="close" title="Закрыть" href="#close"></a>
	</div>
    
</body>
</html>

<script>
    $(document).on('click', '.btn-submit', function() {
        form = $(this).closest('form');
        var name = form.find('input[name=name]').val();

        if(!name || name == '') {
            alert('Укажите корректные ФИО!');
            return false;
        }

        var phone = form.find('input[name=phone]').val();
        if(!phone || phone == '') {
            alert('Укажите телефон!');
            return false;
        }

        var phone_val = phone;
        var reg1 = new RegExp("[^0-9]*", "g"), reg2 = new RegExp("[^0-9\-\+ \(\)]", "g");
        var phone_txt = phone_val.replace(reg1, '');

        if(phone_val.search(reg2) != -1)
        {
            alert('Номер телефона может содержать только цифры, символы "+", "-", "(", ")" и пробелы');
            return false;
        }

        if(!phone_txt || phone_txt.length < 7)
        {
            alert('В вашем телефоне слишком мало цифр!');
            return false;
        }

        var address = form.find('input[name=address]').val();
        if (!address) { address = '';}

        $.ajax({
            type: 'post',
            data: { name: name, phone: phone, address: address},
            url: "watches_mail.php",
            success: function(result){
                if(result === 'Message sent!') {
                    window.location.replace("#close");
                    $("input[name='phone']").val('');
                    $("input[name='address']").val('');
                    $("input[name='name']").val('');
                    $('.alert').html('Спасибо. Ваше сообщение отправлено!');
                }
            }
        });
        return false;
    });
</script>