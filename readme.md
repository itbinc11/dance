# Danceproject.info #

Сайт написан на PHP с использованием фреймворка Laravel и системы потоковой сборки Gulp.js. В качестве СУБД используется SQLite.


## Сборка и запуск ##

После развёртывания файлов из репозитория нужно доустановить библиотеки и компоненты при помощи composer:

```
#!bash
composer install
```

… и npm:

```
#!bash
npm install
```

Далее необходимо настроить пакет Cartalyst/Sentry и создать БД, выполнив последовательно команды…

```
#!bash
# копируем пустую базу
cp app/database/production.sqlite.empty app/database/production.sqlite

# создаём схему в БД и конфиг для Sentry
php artisan migrate --package=cartalyst/sentry
php artisan config:publish cartalyst/sentry

# создаём остальные структуры в БД и прибираемся
php artisan migrate
php artisan optimize
```

или запустив пакетный файл **initSentryAndDB.bat** (в случае работы под Windows).

Последний этап: сборка и оптимизация фронтенда:

```
#!bash
gulp build
```